library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


entity trig_memory_tb is
--  Port ( );
end trig_memory_tb;

architecture behavioral of trig_memory_tb is

  CONSTANT width_sym : natural := 25;
  CONSTANT bc_width_sym : natural := 11;
  CONSTANT latency : natural := 4;
  CONSTANT groups : natural := 2;

  COMPONENT trig_memory
  generic(
    height: natural;
    width: natural;
    bc_width: natural
  );
  port(
    clock : in std_logic;
    bc_clock : in std_logic;
    setup : in std_logic;

    in_bcid : in std_logic_vector(bc_width-1 downto 0);

    in_d_flag : in std_logic;
		in_d : in std_logic_vector(width-6+2-1 downto 0);
		out_d : out std_logic_vector(width+bc_width-6-1 downto 0);
    out_flag : out std_logic;

    err_out : out std_logic;

    search_finished_out : out std_logic;
    read_finished_out : out std_logic;
    read_reset : in std_logic;

    read_cmd : in std_logic;
    read_cmd_reset : out std_logic;

    in_trig_flag : in std_logic;
    in_trig_id : in std_logic_vector(bc_width-1 downto 0)
		);
  END COMPONENT;

  component trigger_control
    generic(
      bc_width : natural
    );
    port(
      bc_clock : in std_logic;
      clock : in std_logic;
      reset : in std_logic;

      trig : in std_logic;
      total_read_finished : in std_logic;
      read_finished_reset : out std_logic;

      read_cmd : out std_logic; -- cmd bc read
      read_cmd_reset : in std_logic;
      search_flag : out std_logic; -- cmd bc search
      search_id : out std_logic_vector(bc_width-1 downto 0); -- bc to search
      bc_flag : out std_logic;
      bc_id : out std_logic_vector(bc_width-1 downto 0) -- bc for writing
    );
  end component;


  --Inputs
  signal clk : std_logic := '0';
  signal bc_clk : std_logic := '0';

  signal bcid_sym : std_logic_vector(bc_width_sym - 1 downto 0);
  --
  -- signal in_bcid_sym_reg : unsigned(bc_width_sym - 1 downto 0);
  -- signal in_bcid_sym_next : unsigned(bc_width_sym - 1 downto 0);

  signal bc_flag : std_logic;

  signal setup_sym : std_logic;
  signal in_d_flag_sym : std_logic;
  signal in_d_sym : std_logic_vector(width_sym-6+2-1 downto 0);

  signal search_finished_out : std_logic_vector(groups downto 1);
  -- signal read_finished_out : std_logic_vector(groups+1 downto 1);
  signal read_cmd : std_logic_vector(groups+1 downto 1); -- cmd bc read
  signal read_cmd_reset : std_logic_vector(groups+1 downto 1);
  -- signal read_reset : std_logic_vector(groups+1 downto 1);


  --signal bc_in : natural;
  signal d_in : natural;

  signal bc_in : std_logic_vector(1 downto 0);
  signal d_in_vec : std_logic_vector(width_sym-6-1 downto 0);

  signal in_trig_flag_sym : std_logic;
  signal in_trig_flag : std_logic;
  signal search_flag : std_logic;
  signal trig_id : std_logic_vector(bc_width_sym-1 downto 0);
  -- signal trig_in : unsigned(bc_width_sym-1 downto 0);

  --Outputs
  signal out_d_sym : std_logic_vector((width_sym + bc_width_sym - 6)*groups-1 downto 0);

  signal out_bc_sym : std_logic_vector(bc_width_sym-1 downto 0);
  signal out_d_vec_sym : std_logic_vector(width_sym-6-1 downto 0);
  signal out_flag_sym : std_logic_vector(groups downto 1);
  signal err_out_sym : std_logic_vector(groups downto 1);

  -- Clock period definitions
  constant clk_period : time := 10 ns;
  constant bc_clk_period : time := 80 ns; --(8x write period)

begin
	-- Instantiate the Unit Under Test (UUT)

  generate_groups :
  for i in 1 to groups generate
    mem_ut : trig_memory
  	generic map(
  		height => 50,
  		width => width_sym,
      bc_width => bc_width_sym
  	)
  	port map (
      clock => clk,
      bc_clock => bc_clk,
      setup => setup_sym,

      in_bcid => bcid_sym,

      in_d_flag => in_d_flag_sym,
  		in_d => in_d_sym,
  		out_d => out_d_sym((width_sym+bc_width_sym-6)*i-1 downto (width_sym+bc_width_sym-6)*(i-1)),
      out_flag => out_flag_sym(i),

      err_out => err_out_sym(i),

      search_finished_out => search_finished_out(i),
      read_finished_out => read_cmd(i+1),
      read_reset => read_cmd_reset(i+1),

      read_cmd => read_cmd(i),
      read_cmd_reset => read_cmd_reset(i),

  		in_trig_flag => search_flag,
  		in_trig_id => trig_id
  	);
  end generate;

  trig_contr_ut : trigger_control
  generic map(
    bc_width => bc_width_sym
  )
  port map(
    bc_clock => bc_clk,
    clock => clk,
    reset => setup_sym,

    trig => in_trig_flag,
    total_read_finished => read_cmd(groups+1),
    read_finished_reset => read_cmd_reset(groups+1),

    read_cmd => read_cmd(1), -- cmd bc read
    read_cmd_reset => read_cmd_reset(1),
    search_flag => search_flag, -- cmd bc search
    search_id => trig_id, -- bc to search
    bc_flag => bc_flag,
    bc_id => bcid_sym -- bc for writing
  );


  out_bc_sym <= out_d_sym(width_sym+bc_width_sym-6-1 downto width_sym-6);
  out_d_vec_sym <= out_d_sym(width_sym-6-1 downto 0);

  -- Clock process definitions
  clk_process : process
  begin
      clk <= '1';
    wait for clk_period/2;
      clk <= '0';
    wait for clk_period/2;
  end process;

  -- bc_counter_process : process(setup_sym, bc_clk)
  -- begin
  --   if(setup_sym = '0') then
  --     in_bcid_sym_reg <= (others => '0');
  --     -- in_trig_flag <= '1';
  --   elsif(bc_clk'event and bc_clk = '1') then
  --     in_bcid_sym_reg <= in_bcid_sym_next;
  --     -- if (in_trig_flag_sym = '1') then
  --     --   in_trig_flag <= '0';
  --     -- elsif (in_trig_flag_sym = '0') then
  --     --   in_trig_flag <= '1';
  --     -- end if;
  --   end if;
  -- end process;

  -- in_trig_flag_process : process(setup_sym, clk)
  -- begin
  --   if(setup_sym = '0') then
  --     in_trig_flag <= '1';
  --   elsif(clk'event and clk = '1') then
  --     if (in_trig_flag_sym = '1') then
  --       in_trig_flag <= '0';
  --     elsif (in_trig_flag_sym = '0') then
  --       in_trig_flag <= '1';
  --     end if;
  --   end if;
  -- end process;
  in_trig_flag <= not in_trig_flag_sym;
  --
  -- read_cmd_process : process(setup_sym, clk)
  -- begin
  --   if(setup_sym = '0') then
  --     read_cmd_sym <= '1';
  --   elsif(clk'event and clk = '1') then
  --     if (search_finished_out_sym = '0') then
  --       read_cmd_sym <= '0';
  --     elsif (search_finished_out_sym = '1') then
  --       read_cmd_sym <= '1';
  --     end if;
  --   end if;
  -- end process;

  -- read_reset_sym <= '0';

  -- in_bcid_sym_next <= in_bcid_sym_reg + 1;
  --
  -- in_bcid_sym <= std_logic_vector(in_bcid_sym_reg);

  bc_clk_process : process
  begin
      bc_clk <= '1';
    wait for bc_clk_period/2;
      bc_clk <= '0';
    wait for bc_clk_period/2;
  end process;

  -- Contacts

  in_d_sym <= bc_in & d_in_vec;
  --bc_in_vec <= std_logic_vector(to_unsigned(bc_in, 2));
  d_in_vec <= std_logic_vector(to_unsigned(d_in, width_sym-6));
  -- trig_id <= std_logic_vector(trig_in);
  -- trig_in <= in_bcid_sym_reg - latency;
   -- Stimulus process
	stim_proc : process
	begin
      setup_sym <= '0';
      bc_in <= "11";
      d_in <= 0;
      in_trig_flag_sym <= '0';

    wait for 41 ns;
      setup_sym <= '1';
      bc_in <= "11";
      d_in <= 0;
      in_d_flag_sym <= '1';
      -- read_cmd_sym <= '1';
      for i in 0 to 10 loop
    wait for 10 ns;
      setup_sym <= '1';
			--in_d_sym <= "00000000000" & "00000011";
      bc_in <= "10";
      d_in <= 3;
      in_d_flag_sym <= '1';
    wait for 10 ns;--2
			--in_d_sym <= "00000000000" & "00000110";
      bc_in <= "00";
      d_in <= 6;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000000";
      -- in_trig_flag_sym <= '1';
		wait for 20 ns;--3
			-- in_d_sym <= "00000000001" & "00001100";
      d_in <= 12;
      in_d_flag_sym <= '1';
    wait for 10 ns;
      in_d_flag_sym <= '0';
      -- in_trig_id_sym <= "00000000000";
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--5
    	-- in_d_sym <= "00000000001" & "00011000";
      d_in <= 24;
      in_d_flag_sym <= '1';
		wait for 10 ns;--6
			-- in_d_sym <= "00000000010" & "00110000";
      d_in <= 48;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000001";
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;
      in_d_flag_sym <= '0';
		wait for 20 ns;--9
			-- in_d_sym <= "00000000010" & "01100000";
      d_in <= 96;
      in_d_flag_sym <= '1';
		wait for 10 ns;--10
			-- in_d_sym <= "00000000011" & "11000000";
      bc_in <= "10";
      d_in <= 192;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000011";
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--14
      in_d_flag_sym <= '0';
      bc_in <= "00";
    wait for 30 ns;--14
			-- in_d_sym <= "00000000100" & "00000101";
      d_in <= 5;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000000";
      --trig_in <= 0;
      ------------------------------------------
      -- in_trig_flag_sym <= '1';
		wait for 10 ns;--15
			-- in_d_sym <= "00000000100" & "00001010";
      d_in <= 20;
      in_d_flag_sym <= '1';
      -- in_trig_flag_sym <= '0';
    wait for 10 ns;--16
			-- in_d_sym <= "00000000100" & "00010100";
      d_in <= 40;
      in_d_flag_sym <= '1';
		wait for 20 ns;--17
      bc_in <= "11";
      -- d_in <= to_integer(in_bcid_sym_reg);
      -- read_cmd_sym <= '0';
    wait for 10 ns;--17
			-- in_d_sym <= "00000000110" & "00101000";
      bc_in <= "10";
      d_in <= 80;
      in_d_flag_sym <= '1';
      -- in_trig_flag_sym <= '0';
      -- read_cmd_sym <= '1';
      -- in_trig_id_sym <= "00000000010";
      --trig_in <= 2;
      -- in_trig_flag_sym <= '1';
		wait for 10 ns;--18
			-- in_d_sym <= "00000000111" & "01010000";
      bc_in <= "00";
      d_in <= 160;
      in_d_flag_sym <= '1';
      -- in_trig_flag_sym <= '1';
		wait for 10 ns;--19

      -- in_trig_flag_sym <= '0';
      -- in_d_sym <= "00000001000" & "00000011";
      d_in <= 3;
      in_d_flag_sym <= '1';
    wait for 10 ns;--2
      -- in_d_sym <= "00000001000" & "00000110";
      d_in <= 6;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000000";
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--3
      in_d_flag_sym <= '0';
      -- read_cmd_sym <= '0';
    wait for 10 ns;--3
      -- read_cmd_sym <= '1';
    wait for 30 ns;--3
      -- in_d_sym <= "00000001001" & "00001100";
      bc_in <= "10";
      d_in <= 12;
      in_d_flag_sym <= '1';
    wait for 10 ns;
      in_d_flag_sym <= '0';
      -- in_trig_id_sym <= "00000000101";
      --trig_in <= 5;
      ------------------------------------------
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--5
      -- in_d_sym <= "00000001001" & "00011000";
      bc_in <= "00";
      d_in <= 24;
      in_d_flag_sym <= '1';
      -- in_trig_flag_sym <= '0';
    wait for 10 ns;--6
      -- in_d_sym <= "00000001010" & "00110000";
      d_in <= 48;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000001";
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--9
      in_d_flag_sym <= '0';
    wait for 20 ns;--9
      -- in_d_sym <= "00000001010" & "01100000";
      d_in <= 96;
      in_d_flag_sym <= '1';
      -- read_cmd_sym <= '0';
    wait for 10 ns;--10
      -- read_cmd_sym <= '1';
      -- in_d_sym <= "00000001011" & "11000000";
      d_in <= 192;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000000111";
      --trig_in <= 7;
      -- in_trig_flag_sym <= '1';
    wait for 10 ns;--14
      in_d_flag_sym <= '0';
      -- in_trig_flag_sym <= '0';
    wait for 30 ns;--14
      -- in_d_sym <= "00000001100" & "00000101";
      bc_in <= "10";
      d_in <= 5;
      in_d_flag_sym <= '1';
      ------------------------------------------------
      -- in_trig_flag_sym <= '1';
      -- read_cmd_sym <= '0';
    wait for 10 ns;--15
      -- in_d_sym <= "00000001100" & "00001010";
      -- read_cmd_sym <= '1';
      bc_in <= "00";
      d_in <= 10;
      in_d_flag_sym <= '1';
    wait for 10 ns;--16
      -- in_d_sym <= "00000001100" & "00010100";
      d_in <= 20;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000001000";
      --trig_in <= 8;
      in_trig_flag_sym <= '1';
    wait for 10 ns;--17
      in_d_flag_sym <= '0';
      in_trig_flag_sym <= '0';
    wait for 20 ns;
      in_trig_flag_sym <= '0';
    wait for 80 ns;--17
      -- in_d_sym <= "00000001110" & "00101000";
      bc_in <= "10";
      d_in <= 40;
      in_d_flag_sym <= '1';
      -- in_trig_id_sym <= "00000001010";
      --trig_in <= 10;
      in_trig_flag_sym <= '0';
      -- read_cmd_sym <= '0';
    wait for 10 ns;--18
      -- in_d_sym <= "00000001111" & "01010000";
      -- read_cmd_sym <= '1';
      bc_in <= "00";
      d_in <= 80;
      in_d_flag_sym <= '1';
      in_trig_flag_sym <= '0';
    wait for 10 ns;--19
      in_d_flag_sym <= '0';

      wait for 10 ns;
        setup_sym <= '1';
  			--in_d_sym <= "00000000000" & "00000011";
        d_in <= 3;
        in_d_flag_sym <= '1';
      wait for 10 ns;--2
  			--in_d_sym <= "00000000000" & "00000110";
        d_in <= 6;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000000";
        -- in_trig_flag_sym <= '1';
  		wait for 20 ns;--3
  			-- in_d_sym <= "00000000001" & "00001100";
        d_in <= 12;
        in_d_flag_sym <= '1';
      wait for 10 ns;
        in_d_flag_sym <= '0';
        -- in_trig_id_sym <= "00000000000";
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;--5
      	-- in_d_sym <= "00000000001" & "00011000";
        d_in <= 24;
        in_d_flag_sym <= '1';
  		wait for 10 ns;--6
  			-- in_d_sym <= "00000000010" & "00110000";
        bc_in <= "10";
        d_in <= 48;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000001";
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;
        in_d_flag_sym <= '0';
  		wait for 20 ns;--9
  			-- in_d_sym <= "00000000010" & "01100000";
        bc_in <= "00";
        d_in <= 96;
        in_d_flag_sym <= '1';
  		wait for 10 ns;--10
  			-- in_d_sym <= "00000000011" & "11000000";
        d_in <= 192;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000011";
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;--14
        in_d_flag_sym <= '0';
      wait for 30 ns;--14
  			-- in_d_sym <= "00000000100" & "00000101";
        bc_in <= "10";
        d_in <= 5;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000000";
        --trig_in <= 0;
        ------------------------------------------
        in_trig_flag_sym <= '1';
  		wait for 10 ns;--15
  			-- in_d_sym <= "00000000100" & "00001010";
        bc_in <= "00";
        d_in <= 20;
        in_d_flag_sym <= '1';
        in_trig_flag_sym <= '0';
      wait for 10 ns;--16
  			-- in_d_sym <= "00000000100" & "00010100";
        d_in <= 40;
        in_d_flag_sym <= '1';
  		wait for 10 ns;--17
        bc_in <= "11";
        -- d_in <= to_integer(in_bcid_sym_reg);
        -- read_cmd_sym <= '0';
      wait for 10 ns;--17
  			-- in_d_sym <= "00000000110" & "00101000";
        bc_in <= "10";
        d_in <= 80;
        in_d_flag_sym <= '1';
        -- in_trig_flag_sym <= '0';
        -- read_cmd_sym <= '1';
        -- in_trig_id_sym <= "00000000010";
        --trig_in <= 2;
        -- in_trig_flag_sym <= '1';
  		wait for 10 ns;--18
  			-- in_d_sym <= "00000000111" & "01010000";
        bc_in <= "00";
        d_in <= 160;
        in_d_flag_sym <= '1';
        -- in_trig_flag_sym <= '1';
  		wait for 10 ns;--19

        in_trig_flag_sym <= '0';
        -- in_d_sym <= "00000001000" & "00000011";
        -- bc_in <= "10";
        d_in <= 3;
        in_d_flag_sym <= '1';
      wait for 10 ns;--2
        -- in_d_sym <= "00000001000" & "00000110";
        bc_in <= "00";
        d_in <= 6;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000000";
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;--3
        in_d_flag_sym <= '0';
        -- read_cmd_sym <= '0';
      wait for 10 ns;--3
        -- read_cmd_sym <= '1';
      wait for 10 ns;--3
        -- in_d_sym <= "00000001001" & "00001100";
        bc_in <= "10";
        d_in <= 12;
        in_d_flag_sym <= '1';
      wait for 10 ns;
        in_d_flag_sym <= '0';
        -- in_trig_id_sym <= "00000000101";
        --trig_in <= 5;
        ------------------------------------------
        in_trig_flag_sym <= '1';
      wait for 10 ns;--5
        -- in_d_sym <= "00000001001" & "00011000";
        bc_in <= "00";
        d_in <= 24;
        in_d_flag_sym <= '1';
        in_trig_flag_sym <= '0';
      wait for 10 ns;--6
        -- in_d_sym <= "00000001010" & "00110000";
        bc_in <= "00";
        d_in <= 48;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000001";
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;--9
        in_d_flag_sym <= '0';
      wait for 20 ns;--9
        -- in_d_sym <= "00000001010" & "01100000";
        -- bc_in <= "10";
        d_in <= 96;
        in_d_flag_sym <= '1';
        -- read_cmd_sym <= '0';
      wait for 10 ns;--10
        -- read_cmd_sym <= '1';
        -- in_d_sym <= "00000001011" & "11000000";
        bc_in <= "00";
        d_in <= 192;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000000111";
        --trig_in <= 7;
        -- in_trig_flag_sym <= '1';
      wait for 10 ns;--14
        in_d_flag_sym <= '0';
        in_trig_flag_sym <= '0';
      wait for 30 ns;--14
        -- in_d_sym <= "00000001100" & "00000101";
        bc_in <= "10";
        d_in <= 5;
        in_d_flag_sym <= '1';
        ------------------------------------------------
        in_trig_flag_sym <= '0';
        -- read_cmd_sym <= '0';
      wait for 10 ns;--15
        -- in_d_sym <= "00000001100" & "00001010";
        -- read_cmd_sym <= '1';
        bc_in <= "00";
        d_in <= 10;
        in_d_flag_sym <= '1';
      wait for 10 ns;--16
        -- in_d_sym <= "00000001100" & "00010100";
        bc_in <= "00";
        d_in <= 20;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000001000";
        --trig_in <= 8;
        in_trig_flag_sym <= '1';
      wait for 10 ns;--17
        in_d_flag_sym <= '0';
        in_trig_flag_sym <= '0';
      wait for 20 ns;
        in_trig_flag_sym <= '0';
      wait for 80 ns;--17
        -- in_d_sym <= "00000001110" & "00101000";
        bc_in <= "10";
        d_in <= 40;
        in_d_flag_sym <= '1';
        -- in_trig_id_sym <= "00000001010";
        --trig_in <= 10;
        in_trig_flag_sym <= '1';
        -- read_cmd_sym <= '0';
      wait for 10 ns;--18
        -- in_d_sym <= "00000001111" & "01010000";
        -- read_cmd_sym <= '1';
        bc_in <= "00";
        d_in <= 80;
        in_d_flag_sym <= '1';
        in_trig_flag_sym <= '0';
      wait for 10 ns;--19
        in_d_flag_sym <= '0';
      end loop;

	end process;

end behavioral;
