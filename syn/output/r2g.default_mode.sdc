# ####################################################################

#  Created by Genus(TM) Synthesis Solution 16.22-s033_1 on Tue Jun 12 15:46:03 +0200 2018

# ####################################################################

set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

# Set the current design
current_design memory_module

create_clock -name "clk320" -add -period 3.125 -waveform {0.0 1.5625} [get_ports clk]
set_clock_transition -min 0.1 [get_clocks clk320]
set_clock_transition -max 0.2 [get_clocks clk320]
create_clock -name "clk40" -add -period 25.0 -waveform {0.0 12.5} [get_ports bc_clk]
set_clock_transition -min 0.2 [get_clocks clk40]
set_clock_transition -max 0.8 [get_clocks clk40]
set_load -pin_load 0.1 [get_ports {search_bc_sync[2]}]
set_load -pin_load 0.1 [get_ports {search_bc_sync[1]}]
set_load -pin_load 0.1 [get_ports {search_bc_sync[0]}]
set_load -pin_load 0.1 [get_ports {read[15]}]
set_load -pin_load 0.1 [get_ports {read[14]}]
set_load -pin_load 0.1 [get_ports {read[13]}]
set_load -pin_load 0.1 [get_ports {read[12]}]
set_load -pin_load 0.1 [get_ports {read[11]}]
set_load -pin_load 0.1 [get_ports {read[10]}]
set_load -pin_load 0.1 [get_ports {read[9]}]
set_load -pin_load 0.1 [get_ports {read[8]}]
set_load -pin_load 0.1 [get_ports {read[7]}]
set_load -pin_load 0.1 [get_ports {read[6]}]
set_load -pin_load 0.1 [get_ports {read[5]}]
set_load -pin_load 0.1 [get_ports {read[4]}]
set_load -pin_load 0.1 [get_ports {read[3]}]
set_load -pin_load 0.1 [get_ports {read[2]}]
set_load -pin_load 0.1 [get_ports {read[1]}]
set_load -pin_load 0.1 [get_ports {read[0]}]
set_load -pin_load 0.1 [get_ports {data_out[27]}]
set_load -pin_load 0.1 [get_ports {data_out[26]}]
set_load -pin_load 0.1 [get_ports {data_out[25]}]
set_load -pin_load 0.1 [get_ports {data_out[24]}]
set_load -pin_load 0.1 [get_ports {data_out[23]}]
set_load -pin_load 0.1 [get_ports {data_out[22]}]
set_load -pin_load 0.1 [get_ports {data_out[21]}]
set_load -pin_load 0.1 [get_ports {data_out[20]}]
set_load -pin_load 0.1 [get_ports {data_out[19]}]
set_load -pin_load 0.1 [get_ports {data_out[18]}]
set_load -pin_load 0.1 [get_ports {data_out[17]}]
set_load -pin_load 0.1 [get_ports {data_out[16]}]
set_load -pin_load 0.1 [get_ports {data_out[15]}]
set_load -pin_load 0.1 [get_ports {data_out[14]}]
set_load -pin_load 0.1 [get_ports {data_out[13]}]
set_load -pin_load 0.1 [get_ports {data_out[12]}]
set_load -pin_load 0.1 [get_ports {data_out[11]}]
set_load -pin_load 0.1 [get_ports {data_out[10]}]
set_load -pin_load 0.1 [get_ports {data_out[9]}]
set_load -pin_load 0.1 [get_ports {data_out[8]}]
set_load -pin_load 0.1 [get_ports {data_out[7]}]
set_load -pin_load 0.1 [get_ports {data_out[6]}]
set_load -pin_load 0.1 [get_ports {data_out[5]}]
set_load -pin_load 0.1 [get_ports {data_out[4]}]
set_load -pin_load 0.1 [get_ports {data_out[3]}]
set_load -pin_load 0.1 [get_ports {data_out[2]}]
set_load -pin_load 0.1 [get_ports {data_out[1]}]
set_load -pin_load 0.1 [get_ports {data_out[0]}]
set_load -pin_load 0.1 [get_ports out_flag]
set_load -pin_load 0.1 [get_ports err_out]
set_false_path -from [get_clocks clk40] -to [get_clocks clk320]
set_false_path -from [get_clocks clk320] -to [get_clocks clk40]
set_false_path -from [get_clocks clk40] -to [list \
  [get_ports {read[15]}]  \
  [get_ports {read[14]}]  \
  [get_ports {read[13]}]  \
  [get_ports {read[12]}]  \
  [get_ports {read[11]}]  \
  [get_ports {read[10]}]  \
  [get_ports {read[9]}]  \
  [get_ports {read[8]}]  \
  [get_ports {read[7]}]  \
  [get_ports {read[6]}]  \
  [get_ports {read[5]}]  \
  [get_ports {read[4]}]  \
  [get_ports {read[3]}]  \
  [get_ports {read[2]}]  \
  [get_ports {read[1]}]  \
  [get_ports {read[0]}] ]
set_clock_groups -asynchronous -group [get_clocks clk320] -group [get_clocks clk40]
group_path -name C2C_normal_mode -from [list \
  [get_cells {inst_writing/pix_loop[1].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[2].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[3].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[5].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[6].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[7].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[8].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[9].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[11].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[13].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[14].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[15].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[0].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__0__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__2__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__3__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__5__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__1__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__4__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__17__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__12__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__13__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__14__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__15__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__16__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__11__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__18__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__19__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__20__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__21__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__22__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__23__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__8__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__9__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__10__]  \
  [get_cells inst_writing/inst_state/data_out_reg__0__]  \
  [get_cells inst_writing/inst_state/data_out_reg__1__]  \
  [get_cells inst_writing/inst_state/data_out_reg__2__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__0__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__1__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[15].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[14].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_group/data_out_reg__0__]  \
  [get_cells inst_writing/inst_group/data_out_reg__3__]  \
  [get_cells inst_writing/inst_group/data_out_reg__4__]  \
  [get_cells inst_writing/inst_group/data_out_reg__5__]  \
  [get_cells inst_writing/inst_group/data_out_reg__1__]  \
  [get_cells inst_writing/inst_group/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[10].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[11].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[12].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[13].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[9].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_finec/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__0__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__3__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__5__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__1__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__4__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__6__]  \
  [get_cells {inst_writing/pix_loop[10].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[12].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[4].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/inst_colid/data_out_reg__1__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__2__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__3__]  \
  [get_cells {inst_writing/col_loop[1].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[2].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[3].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[4].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[5].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[6].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[7].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[8].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[0].inst_col/data_out_reg}]  \
  [get_cells inst_writing/tail_ff/data_out_reg__0__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__1__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__0__]  \
  [get_cells inst_readout/bc_counter_reg_reg__1__]  \
  [get_cells inst_readout/bc_counter_reg_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__4__]  \
  [get_cells inst_readout/bc_counter_reg_reg__5__]  \
  [get_cells inst_readout/bc_counter_reg_reg__6__]  \
  [get_cells inst_readout/bc_counter_reg_reg__7__]  \
  [get_cells inst_readout/bc_counter_reg_reg__8__]  \
  [get_cells inst_readout/bc_counter_reg_reg__9__]  \
  [get_cells inst_readout/r_reg_reg__0__]  \
  [get_cells inst_readout/r_reg_reg__1__]  \
  [get_cells inst_readout/r_reg_reg__2__]  \
  [get_cells inst_readout/r_reg_reg__3__]  \
  [get_cells inst_readout/r_reg_reg__4__]  \
  [get_cells inst_readout/r_reg_reg__5__]  \
  [get_cells inst_readout/r_reg_reg__6__]  \
  [get_cells inst_readout/r_reg_reg__7__]  \
  [get_cells inst_readout/read_finished_out_reg]  \
  [get_cells inst_readout/state_reg__0__]  \
  [get_cells inst_readout/state_reg__1__]  \
  [get_cells inst_readout/state_reg__2__]  \
  [get_cells inst_readout/trig_flag_reg]  \
  [get_cells inst_readout/trig_id_reg__0__]  \
  [get_cells inst_readout/trig_id_reg__1__]  \
  [get_cells inst_readout/trig_id_reg__2__]  \
  [get_cells inst_readout/trig_id_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__4__]  \
  [get_cells inst_readout/trig_id_reg__5__]  \
  [get_cells inst_readout/trig_id_reg__6__]  \
  [get_cells inst_readout/trig_id_reg__7__]  \
  [get_cells inst_readout/trig_id_reg__9__]  \
  [get_cells inst_readout/trig_reg]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/read_cmd_reg]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__0__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__1__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__2__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__3__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__4__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__5__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__6__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__7__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__8__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__9__]  \
  [get_cells inst_trig_contr/state_reg]  \
  [get_cells inst_trig_contr/trig_flag_reg]  \
  [get_cells inst_writing/inst_data_flag/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__13__]  \
  [get_cells inst_writing/inst_data/data_out_reg__16__]  \
  [get_cells inst_writing/inst_data/data_out_reg__14__]  \
  [get_cells inst_writing/inst_data/data_out_reg__10__]  \
  [get_cells inst_writing/inst_data/data_out_reg__2__]  \
  [get_cells inst_writing/inst_data/data_out_reg__1__]  \
  [get_cells inst_writing/inst_data/data_out_reg__9__]  \
  [get_cells inst_writing/inst_data/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__17__]  \
  [get_cells inst_writing/inst_data/data_out_reg__3__]  \
  [get_cells inst_writing/inst_data/data_out_reg__7__]  \
  [get_cells inst_writing/inst_data/data_out_reg__15__]  \
  [get_cells inst_writing/inst_data/data_out_reg__12__]  \
  [get_cells inst_writing/inst_data/data_out_reg__6__]  \
  [get_cells inst_writing/inst_data/data_out_reg__5__]  \
  [get_cells inst_writing/inst_data/data_out_reg__11__]  \
  [get_cells inst_writing/inst_data/data_out_reg__4__]  \
  [get_cells inst_writing/inst_data/data_out_reg__8__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__5__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__4__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__2__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__1__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__3__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__0__]  \
  [get_cells inst_trig_contr/read_finished_reset_reg]  \
  [get_cells inst_trig_contr/search_flag_reg]  \
  [get_cells inst_readout/en_data_reg]  \
  [get_cells inst_readout/err_out_reg]  \
  [get_cells inst_readout/out_ff_reg__0__]  \
  [get_cells inst_readout/out_ff_reg__1__]  \
  [get_cells inst_readout/out_ff_reg__2__]  \
  [get_cells inst_readout/out_ff_reg__3__]  \
  [get_cells inst_readout/out_ff_reg__4__]  \
  [get_cells inst_readout/out_ff_reg__5__]  \
  [get_cells inst_readout/out_ff_reg__6__]  \
  [get_cells inst_readout/out_ff_reg__7__]  \
  [get_cells inst_readout/out_ff_reg__8__]  \
  [get_cells inst_readout/out_ff_reg__9__]  \
  [get_cells inst_readout/out_ff_reg__10__]  \
  [get_cells inst_readout/out_ff_reg__11__]  \
  [get_cells inst_readout/out_ff_reg__12__]  \
  [get_cells inst_readout/out_ff_reg__13__]  \
  [get_cells inst_readout/out_ff_reg__14__]  \
  [get_cells inst_readout/out_ff_reg__15__]  \
  [get_cells inst_readout/out_ff_reg__16__]  \
  [get_cells inst_readout/out_ff_reg__17__]  \
  [get_cells inst_readout/out_ff_reg__18__]  \
  [get_cells inst_readout/out_ff_reg__19__]  \
  [get_cells inst_readout/out_ff_reg__20__]  \
  [get_cells inst_readout/out_ff_reg__21__]  \
  [get_cells inst_readout/out_ff_reg__22__]  \
  [get_cells inst_readout/out_ff_reg__23__]  \
  [get_cells inst_readout/out_ff_reg__24__]  \
  [get_cells inst_readout/out_ff_reg__25__]  \
  [get_cells inst_readout/out_ff_reg__26__]  \
  [get_cells inst_readout/out_ff_reg__27__]  \
  [get_cells inst_readout/out_flag_ff_reg]  \
  [get_cells inst_readout/bc_counter_reg_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__8__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__0__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__1__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__3__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__0__] ] -to [list \
  [get_cells {inst_writing/pix_loop[1].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[2].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[3].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[5].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[6].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[7].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[8].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[9].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[11].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[13].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[14].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[15].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[0].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__0__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__2__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__3__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__5__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__1__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__4__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__17__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__12__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__13__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__14__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__15__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__16__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__11__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__18__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__19__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__20__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__21__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__22__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__23__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__8__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__9__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__10__]  \
  [get_cells inst_writing/inst_state/data_out_reg__0__]  \
  [get_cells inst_writing/inst_state/data_out_reg__1__]  \
  [get_cells inst_writing/inst_state/data_out_reg__2__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__0__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__1__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[15].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[14].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_group/data_out_reg__0__]  \
  [get_cells inst_writing/inst_group/data_out_reg__3__]  \
  [get_cells inst_writing/inst_group/data_out_reg__4__]  \
  [get_cells inst_writing/inst_group/data_out_reg__5__]  \
  [get_cells inst_writing/inst_group/data_out_reg__1__]  \
  [get_cells inst_writing/inst_group/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[10].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[11].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[12].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[13].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[9].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_finec/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__0__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__3__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__5__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__1__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__4__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__6__]  \
  [get_cells {inst_writing/pix_loop[10].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[12].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[4].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/inst_colid/data_out_reg__1__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__2__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__3__]  \
  [get_cells {inst_writing/col_loop[1].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[2].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[3].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[4].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[5].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[6].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[7].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[8].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[0].inst_col/data_out_reg}]  \
  [get_cells inst_writing/tail_ff/data_out_reg__0__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__1__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__0__]  \
  [get_cells inst_readout/bc_counter_reg_reg__1__]  \
  [get_cells inst_readout/bc_counter_reg_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__4__]  \
  [get_cells inst_readout/bc_counter_reg_reg__5__]  \
  [get_cells inst_readout/bc_counter_reg_reg__6__]  \
  [get_cells inst_readout/bc_counter_reg_reg__7__]  \
  [get_cells inst_readout/bc_counter_reg_reg__8__]  \
  [get_cells inst_readout/bc_counter_reg_reg__9__]  \
  [get_cells inst_readout/r_reg_reg__0__]  \
  [get_cells inst_readout/r_reg_reg__1__]  \
  [get_cells inst_readout/r_reg_reg__2__]  \
  [get_cells inst_readout/r_reg_reg__3__]  \
  [get_cells inst_readout/r_reg_reg__4__]  \
  [get_cells inst_readout/r_reg_reg__5__]  \
  [get_cells inst_readout/r_reg_reg__6__]  \
  [get_cells inst_readout/r_reg_reg__7__]  \
  [get_cells inst_readout/read_finished_out_reg]  \
  [get_cells inst_readout/state_reg__0__]  \
  [get_cells inst_readout/state_reg__1__]  \
  [get_cells inst_readout/state_reg__2__]  \
  [get_cells inst_readout/trig_flag_reg]  \
  [get_cells inst_readout/trig_id_reg__0__]  \
  [get_cells inst_readout/trig_id_reg__1__]  \
  [get_cells inst_readout/trig_id_reg__2__]  \
  [get_cells inst_readout/trig_id_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__4__]  \
  [get_cells inst_readout/trig_id_reg__5__]  \
  [get_cells inst_readout/trig_id_reg__6__]  \
  [get_cells inst_readout/trig_id_reg__7__]  \
  [get_cells inst_readout/trig_id_reg__9__]  \
  [get_cells inst_readout/trig_reg]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/read_cmd_reg]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__0__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__1__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__2__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__3__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__4__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__5__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__6__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__7__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__8__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__9__]  \
  [get_cells inst_trig_contr/state_reg]  \
  [get_cells inst_trig_contr/trig_flag_reg]  \
  [get_cells inst_writing/inst_data_flag/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__13__]  \
  [get_cells inst_writing/inst_data/data_out_reg__16__]  \
  [get_cells inst_writing/inst_data/data_out_reg__14__]  \
  [get_cells inst_writing/inst_data/data_out_reg__10__]  \
  [get_cells inst_writing/inst_data/data_out_reg__2__]  \
  [get_cells inst_writing/inst_data/data_out_reg__1__]  \
  [get_cells inst_writing/inst_data/data_out_reg__9__]  \
  [get_cells inst_writing/inst_data/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__17__]  \
  [get_cells inst_writing/inst_data/data_out_reg__3__]  \
  [get_cells inst_writing/inst_data/data_out_reg__7__]  \
  [get_cells inst_writing/inst_data/data_out_reg__15__]  \
  [get_cells inst_writing/inst_data/data_out_reg__12__]  \
  [get_cells inst_writing/inst_data/data_out_reg__6__]  \
  [get_cells inst_writing/inst_data/data_out_reg__5__]  \
  [get_cells inst_writing/inst_data/data_out_reg__11__]  \
  [get_cells inst_writing/inst_data/data_out_reg__4__]  \
  [get_cells inst_writing/inst_data/data_out_reg__8__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__5__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__4__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__2__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__1__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__3__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__0__]  \
  [get_cells inst_trig_contr/read_finished_reset_reg]  \
  [get_cells inst_trig_contr/search_flag_reg]  \
  [get_cells inst_readout/en_data_reg]  \
  [get_cells inst_readout/err_out_reg]  \
  [get_cells inst_readout/out_ff_reg__0__]  \
  [get_cells inst_readout/out_ff_reg__1__]  \
  [get_cells inst_readout/out_ff_reg__2__]  \
  [get_cells inst_readout/out_ff_reg__3__]  \
  [get_cells inst_readout/out_ff_reg__4__]  \
  [get_cells inst_readout/out_ff_reg__5__]  \
  [get_cells inst_readout/out_ff_reg__6__]  \
  [get_cells inst_readout/out_ff_reg__7__]  \
  [get_cells inst_readout/out_ff_reg__8__]  \
  [get_cells inst_readout/out_ff_reg__9__]  \
  [get_cells inst_readout/out_ff_reg__10__]  \
  [get_cells inst_readout/out_ff_reg__11__]  \
  [get_cells inst_readout/out_ff_reg__12__]  \
  [get_cells inst_readout/out_ff_reg__13__]  \
  [get_cells inst_readout/out_ff_reg__14__]  \
  [get_cells inst_readout/out_ff_reg__15__]  \
  [get_cells inst_readout/out_ff_reg__16__]  \
  [get_cells inst_readout/out_ff_reg__17__]  \
  [get_cells inst_readout/out_ff_reg__18__]  \
  [get_cells inst_readout/out_ff_reg__19__]  \
  [get_cells inst_readout/out_ff_reg__20__]  \
  [get_cells inst_readout/out_ff_reg__21__]  \
  [get_cells inst_readout/out_ff_reg__22__]  \
  [get_cells inst_readout/out_ff_reg__23__]  \
  [get_cells inst_readout/out_ff_reg__24__]  \
  [get_cells inst_readout/out_ff_reg__25__]  \
  [get_cells inst_readout/out_ff_reg__26__]  \
  [get_cells inst_readout/out_ff_reg__27__]  \
  [get_cells inst_readout/out_flag_ff_reg]  \
  [get_cells inst_readout/bc_counter_reg_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__8__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__0__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__1__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__3__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__0__] ]
group_path -name C2O_normal_mode -from [list \
  [get_cells {inst_writing/pix_loop[1].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[2].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[3].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[5].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[6].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[7].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[8].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[9].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[11].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[13].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[14].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[15].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[0].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__0__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__2__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__3__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__5__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__1__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__4__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__17__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__12__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__13__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__14__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__15__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__16__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__11__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__18__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__19__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__20__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__21__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__22__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__23__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__8__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__9__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__10__]  \
  [get_cells inst_writing/inst_state/data_out_reg__0__]  \
  [get_cells inst_writing/inst_state/data_out_reg__1__]  \
  [get_cells inst_writing/inst_state/data_out_reg__2__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__0__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__1__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[15].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[14].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_group/data_out_reg__0__]  \
  [get_cells inst_writing/inst_group/data_out_reg__3__]  \
  [get_cells inst_writing/inst_group/data_out_reg__4__]  \
  [get_cells inst_writing/inst_group/data_out_reg__5__]  \
  [get_cells inst_writing/inst_group/data_out_reg__1__]  \
  [get_cells inst_writing/inst_group/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[10].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[11].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[12].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[13].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[9].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_finec/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__0__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__3__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__5__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__1__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__4__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__6__]  \
  [get_cells {inst_writing/pix_loop[10].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[12].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[4].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/inst_colid/data_out_reg__1__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__2__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__3__]  \
  [get_cells {inst_writing/col_loop[1].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[2].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[3].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[4].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[5].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[6].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[7].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[8].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[0].inst_col/data_out_reg}]  \
  [get_cells inst_writing/tail_ff/data_out_reg__0__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__1__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__0__]  \
  [get_cells inst_readout/bc_counter_reg_reg__1__]  \
  [get_cells inst_readout/bc_counter_reg_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__4__]  \
  [get_cells inst_readout/bc_counter_reg_reg__5__]  \
  [get_cells inst_readout/bc_counter_reg_reg__6__]  \
  [get_cells inst_readout/bc_counter_reg_reg__7__]  \
  [get_cells inst_readout/bc_counter_reg_reg__8__]  \
  [get_cells inst_readout/bc_counter_reg_reg__9__]  \
  [get_cells inst_readout/r_reg_reg__0__]  \
  [get_cells inst_readout/r_reg_reg__1__]  \
  [get_cells inst_readout/r_reg_reg__2__]  \
  [get_cells inst_readout/r_reg_reg__3__]  \
  [get_cells inst_readout/r_reg_reg__4__]  \
  [get_cells inst_readout/r_reg_reg__5__]  \
  [get_cells inst_readout/r_reg_reg__6__]  \
  [get_cells inst_readout/r_reg_reg__7__]  \
  [get_cells inst_readout/read_finished_out_reg]  \
  [get_cells inst_readout/state_reg__0__]  \
  [get_cells inst_readout/state_reg__1__]  \
  [get_cells inst_readout/state_reg__2__]  \
  [get_cells inst_readout/trig_flag_reg]  \
  [get_cells inst_readout/trig_id_reg__0__]  \
  [get_cells inst_readout/trig_id_reg__1__]  \
  [get_cells inst_readout/trig_id_reg__2__]  \
  [get_cells inst_readout/trig_id_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__4__]  \
  [get_cells inst_readout/trig_id_reg__5__]  \
  [get_cells inst_readout/trig_id_reg__6__]  \
  [get_cells inst_readout/trig_id_reg__7__]  \
  [get_cells inst_readout/trig_id_reg__9__]  \
  [get_cells inst_readout/trig_reg]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/read_cmd_reg]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__0__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__1__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__2__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__3__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__4__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__5__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__6__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__7__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__8__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__9__]  \
  [get_cells inst_trig_contr/state_reg]  \
  [get_cells inst_trig_contr/trig_flag_reg]  \
  [get_cells inst_writing/inst_data_flag/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__13__]  \
  [get_cells inst_writing/inst_data/data_out_reg__16__]  \
  [get_cells inst_writing/inst_data/data_out_reg__14__]  \
  [get_cells inst_writing/inst_data/data_out_reg__10__]  \
  [get_cells inst_writing/inst_data/data_out_reg__2__]  \
  [get_cells inst_writing/inst_data/data_out_reg__1__]  \
  [get_cells inst_writing/inst_data/data_out_reg__9__]  \
  [get_cells inst_writing/inst_data/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__17__]  \
  [get_cells inst_writing/inst_data/data_out_reg__3__]  \
  [get_cells inst_writing/inst_data/data_out_reg__7__]  \
  [get_cells inst_writing/inst_data/data_out_reg__15__]  \
  [get_cells inst_writing/inst_data/data_out_reg__12__]  \
  [get_cells inst_writing/inst_data/data_out_reg__6__]  \
  [get_cells inst_writing/inst_data/data_out_reg__5__]  \
  [get_cells inst_writing/inst_data/data_out_reg__11__]  \
  [get_cells inst_writing/inst_data/data_out_reg__4__]  \
  [get_cells inst_writing/inst_data/data_out_reg__8__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__5__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__4__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__2__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__1__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__3__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__0__]  \
  [get_cells inst_trig_contr/read_finished_reset_reg]  \
  [get_cells inst_trig_contr/search_flag_reg]  \
  [get_cells inst_readout/en_data_reg]  \
  [get_cells inst_readout/err_out_reg]  \
  [get_cells inst_readout/out_ff_reg__0__]  \
  [get_cells inst_readout/out_ff_reg__1__]  \
  [get_cells inst_readout/out_ff_reg__2__]  \
  [get_cells inst_readout/out_ff_reg__3__]  \
  [get_cells inst_readout/out_ff_reg__4__]  \
  [get_cells inst_readout/out_ff_reg__5__]  \
  [get_cells inst_readout/out_ff_reg__6__]  \
  [get_cells inst_readout/out_ff_reg__7__]  \
  [get_cells inst_readout/out_ff_reg__8__]  \
  [get_cells inst_readout/out_ff_reg__9__]  \
  [get_cells inst_readout/out_ff_reg__10__]  \
  [get_cells inst_readout/out_ff_reg__11__]  \
  [get_cells inst_readout/out_ff_reg__12__]  \
  [get_cells inst_readout/out_ff_reg__13__]  \
  [get_cells inst_readout/out_ff_reg__14__]  \
  [get_cells inst_readout/out_ff_reg__15__]  \
  [get_cells inst_readout/out_ff_reg__16__]  \
  [get_cells inst_readout/out_ff_reg__17__]  \
  [get_cells inst_readout/out_ff_reg__18__]  \
  [get_cells inst_readout/out_ff_reg__19__]  \
  [get_cells inst_readout/out_ff_reg__20__]  \
  [get_cells inst_readout/out_ff_reg__21__]  \
  [get_cells inst_readout/out_ff_reg__22__]  \
  [get_cells inst_readout/out_ff_reg__23__]  \
  [get_cells inst_readout/out_ff_reg__24__]  \
  [get_cells inst_readout/out_ff_reg__25__]  \
  [get_cells inst_readout/out_ff_reg__26__]  \
  [get_cells inst_readout/out_ff_reg__27__]  \
  [get_cells inst_readout/out_flag_ff_reg]  \
  [get_cells inst_readout/bc_counter_reg_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__8__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__0__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__1__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__3__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__0__] ] -to [list \
  [get_ports {search_bc_sync[2]}]  \
  [get_ports {search_bc_sync[1]}]  \
  [get_ports {search_bc_sync[0]}]  \
  [get_ports {read[15]}]  \
  [get_ports {read[14]}]  \
  [get_ports {read[13]}]  \
  [get_ports {read[12]}]  \
  [get_ports {read[11]}]  \
  [get_ports {read[10]}]  \
  [get_ports {read[9]}]  \
  [get_ports {read[8]}]  \
  [get_ports {read[7]}]  \
  [get_ports {read[6]}]  \
  [get_ports {read[5]}]  \
  [get_ports {read[4]}]  \
  [get_ports {read[3]}]  \
  [get_ports {read[2]}]  \
  [get_ports {read[1]}]  \
  [get_ports {read[0]}]  \
  [get_ports {data_out[27]}]  \
  [get_ports {data_out[26]}]  \
  [get_ports {data_out[25]}]  \
  [get_ports {data_out[24]}]  \
  [get_ports {data_out[23]}]  \
  [get_ports {data_out[22]}]  \
  [get_ports {data_out[21]}]  \
  [get_ports {data_out[20]}]  \
  [get_ports {data_out[19]}]  \
  [get_ports {data_out[18]}]  \
  [get_ports {data_out[17]}]  \
  [get_ports {data_out[16]}]  \
  [get_ports {data_out[15]}]  \
  [get_ports {data_out[14]}]  \
  [get_ports {data_out[13]}]  \
  [get_ports {data_out[12]}]  \
  [get_ports {data_out[11]}]  \
  [get_ports {data_out[10]}]  \
  [get_ports {data_out[9]}]  \
  [get_ports {data_out[8]}]  \
  [get_ports {data_out[7]}]  \
  [get_ports {data_out[6]}]  \
  [get_ports {data_out[5]}]  \
  [get_ports {data_out[4]}]  \
  [get_ports {data_out[3]}]  \
  [get_ports {data_out[2]}]  \
  [get_ports {data_out[1]}]  \
  [get_ports {data_out[0]}]  \
  [get_ports out_flag]  \
  [get_ports err_out] ]
group_path -name I2C_normal_mode -from [list \
  [get_ports clk]  \
  [get_ports rstb]  \
  [get_ports bc_clk]  \
  [get_ports {pix_in[15]}]  \
  [get_ports {pix_in[14]}]  \
  [get_ports {pix_in[13]}]  \
  [get_ports {pix_in[12]}]  \
  [get_ports {pix_in[11]}]  \
  [get_ports {pix_in[10]}]  \
  [get_ports {pix_in[9]}]  \
  [get_ports {pix_in[8]}]  \
  [get_ports {pix_in[7]}]  \
  [get_ports {pix_in[6]}]  \
  [get_ports {pix_in[5]}]  \
  [get_ports {pix_in[4]}]  \
  [get_ports {pix_in[3]}]  \
  [get_ports {pix_in[2]}]  \
  [get_ports {pix_in[1]}]  \
  [get_ports {pix_in[0]}]  \
  [get_ports {groupid[5]}]  \
  [get_ports {groupid[4]}]  \
  [get_ports {groupid[3]}]  \
  [get_ports {groupid[2]}]  \
  [get_ports {groupid[1]}]  \
  [get_ports {groupid[0]}]  \
  [get_ports {finecounter[3]}]  \
  [get_ports {finecounter[2]}]  \
  [get_ports {finecounter[1]}]  \
  [get_ports {finecounter[0]}]  \
  [get_ports {valid[15]}]  \
  [get_ports {valid[14]}]  \
  [get_ports {valid[13]}]  \
  [get_ports {valid[12]}]  \
  [get_ports {valid[11]}]  \
  [get_ports {valid[10]}]  \
  [get_ports {valid[9]}]  \
  [get_ports {valid[8]}]  \
  [get_ports {valid[7]}]  \
  [get_ports {valid[6]}]  \
  [get_ports {valid[5]}]  \
  [get_ports {valid[4]}]  \
  [get_ports {valid[3]}]  \
  [get_ports {valid[2]}]  \
  [get_ports {valid[1]}]  \
  [get_ports {valid[0]}]  \
  [get_ports trig] ] -to [list \
  [get_cells {inst_writing/pix_loop[1].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[2].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[3].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[5].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[6].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[7].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[8].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[9].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[11].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[13].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[14].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[15].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[0].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__0__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__2__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__3__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__5__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__1__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__4__]  \
  [get_cells inst_writing/head_ff_ff/data_out_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__17__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__12__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__13__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__14__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__15__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__16__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__11__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__2__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__18__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__19__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__20__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__1__]  \
  [get_cells inst_writing/inst_wr_control/w_addr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__21__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__22__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__4__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__23__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__5__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__6__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__3__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__0__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__7__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__8__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__9__]  \
  [get_cells inst_writing/inst_wr_control/data_wr_reg__10__]  \
  [get_cells inst_writing/inst_state/data_out_reg__0__]  \
  [get_cells inst_writing/inst_state/data_out_reg__1__]  \
  [get_cells inst_writing/inst_state/data_out_reg__2__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__0__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__1__]  \
  [get_cells inst_writing/inst_search_bc/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[15].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[14].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_group/data_out_reg__0__]  \
  [get_cells inst_writing/inst_group/data_out_reg__3__]  \
  [get_cells inst_writing/inst_group/data_out_reg__4__]  \
  [get_cells inst_writing/inst_group/data_out_reg__5__]  \
  [get_cells inst_writing/inst_group/data_out_reg__1__]  \
  [get_cells inst_writing/inst_group/data_out_reg__2__]  \
  [get_cells {inst_writing/col_loop[10].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[11].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[12].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[13].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[9].inst_col/data_out_reg}]  \
  [get_cells inst_writing/inst_finec/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__0__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__2__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__3__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__5__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__1__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__4__]  \
  [get_cells inst_writing/pre_head_ff/data_out_reg__6__]  \
  [get_cells {inst_writing/pix_loop[10].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[12].inst_pix/data_out_reg}]  \
  [get_cells {inst_writing/pix_loop[4].inst_pix/data_out_reg}]  \
  [get_cells inst_writing/inst_colid/data_out_reg__1__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__2__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__3__]  \
  [get_cells {inst_writing/col_loop[1].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[2].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[3].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[4].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[5].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[6].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[7].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[8].inst_col/data_out_reg}]  \
  [get_cells {inst_writing/col_loop[0].inst_col/data_out_reg}]  \
  [get_cells inst_writing/tail_ff/data_out_reg__0__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__1__]  \
  [get_cells inst_writing/tail_ff/data_out_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__0__]  \
  [get_cells inst_readout/bc_counter_reg_reg__1__]  \
  [get_cells inst_readout/bc_counter_reg_reg__2__]  \
  [get_cells inst_readout/bc_counter_reg_reg__4__]  \
  [get_cells inst_readout/bc_counter_reg_reg__5__]  \
  [get_cells inst_readout/bc_counter_reg_reg__6__]  \
  [get_cells inst_readout/bc_counter_reg_reg__7__]  \
  [get_cells inst_readout/bc_counter_reg_reg__8__]  \
  [get_cells inst_readout/bc_counter_reg_reg__9__]  \
  [get_cells inst_readout/r_reg_reg__0__]  \
  [get_cells inst_readout/r_reg_reg__1__]  \
  [get_cells inst_readout/r_reg_reg__2__]  \
  [get_cells inst_readout/r_reg_reg__3__]  \
  [get_cells inst_readout/r_reg_reg__4__]  \
  [get_cells inst_readout/r_reg_reg__5__]  \
  [get_cells inst_readout/r_reg_reg__6__]  \
  [get_cells inst_readout/r_reg_reg__7__]  \
  [get_cells inst_readout/read_finished_out_reg]  \
  [get_cells inst_readout/state_reg__0__]  \
  [get_cells inst_readout/state_reg__1__]  \
  [get_cells inst_readout/state_reg__2__]  \
  [get_cells inst_readout/trig_flag_reg]  \
  [get_cells inst_readout/trig_id_reg__0__]  \
  [get_cells inst_readout/trig_id_reg__1__]  \
  [get_cells inst_readout/trig_id_reg__2__]  \
  [get_cells inst_readout/trig_id_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__4__]  \
  [get_cells inst_readout/trig_id_reg__5__]  \
  [get_cells inst_readout/trig_id_reg__6__]  \
  [get_cells inst_readout/trig_id_reg__7__]  \
  [get_cells inst_readout/trig_id_reg__9__]  \
  [get_cells inst_readout/trig_reg]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/bc_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/read_cmd_reg]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_counter_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__0__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__1__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__2__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__3__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__4__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__5__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__6__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__7__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__8__]  \
  [get_cells inst_trig_contr/search_id_reg_reg__9__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__0__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__1__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__2__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__3__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__4__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__5__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__6__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__7__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__8__]  \
  [get_cells inst_trig_contr/search_id_triggered_reg__9__]  \
  [get_cells inst_trig_contr/state_reg]  \
  [get_cells inst_trig_contr/trig_flag_reg]  \
  [get_cells inst_writing/inst_data_flag/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__13__]  \
  [get_cells inst_writing/inst_data/data_out_reg__16__]  \
  [get_cells inst_writing/inst_data/data_out_reg__14__]  \
  [get_cells inst_writing/inst_data/data_out_reg__10__]  \
  [get_cells inst_writing/inst_data/data_out_reg__2__]  \
  [get_cells inst_writing/inst_data/data_out_reg__1__]  \
  [get_cells inst_writing/inst_data/data_out_reg__9__]  \
  [get_cells inst_writing/inst_data/data_out_reg__0__]  \
  [get_cells inst_writing/inst_data/data_out_reg__17__]  \
  [get_cells inst_writing/inst_data/data_out_reg__3__]  \
  [get_cells inst_writing/inst_data/data_out_reg__7__]  \
  [get_cells inst_writing/inst_data/data_out_reg__15__]  \
  [get_cells inst_writing/inst_data/data_out_reg__12__]  \
  [get_cells inst_writing/inst_data/data_out_reg__6__]  \
  [get_cells inst_writing/inst_data/data_out_reg__5__]  \
  [get_cells inst_writing/inst_data/data_out_reg__11__]  \
  [get_cells inst_writing/inst_data/data_out_reg__4__]  \
  [get_cells inst_writing/inst_data/data_out_reg__8__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__5__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__4__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__2__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__1__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__3__]  \
  [get_cells inst_writing/inst_codeff/data_out_reg__0__]  \
  [get_cells inst_trig_contr/read_finished_reset_reg]  \
  [get_cells inst_trig_contr/search_flag_reg]  \
  [get_cells inst_readout/en_data_reg]  \
  [get_cells inst_readout/err_out_reg]  \
  [get_cells inst_readout/out_ff_reg__0__]  \
  [get_cells inst_readout/out_ff_reg__1__]  \
  [get_cells inst_readout/out_ff_reg__2__]  \
  [get_cells inst_readout/out_ff_reg__3__]  \
  [get_cells inst_readout/out_ff_reg__4__]  \
  [get_cells inst_readout/out_ff_reg__5__]  \
  [get_cells inst_readout/out_ff_reg__6__]  \
  [get_cells inst_readout/out_ff_reg__7__]  \
  [get_cells inst_readout/out_ff_reg__8__]  \
  [get_cells inst_readout/out_ff_reg__9__]  \
  [get_cells inst_readout/out_ff_reg__10__]  \
  [get_cells inst_readout/out_ff_reg__11__]  \
  [get_cells inst_readout/out_ff_reg__12__]  \
  [get_cells inst_readout/out_ff_reg__13__]  \
  [get_cells inst_readout/out_ff_reg__14__]  \
  [get_cells inst_readout/out_ff_reg__15__]  \
  [get_cells inst_readout/out_ff_reg__16__]  \
  [get_cells inst_readout/out_ff_reg__17__]  \
  [get_cells inst_readout/out_ff_reg__18__]  \
  [get_cells inst_readout/out_ff_reg__19__]  \
  [get_cells inst_readout/out_ff_reg__20__]  \
  [get_cells inst_readout/out_ff_reg__21__]  \
  [get_cells inst_readout/out_ff_reg__22__]  \
  [get_cells inst_readout/out_ff_reg__23__]  \
  [get_cells inst_readout/out_ff_reg__24__]  \
  [get_cells inst_readout/out_ff_reg__25__]  \
  [get_cells inst_readout/out_ff_reg__26__]  \
  [get_cells inst_readout/out_ff_reg__27__]  \
  [get_cells inst_readout/out_flag_ff_reg]  \
  [get_cells inst_readout/bc_counter_reg_reg__3__]  \
  [get_cells inst_readout/trig_id_reg__8__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__0__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__1__]  \
  [get_cells inst_writing/inst_finec/data_out_reg__3__]  \
  [get_cells inst_writing/inst_colid/data_out_reg__0__] ]
group_path -name I2O_normal_mode -from [list \
  [get_ports clk]  \
  [get_ports rstb]  \
  [get_ports bc_clk]  \
  [get_ports {pix_in[15]}]  \
  [get_ports {pix_in[14]}]  \
  [get_ports {pix_in[13]}]  \
  [get_ports {pix_in[12]}]  \
  [get_ports {pix_in[11]}]  \
  [get_ports {pix_in[10]}]  \
  [get_ports {pix_in[9]}]  \
  [get_ports {pix_in[8]}]  \
  [get_ports {pix_in[7]}]  \
  [get_ports {pix_in[6]}]  \
  [get_ports {pix_in[5]}]  \
  [get_ports {pix_in[4]}]  \
  [get_ports {pix_in[3]}]  \
  [get_ports {pix_in[2]}]  \
  [get_ports {pix_in[1]}]  \
  [get_ports {pix_in[0]}]  \
  [get_ports {groupid[5]}]  \
  [get_ports {groupid[4]}]  \
  [get_ports {groupid[3]}]  \
  [get_ports {groupid[2]}]  \
  [get_ports {groupid[1]}]  \
  [get_ports {groupid[0]}]  \
  [get_ports {finecounter[3]}]  \
  [get_ports {finecounter[2]}]  \
  [get_ports {finecounter[1]}]  \
  [get_ports {finecounter[0]}]  \
  [get_ports {valid[15]}]  \
  [get_ports {valid[14]}]  \
  [get_ports {valid[13]}]  \
  [get_ports {valid[12]}]  \
  [get_ports {valid[11]}]  \
  [get_ports {valid[10]}]  \
  [get_ports {valid[9]}]  \
  [get_ports {valid[8]}]  \
  [get_ports {valid[7]}]  \
  [get_ports {valid[6]}]  \
  [get_ports {valid[5]}]  \
  [get_ports {valid[4]}]  \
  [get_ports {valid[3]}]  \
  [get_ports {valid[2]}]  \
  [get_ports {valid[1]}]  \
  [get_ports {valid[0]}]  \
  [get_ports trig] ] -to [list \
  [get_ports {search_bc_sync[2]}]  \
  [get_ports {search_bc_sync[1]}]  \
  [get_ports {search_bc_sync[0]}]  \
  [get_ports {read[15]}]  \
  [get_ports {read[14]}]  \
  [get_ports {read[13]}]  \
  [get_ports {read[12]}]  \
  [get_ports {read[11]}]  \
  [get_ports {read[10]}]  \
  [get_ports {read[9]}]  \
  [get_ports {read[8]}]  \
  [get_ports {read[7]}]  \
  [get_ports {read[6]}]  \
  [get_ports {read[5]}]  \
  [get_ports {read[4]}]  \
  [get_ports {read[3]}]  \
  [get_ports {read[2]}]  \
  [get_ports {read[1]}]  \
  [get_ports {read[0]}]  \
  [get_ports {data_out[27]}]  \
  [get_ports {data_out[26]}]  \
  [get_ports {data_out[25]}]  \
  [get_ports {data_out[24]}]  \
  [get_ports {data_out[23]}]  \
  [get_ports {data_out[22]}]  \
  [get_ports {data_out[21]}]  \
  [get_ports {data_out[20]}]  \
  [get_ports {data_out[19]}]  \
  [get_ports {data_out[18]}]  \
  [get_ports {data_out[17]}]  \
  [get_ports {data_out[16]}]  \
  [get_ports {data_out[15]}]  \
  [get_ports {data_out[14]}]  \
  [get_ports {data_out[13]}]  \
  [get_ports {data_out[12]}]  \
  [get_ports {data_out[11]}]  \
  [get_ports {data_out[10]}]  \
  [get_ports {data_out[9]}]  \
  [get_ports {data_out[8]}]  \
  [get_ports {data_out[7]}]  \
  [get_ports {data_out[6]}]  \
  [get_ports {data_out[5]}]  \
  [get_ports {data_out[4]}]  \
  [get_ports {data_out[3]}]  \
  [get_ports {data_out[2]}]  \
  [get_ports {data_out[1]}]  \
  [get_ports {data_out[0]}]  \
  [get_ports out_flag]  \
  [get_ports err_out] ]
set_clock_gating_check -setup 0.0 
set_max_fanout 1.000 [get_ports clk]
set_max_fanout 1.000 [get_ports rstb]
set_max_fanout 1.000 [get_ports bc_clk]
set_max_fanout 1.000 [get_ports {pix_in[15]}]
set_max_fanout 1.000 [get_ports {pix_in[14]}]
set_max_fanout 1.000 [get_ports {pix_in[13]}]
set_max_fanout 1.000 [get_ports {pix_in[12]}]
set_max_fanout 1.000 [get_ports {pix_in[11]}]
set_max_fanout 1.000 [get_ports {pix_in[10]}]
set_max_fanout 1.000 [get_ports {pix_in[9]}]
set_max_fanout 1.000 [get_ports {pix_in[8]}]
set_max_fanout 1.000 [get_ports {pix_in[7]}]
set_max_fanout 1.000 [get_ports {pix_in[6]}]
set_max_fanout 1.000 [get_ports {pix_in[5]}]
set_max_fanout 1.000 [get_ports {pix_in[4]}]
set_max_fanout 1.000 [get_ports {pix_in[3]}]
set_max_fanout 1.000 [get_ports {pix_in[2]}]
set_max_fanout 1.000 [get_ports {pix_in[1]}]
set_max_fanout 1.000 [get_ports {pix_in[0]}]
set_max_fanout 1.000 [get_ports {groupid[5]}]
set_max_fanout 1.000 [get_ports {groupid[4]}]
set_max_fanout 1.000 [get_ports {groupid[3]}]
set_max_fanout 1.000 [get_ports {groupid[2]}]
set_max_fanout 1.000 [get_ports {groupid[1]}]
set_max_fanout 1.000 [get_ports {groupid[0]}]
set_max_fanout 1.000 [get_ports {finecounter[3]}]
set_max_fanout 1.000 [get_ports {finecounter[2]}]
set_max_fanout 1.000 [get_ports {finecounter[1]}]
set_max_fanout 1.000 [get_ports {finecounter[0]}]
set_max_fanout 1.000 [get_ports {valid[15]}]
set_max_fanout 1.000 [get_ports {valid[14]}]
set_max_fanout 1.000 [get_ports {valid[13]}]
set_max_fanout 1.000 [get_ports {valid[12]}]
set_max_fanout 1.000 [get_ports {valid[11]}]
set_max_fanout 1.000 [get_ports {valid[10]}]
set_max_fanout 1.000 [get_ports {valid[9]}]
set_max_fanout 1.000 [get_ports {valid[8]}]
set_max_fanout 1.000 [get_ports {valid[7]}]
set_max_fanout 1.000 [get_ports {valid[6]}]
set_max_fanout 1.000 [get_ports {valid[5]}]
set_max_fanout 1.000 [get_ports {valid[4]}]
set_max_fanout 1.000 [get_ports {valid[3]}]
set_max_fanout 1.000 [get_ports {valid[2]}]
set_max_fanout 1.000 [get_ports {valid[1]}]
set_max_fanout 1.000 [get_ports {valid[0]}]
set_max_fanout 1.000 [get_ports trig]
set_input_transition -min 0.4 [get_ports rstb]
set_input_transition -max 1.5 [get_ports rstb]
set_input_transition -min 0.4 [get_ports {valid[15]}]
set_input_transition -max 1.5 [get_ports {valid[15]}]
set_input_transition -min 0.4 [get_ports {valid[14]}]
set_input_transition -max 1.5 [get_ports {valid[14]}]
set_input_transition -min 0.4 [get_ports {valid[13]}]
set_input_transition -max 1.5 [get_ports {valid[13]}]
set_input_transition -min 0.4 [get_ports {valid[12]}]
set_input_transition -max 1.5 [get_ports {valid[12]}]
set_input_transition -min 0.4 [get_ports {valid[11]}]
set_input_transition -max 1.5 [get_ports {valid[11]}]
set_input_transition -min 0.4 [get_ports {valid[10]}]
set_input_transition -max 1.5 [get_ports {valid[10]}]
set_input_transition -min 0.4 [get_ports {valid[9]}]
set_input_transition -max 1.5 [get_ports {valid[9]}]
set_input_transition -min 0.4 [get_ports {valid[8]}]
set_input_transition -max 1.5 [get_ports {valid[8]}]
set_input_transition -min 0.4 [get_ports {valid[7]}]
set_input_transition -max 1.5 [get_ports {valid[7]}]
set_input_transition -min 0.4 [get_ports {valid[6]}]
set_input_transition -max 1.5 [get_ports {valid[6]}]
set_input_transition -min 0.4 [get_ports {valid[5]}]
set_input_transition -max 1.5 [get_ports {valid[5]}]
set_input_transition -min 0.4 [get_ports {valid[4]}]
set_input_transition -max 1.5 [get_ports {valid[4]}]
set_input_transition -min 0.4 [get_ports {valid[3]}]
set_input_transition -max 1.5 [get_ports {valid[3]}]
set_input_transition -min 0.4 [get_ports {valid[2]}]
set_input_transition -max 1.5 [get_ports {valid[2]}]
set_input_transition -min 0.4 [get_ports {valid[1]}]
set_input_transition -max 1.5 [get_ports {valid[1]}]
set_input_transition -min 0.4 [get_ports {valid[0]}]
set_input_transition -max 1.5 [get_ports {valid[0]}]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/TIEH_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/TIEL_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/HOLD_X1_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/ANTENNA_18_SVT]
set_clock_uncertainty -setup 0.1 [get_clocks clk320]
set_clock_uncertainty -hold 0.1 [get_clocks clk320]
set_clock_uncertainty -setup 1.0 [get_clocks clk40]
set_clock_uncertainty -hold 0.3 [get_clocks clk40]
