#---------------------------------------------------------------------------
# CERN digital implementation flow, Genus/Innovus 17.11
#---------------------------------------------------------------------------
if {[file exists /proc/cpuinfo]} {
  sh grep "model name" /proc/cpuinfo
  sh grep "cpu MHz"    /proc/cpuinfo
}

set times(start) [clock seconds]
puts "Hostname : [info hostname]"

####################################################################
## Load Design
####################################################################

# set_db / .hdl_language vhdl
set_db / .hdl_language sv

#### Including setup file (root attributes & setup variables).
include ../scripts/readout_setup_genus.tcl

# Use -define <MACRO> to specify any macros
# read_hdl -language vhdl $RTL_FILE_LIST_vhd
read_hdl -language sv $RTL_FILE_LIST_sv
# Read in pre-synthesized netlists
#read_hdl -netlist <NETLIST_FILES>

elaborate $DESIGN
#elaborate $DESIGN_sv
#elaborate $DESIGN_vhd
puts "Runtime & Memory after 'read_hdl'"
time_info Elaboration

set_db [get_db hinsts voter] .preserve true

check_design -unresolved

####################################################################
## Constraints Setup
####################################################################

set_db / .clp_treat_errors_as_warnings true

set sdc(normal_mode) "$_SDC_PATH/constraint.sdc"

## Use lines below to define multiple timing modes with multiple sdc files
#set sdc(normal_mode) "$_SDC_PATH/constraint.sdc $_SDC_PATH/normal_mode.sdc"
#set sdc(config_mode) "$_SDC_PATH/constraint.sdc $_SDC_PATH/config_mode.sdc"

set sdc_modes {normal_mode}
#set sdc_modes {normal_mode config_mode}

## Create different timing modes here
create_mode -default -name normal_mode -design $DESIGN
#create_mode -name config_mode -design $DESIGN

foreach mode $sdc_modes {
    set sdc_files $sdc($mode)
    foreach f $sdc_files {
        read_sdc $f -mode $mode -echo -verbose
    }
}

check_design -unresolved

#set_db "design:$DESIGN" .force_wireload <wireload name>

if {![file exists ${_LOG_PATH}]} {
  file mkdir ${_LOG_PATH}
  puts "Creating directory ${_LOG_PATH}"
}

if {![file exists ${_OUTPUTS_PATH}]} {
  file mkdir ${_OUTPUTS_PATH}
  puts "Creating directory ${_OUTPUTS_PATH}"
}

if {![file exists ${_REPORTS_PATH}]} {
  file mkdir ${_REPORTS_PATH}
  puts "Creating directory ${_REPORTS_PATH}"
}
## Report timing lint
set timing_modes [vfind / -mode *]
foreach mode  $timing_modes {
  report_timing -lint -mode $mode
}

###################################################################################
## Define cost groups (clock-clock, clock-output, input-clock, input-output)
###################################################################################

## Uncomment to remove already existing costgroups before creating new ones.
## delete_obj [vfind /designs/* -cost_group *]

if {[llength [all::all_seqs]] > 0} {
  set list_mod [vfind / -mode *]
  if {[llength $list_mod] >= 1} {
    foreach mode $list_mod {
      define_cost_group -name I2C_[vbasename $mode] -design $DESIGN
      define_cost_group -name C2O_[vbasename $mode] -design $DESIGN
      define_cost_group -name C2C_[vbasename $mode] -design $DESIGN
    }
  } else {
    define_cost_group -name I2C -design $DESIGN
    define_cost_group -name C2O -design $DESIGN
    define_cost_group -name C2C -design $DESIGN
  }

  foreach mode [vfind / -mode *] {
    path_group -from [all::all_seqs] -to [all::all_seqs] -group C2C_[vbasename $mode] \
        -name C2C_[vbasename $mode] -mode $mode
    path_group -from [all::all_seqs] -to [all::all_outs] -group C2O_[vbasename $mode] \
        -name C2O_[vbasename $mode] -mode $mode
    path_group -from [all::all_inps]  -to [all::all_seqs] -group I2C_[vbasename $mode] \
        -name I2C_[vbasename $mode] -mode $mode
  }
}

set list_mod [vfind / -mode *]
if {[llength $list_mod] >= 1} {
  foreach mode $list_mod {
    define_cost_group -name I2O_[vbasename $mode] -design $DESIGN
  }
} else {
  define_cost_group -name I2O -design $DESIGN
}
foreach mode [vfind / -mode *] {
  path_group -from [all::all_inps]  -to [all::all_outs] -group I2O_[vbasename $mode] \
      -name I2O_[vbasename $mode] -mode $mode
}

write_snapshot -outdir $_REPORTS_PATH -tag initial

report_summary -directory $_REPORTS_PATH

foreach cg [vfind / -cost_group *] {
  foreach mode [vfind / -mode *] {
    report_timing -cost_group [list $cg] -mode $mode >> $_REPORTS_PATH/${DESIGN}_pretim_${cg}.rpt
  }
}
####


#### uniquify the subdesign if it is multiple instantiated
#### and you would like to assign one of the instantiations
#### to a different library domain
#### edit_netlist uniquify <design|subdesign>

#### To turn off sequential merging on the design
#### uncomment & use the following attributes.
set_db / .optimize_merge_flops false
##set_db / .optimize_merge_latches false
#### For a particular instance use attribute 'optimize_merge_seqs' to turn off sequential merging.

####################################################################################################
## Synthesizing to generic
####################################################################################################

set_db / .syn_generic_effort $GEN_EFF
syn_generic
puts "Runtime & Memory after 'syn_generic'"
time_info GENERIC
report_dp > $_REPORTS_PATH/generic/${DESIGN}_datapath.rpt
write_snapshot -outdir $_REPORTS_PATH -tag generic
report_summary -directory $_REPORTS_PATH

####################################################################################################
## Synthesizing to gates
####################################################################################################

set_db / .syn_map_effort $MAP_OPT_EFF
syn_map
puts "Runtime & Memory after 'syn_map'"
time_info MAPPED

#set_db [get_db hinsts *] .ungroup_ok true

write_snapshot -outdir $_REPORTS_PATH -tag map
report_summary -directory $_REPORTS_PATH
report_dp > $_REPORTS_PATH/map/${DESIGN}_datapath.rpt

foreach cg [vfind / -cost_group *] {
  foreach mode [vfind / -mode *] {
    report_timing -cost_group [list $cg] -mode $mode >> $_REPORTS_PATH/${DESIGN}_[vbasename $cg]_post_map.rpt
  }
}

##Intermediate netlist for LEC verification..
write_hdl -lec > ${_OUTPUTS_PATH}/${DESIGN}_intermediate.v
write_do_lec -revised_design ${_OUTPUTS_PATH}/${DESIGN}_intermediate.v -logfile ${_LOG_PATH}/rtl2intermediate.lec.log > ${_OUTPUTS_PATH}/rtl2intermediate.lec.do

## ungroup -threshold <value>

#######################################################################################################
## Optimize Netlist
#######################################################################################################

## Uncomment to remove assigns & insert tiehilo cells during Incremental synthesis
set_db / .remove_assigns true
##set_remove_assign_options -buffer_or_inverter <libcell> -design <design|subdesign>
set_db / .use_tiehilo_for_const duplicate
set_db ui_respects_preserve false
#set_db add_tieoffs #-high TIEH_18_SVT -low TIEL_18_SVT lib_cell -max_10 -all


set_db / .syn_opt_effort $MAP_OPT_EFF
time_info OPT_START
syn_opt
write_snapshot -outdir $_REPORTS_PATH -tag syn_opt
report_summary -directory $_REPORTS_PATH

puts "Runtime & Memory after 'syn_opt'"
time_info OPT

foreach cg [vfind / -cost_group *] {
  foreach mode [vfind / -mode *] {
    report_timing -cost_group [list $cg] -mode $mode >> $_REPORTS_PATH/${DESIGN}_[vbasename $cg]_post_opt.rpt
  }
}

######################################################################################################
## write backend file set (verilog, SDC, config, etc.)
######################################################################################################

report_dp > $_REPORTS_PATH/${DESIGN}_datapath_incr.rpt
report_messages > $_REPORTS_PATH/${DESIGN}_messages.rpt
write_snapshot -outdir $_REPORTS_PATH -tag final
report_summary -directory $_REPORTS_PATH
write_db -to_file ${_OUTPUTS_PATH}/${DESIGN}.db

write_hdl  > ${_OUTPUTS_PATH}/r2g_writing.v

## write_script > ${_OUTPUTS_PATH}/${DESIGN}_m.script

foreach mode [vfind / -mode *] {
  write_sdc -mode $mode > ${_OUTPUTS_PATH}/r2g_writing.${mode}.sdc
}

if {[llength $timing_modes] == 0} {
    write_sdc > ${_OUTPUTS_PATH}/r2g_writing.sdc
} else {
    write_sdc -mode normal_mode > ${_OUTPUTS_PATH}/r2g_writing.default_mode.sdc
}

#################################
### write_do_lec
#################################

write_do_lec -golden_design ${_OUTPUTS_PATH}/${DESIGN}_intermediate.v -revised_design ${_OUTPUTS_PATH}/${DESIGN}_m.v -logfile  ${_LOG_PATH}/intermediate2final.lec.log > ${_OUTPUTS_PATH}/intermediate2final.lec.do

##Uncomment if the RTL is to be compared with the final netlist..
write_do_lec -revised_design ${_OUTPUTS_PATH}/${DESIGN}_m.v -logfile ${_LOG_PATH}/rtl2final.lec.log > ${_OUTPUTS_PATH}/rtl2final.lec.do

## Uncomment if SV wrapper should be created
#write_sv_wrapper -module_suffix Wrapper $DESIGN > ${_OUTPUTS_PATH}/${DESIGN}Wrapper.sv

set times(end) [clock seconds]
set syn_time [expr $times(end) - $times(start)]

puts "Final Runtime & Memory."
time_info FINAL
puts "============================"
puts "Synthesis Finished ........."
puts "  Total time: $syn_time s"
puts "============================"

file copy [get_db / .stdout_log] ${_LOG_PATH}/.

puts "To load a synthesized design, you can do 'read_db ../output/${DESIGN}.db'"

##quit
