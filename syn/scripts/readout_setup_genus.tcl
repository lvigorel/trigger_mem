#---------------------------------------------------------------------------
# CERN digital implementation flow, Genus/Innovus 17.11
#---------------------------------------------------------------------------

set DESIGN readout
# set DESIGN_writing readout
# set DESIGN_readout readout_corr
# set DESIGN_trig trigger_control
# set DESIGN_head_dec head_h_dec
# set DESIGN_bc_dec bc_h_dec

set GEN_EFF medium
set MAP_OPT_EFF medium

set DATE [clock format [clock seconds] -format "%b%d-%T"]

set _OUTPUTS_PATH ../output
set _REPORTS_PATH ../report
set _LOG_PATH ../log
set _SDC_PATH "../sdc/"

set RTL_FILE_LIST_sv "$DESIGN.sv"
# set RTL_FILE_LIST_vhd "$DESIGN_readout.vhd $DESIGN_head_dec.vhd $DESIGN_trig.vhd $DESIGN_bc_dec.vhd" 

set_db / .init_lib_search_path ". /projects/TOWER180/ALICEITS/IS_OA_53/workAreas/camarin/ALICEITS_OA/work_libs/user/cds/digital/tsl18fs190/lib/liberty/"
set_db / .script_search_path ". ../scripts"
set_db / .init_hdl_search_path ". ../../src"

set_db / .leakage_power_effort high

## Uncomment if SV wrapper should be created
#set_db / .write_sv_port_wrapper true

##Default undriven/unconnected setting is 'none'.
##set_db / .hdl_unconnected_input_port_value 0 | 1 | x | none
##set_db / .hdl_undriven_output_port_value   0 | 1 | x | none
##set_db / .hdl_undriven_signal_value        0 | 1 | x | none

##set_db / .wireload_mode <value>
set_db / .information_level 7

read_libs -max_libs "tsl18fs190svt_ss_1p62v_125c.lib"

## PLE
set_db / .lef_library "/vlsicad/micsoft/TOWER180/digital_libs/STD_CELLS/tsl18fs120_Rev_2014.12/tech/lef/6M1L/tsl180l6.lef /projects/TOWER180/ALICEITS/IS_OA_53/workAreas/camarin/ALICEITS_OA/work_libs/user/cds/digital/tsl18fs190/lib/lef/tsl18fs190svt.lef"

## Provide either cap_table_file or the qrc_tech_file
set_db / .qrc_tech_file "/projects/TOWER180/ALICEITS/IS_OA_5096/workAreas/camarin/ALICEITS_OA/work_libs/user/cds/digital/techfile/qrcTechFile"

##generates <signal>_reg[<bit_width>] format
set_db / .hdl_array_naming_style %s__%d__
##

## Comment out to disable clock gating
set_db / .lp_insert_clock_gating false
