#---------------------------------------------------------------------------
# CERN Digital implementation flow, Genus 2016
# gn_init.tcl
# Inits the tech- and design-specific variables before synthesis.
#---------------------------------------------------------------------------


proc report_time_and_memory {msg} {
    puts "\nINFO: Total cpu-time and memory after $msg: [get_attr runtime /] sec., [get_attr memory_usage /] MBytes.\n"
}

namespace eval ec {}

# start timer
puts "Start at: [clock format [clock seconds] -format {%x %X}]"
set ec::start [clock seconds]

#####################################################################
# Setup file, directories, and variables
#####################################################################

set ec::inDir           ../verilog
set ec::outDir          ../output
set ec::reportDir       ../report

set ec::SYN_EFFORT      high
set ec::MAP_EFFORT      high
set ec::INCR_EFFORT     high

set TS18_PDK $env(TS18IS_PDK)

set CORE_CHIP 	CORE
set DFT OFF

# Top module
set ec::DESIGN DTU_serializer

set ec::RTL_PATH        ../verilog/
set ec::LIB_PATH  "/projects/TOWER180/ALICEITS/IS_OA_52/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/library/tsl18fs190svt_Rev_2016.12/lib/liberty/"

set cpf_file ""

source ../data/tech_data.tcl

# Verilof netlist files
set ec::VERILOG_LIST    { }
set ec::VHDL_LIST       { DTU_serializer.vhd clk_syncronizer.vhd serializer.vhd shift_reg_triple.vhd voter.vhd multibit_voter.vhd updater.vhd}
set ec::VHDL_VERSION    1993
set ec::SDC      ../sdc/constraint.sdc

# If a message annoys you, add its ID here
set ec::SUPPRESS_MSG    {LBR-30 LBR-31 VLOGPT-35}

# include needed script
include load_etc.tcl

#####################################################################
# Preset global variables and attributes
#####################################################################

# define diagnostic variables
set_attribute iopt_stats 1 /
set_attribute map_fancy_names 1 /

# define tool setup and compatibility
set_attribute information_level 9 /  ; # valid range: 1 (least verbose) through 9 (most verbose)
set_attribute hdl_max_loop_limit 1024 /
set_attribute hdl_reg_naming_style %s_reg%s /
set_attribute gen_module_prefix G2C_DP_ /
# set_attribute endpoint_slack_opto 1 /
#set_attribute optimize_constant_0_flops false /
#set_attribute optimize_constant_1_flops false /
set_attribute input_pragma_keyword {cadence synopsys get2chip g2c} /
set_attribute synthesis_off_command translate_off /
set_attribute synthesis_on_command translate_on /
set_attribute input_case_cover_pragma {full_case} /
set_attribute input_case_decode_pragma {parallel_case} /
set_attribute input_synchro_reset_pragma sync_set_reset /
set_attribute input_synchro_reset_blk_pragma sync_set_reset_local /
set_attribute input_asynchro_reset_pragma async_set_reset /
set_attribute input_asynchro_reset_blk_pragma async_set_reset_local /
#set_attribute delayed_pragma_commands_interpreter dc /
set_attribute script_begin dc_script_begin /
set_attribute script_end dc_script_end /
set_attribute iopt_force_constant_removal true /


# GAR conventional naming styles from Matthew:
set_attribute hdl_array_naming_style %s_%d /
set_attribute hdl_record_naming_style %s_%s /
set_attribute hdl_generate_index_style %s_%d /
set_attribute hdl_generate_separator _ /


# GAR Generate error and stop when inferring a latch
set_attribute hdl_error_on_latch false


# suppress messages
suppress_messages $ec::SUPPRESS_MSG

# setup shrink_factor attribute
set_attribute shrink_factor 1.0 /

#####################################################################
# RTL and libraries setup
#####################################################################


# search paths
set_attribute hdl_search_path $ec::RTL_PATH /
set_attribute lib_search_path $ec::LIB_PATH /

# lef & captbl
set_attribute lef_library $ec::LEFLIB /
set_attribute cap_table_file $ec::CAPTABLE /

set_attribute interconnect_mode ple /

report_time_and_memory "SETUP"


### Power root attributes
#ikremast - 25/09/2017: lp_insert_clock_gating -> true
set_attribute lp_insert_clock_gating true /
set_attribute lp_clock_gating_prefix lpg /
set_attribute hdl_track_filename_row_col true /

## Power root attributes -NEW
##set_attribute lp_clock_gating_prefix <string> /
##set_attribute lp_operand_isolation_prefix <string> /
##set_attribute lp_power_analysis_effort <high> /
##set_attribute lp_power_unit mW /
##set_attribute lp_toggle_rate_unit /ns /
#set_attribute hdl_track_filename_row_col true /
#set_attribute leakage_power_effort low /

#####################################################################
# Load RTL
#####################################################################
#set_attribute hdl_language sv /
#read_hdl -sv $ec::VERILOG_LIST

set_attribute hdl_language vhdl
read_hdl -vhdl $ec::VHDL_LIST

#read_power_intent -cpf $cpf_file -module CLICTD

report_time_and_memory "LOAD"

#####################################################################
# Elaborate
#####################################################################

elaborate $ec::DESIGN

set_attribute preserve true [find /designs/DTU_serializer/subdesigns -subdesign "voter"]
#set_attribute preserve true [ find / -inst *INST_VOTER* ]

# GAR forcing not to merge the equivalent FFs (protect TMR)
set_attribute optimize_merge_flops false /

# GAR Make use of tiehi tielo cells
set_attribute use_tiehilo_for_const duplicate ;
set_attribute ignore_preserve_in_tiecell_insertion  true;
insert_tiehilo_cells -hi TIEH_18_SVT -lo TIEL_18_SVT -verbose -all -aon_hi TIEH_18_SVT -aon_lo TIEL_18_SVT

# GAR Remove all assigns 
set_attribute remove_assigns true ;
set_remove_assign_options -verbose 
set_remove_assign_options -ignore_preserve_setting 

# Prevent ungrouping of main top level instances, design specific settings 

#set_attribute ungroup_ok false [find -subdesign "reset_synchronizer"];


# GAR Prevent merging of decoding modules
set_attribute merge_combinational_hier_instances false ;
get_remove_assign_options -all

get_attribute number_of_routing_layer /designs/*
set_attribute number_of_routing_layers 6 /designs/*


if {$cpf_file != ""} {
    apply_power_intent
    commit_power_intent
}

report_time_and_memory "ELAB"

#####################################################################
# Constraint setup
#####################################################################

# Create different timing modes here
create_mode -default -name default_mode -design $ec::DESIGN
#create_mode -name config_mode -design $ec::DESIGN

# read sdc constraint
foreach ec::FILE_NAME $ec::SDC {
    read_sdc $ec::FILE_NAME -mode default_mode
}

report timing -lint

report_time_and_memory "CONSTRAINT"

#####################################################################
# Define cost groups (clock-clock, clock-output, input-clock, input-output)
#####################################################################

define_cost_group -name I2C
define_cost_group -name C2O
define_cost_group -name I2O
define_cost_group -name C2C
path_group -from [all des seqs] -to [all des outs] -group C2C -name C2C -mode default_mode
path_group -from [all des seqs] -to [all des outs] -group C2O -name C2O -mode default_mode
path_group -from [all des inps] -to [all des seqs] -group I2C -name I2C -mode default_mode
path_group -from [all des inps] -to [all des outs] -group I2O -name I2O -mode default_mode

#####################################################################
# Initial reports
#####################################################################

# print out the exceptions
set ec::XCEP [find /designs* -exception *]
puts "\nEC INFO: Total numbers of exceptions: [llength $ec::XCEP]\n"
catch {open $ec::reportDir/exception.all "w"} ec::FXCEP
puts $ec::FXCEP "Total numbers of exceptions: [llength $ec::XCEP]\n"
foreach ec::X $ec::XCEP {
  puts $ec::FXCEP $ec::X
}
close $ec::FXCEP

report_time_and_memory "POST-SDC"

# report initial design
report design > $ec::reportDir/init.design

# report initial gates
report gates > $ec::reportDir/init.gate

# report initial area
report area > $ec::reportDir/init.area

# report initial timing
report timing -full > $ec::reportDir/init.timing

# report initial timing groups
report timing -end -slack 0 > $ec::reportDir/init.timing.ep
report timing -from [dc::all_inputs] > $ec::reportDir/init.timing.in
report timing -to   [dc::all_outputs] > $ec::reportDir/init.timing.out
set ec::CNT 1
foreach ec::CLK [find /designs* -clock *] {
  exec echo "####################" > $ec::reportDir/init.timing.clk$ec::CNT
  exec echo "# from clock: $ec::CLK" >> $ec::reportDir/init.timing.clk$ec::CNT
  exec echo "# to clock: $ec::CLK" >> $ec::reportDir/init.timing.clk$ec::CNT
  exec echo "####################" >> $ec::reportDir/init.timing.clk$ec::CNT
  report timing -from $ec::CLK -to $ec::CLK >> $ec::reportDir/init.timing.clk$ec::CNT
  incr ec::CNT
}

# report initial summary
puts "\nEC INFO: Reporting Initial QoR below...\n"
redirect -tee $ec::reportDir/init.qor {report qor}
puts "\nEC INFO: Reporting Initial Summary below...\n"
redirect -tee $ec::reportDir/init.summary {report summary}

report timing -lint


