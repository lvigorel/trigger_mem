#---------------------------------------------------------------------------
# CERN Digital implementation flow for Genus, 2016
# gn_dft2.tcl
#---------------------------------------------------------------------------

### Optional additional DFT commands.
#set_compatible_test_clocks -all
connect_scan_chains -auto_create_chains
report dft_chains > $ec::reportDir/dft_chains.rpt

## report dft_setup > ${ec::DESIGN}-DFTsetup
#write_scandef > ../output/scan.def
#write_et_atpg -library foo.v -directory ${ec::DESIGN}
#
write_et_atpg -library foo.v -directory et_atpg
## write_dft_abstract_model > ${ec::DESIGN}-scanAbstract
## write_hdl -abstract > ${ec::DESIGN}-logicAbstract
