#---------------------------------------------------------------------------
# CERN Digital implementation flow, Genus 2016
# gn_syn.tcl
# Synthesizes design into generic logic and then maps it to the tech
# library cells.
#---------------------------------------------------------------------------

################################################
# Leakage/Dynamic power/Clock Gating setup
################################################
set_attribute max_leakage_power 0.0 "$ec::DESIGN"


################################################
# Synthesizing to gates, low effort to preserve
# all instances to set attribiutes
################################################

set_attribute syn_generic_effort low
syn_gen

#set_attribute auto_ungroup none


report datapath > $ec::reportDir/datapath_generic.rpt

################################################
# Synthesizing to gates
################################################

set_attribute syn_map_effort $ec::MAP_EFFORT
syn_map



puts "Runtime & Memory after 'synthesize -to_map -no_incr'"
report datapath > $ec::reportDir/datapath_mapped.rpt

file mkdir ../../lec
write_hdl -lec > ../../lec/top.vlec
# Write LEC file
write_do_lec -no_exit -revised_design ../../lec/top.vlec -log rtl2map.do.log > ../../lec/rtl2map.do
