#---------------------------------------------------------------------------
# CERN Digital implementation flow for Genus, 2016
#---------------------------------------------------------------------------

if {[catch {

	source ../scripts/gn_init.tcl
	report messages
	if {$DFT == "ON"} {source ../scripts/gn_dft.tcl}
	source ../scripts/gn_syn.tcl
	report messages
	if {$DFT == "ON"} {source ../scripts/gn_dft2.tcl}
	source ../scripts/gn_syn2.tcl
	report messages

	write_sdf > ../output/r2g.sdf	

	#####################################################################
	# BEGIN POSTAMBLE: DO NOT EDIT
	write_db -to_file $ec::outDir/opt.db
     write_design -innovus -base_name $ec::outDir/r2g
     write_design -base_name $ec::outDir/r2g

	# Write LEC file
	write_do_lec -no_exit -golden ../../lec/top.vlec -revised_design $ec::outDir/r2g.v -log map2final.do.log > ../../lec/map2final.do
    report messages

	# end timer
	puts "\nEC INFO: End at: [clock format [clock seconds] -format {%x %X}]"
	set ec::end [clock seconds]
	set ec::seconds [expr $ec::end - $ec::start]
	puts "\nEC INFO: Elapsed-time: $ec::seconds seconds\n"

} msg]} {
	puts "\nEC ERROR: RC could not finish successfully. Force an exit now. ($msg)\n"
}

