# ####################################################################

#  Created by Genus(TM) Synthesis Solution 16.22-s033_1 on Mon May 07 16:40:02 +0200 2018

# ####################################################################

set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

# Set the current design
current_design memory_module

create_clock [get_ports {clk}] -name clk320 -period 3.1250 -waveform {0 1.5625 }
set_clock_transition -min 0.1  [get_clocks {clk320}]
set_clock_transition -max 0.2  [get_clocks {clk320}]
set_clock_uncertainty -setup 0.100    [get_clocks clk320]
set_clock_uncertainty -hold  0.100    [get_clocks clk320]

create_clock [get_ports {bc_clk}]  -name clk40  -period 25   -waveform {0 12.5}
set_clock_transition -min 0.2 [get_clocks {clk40}]
set_clock_transition -max 0.8 [get_clocks {clk40}]
set_clock_uncertainty -setup 1.000 [get_clocks clk40]
set_clock_uncertainty -hold  0.300 [get_clocks clk40]

set_clock_groups -asynchronous -group {clk320}   -group {clk40}

#set_max_transition 0.1 [get_ports read*]


set_load 0.1 [all_outputs]
set_load 0.1 [get_port read*]

#set output_margin 0.000


########################################
# Input constraints
########################################
set_max_fanout 1 [all_inputs]

set_input_transition  -max 1.500 [get_port rstb]
set_input_transition  -min 0.400 [get_port rstb]

set_input_transition  -max 1.500 [get_port valid*]
set_input_transition  -min 0.400 [get_port valid*]


########################################
# Output constraints
########################################


set_output_transition  -max 0.200 [get_port read*]
set_output_transition  -min 0.05 [get_port read*]

########################################
# False paths
########################################
set_false_path -from [get_clocks clk40]    -to [get_clocks clk320]
set_false_path -from [get_clocks clk320]   -to [get_clocks clk40]
set_false_path -from [get_clocks clk40]   -to [get_ports read*]


set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/TIEH_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/TIEL_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/HOLD_X1_18_SVT]
set_dont_use [get_lib_cells tsl18fs190svt_ss_1p62v_125c/ANTENNA_18_SVT]
