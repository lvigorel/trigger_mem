#---------------------------------------------------------------------------
# CERN Digital implementation flow for Innovus, 2016
# TowerJazz180 file definitions for Genus synthesis
# MODIFIED: Camarin, 10/01/2018
#	    for TowerJazz flow
#---------------------------------------------------------------------------

set libType tsl18fs190svt
set voltages "ss_1p62v_125c tt_1p8v_25c ff_1p98v_m40c"

set ec::corner [dict create]
foreach cor $voltages {
    dict set ec::corner $cor "$libType\_$cor.lib"
}
#Setting corner
# 0 ss_1p62v_125c
# 1 tt_1p8v_25c
# 2 ff_1p96v_m40c
set set_corner 0

# Choose here the corner to use for synthesis
set ec::use_corner [lindex $ec::corner [expr 2*$set_corner+1]]

set ec::LIBRARY /homedir/camarin/Downloads/tsl18fs190svt_Rev_2017.07/lib/liberty/tsl18fs190svt_ss_1p62v_125c.lib

set ec::LEFLIB "/homedir/camarin/Downloads/tsl18fs190svt_Rev_2017.07/tech/lef/6M1L/tsl180l6.lef \ /projects/TOWER180/ALICEITS/IS_OA_52/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/library/tsl18fs190svt_Rev_2016.12/lib/lef/tsl18fs190svt.lef"

set ec::CAPTABLE /projects/TOWER180/ALICEITS/IS_OA_5096/workAreas/camarin/ALICEITS_OA/work_libs/user/cds/digital/techgen/TSL6ML_TC.CapTbl


# Create timing libraries
create_library_domain [lindex $ec::corner [expr 2*$set_corner]]
set_attribute library $ec::LIBRARY [lindex $ec::corner [expr 2*$set_corner]]
set_attribute default true [lindex $ec::corner [expr 2*$set_corner]]


