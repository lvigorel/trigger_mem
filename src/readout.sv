`timescale 1ns/1ps

module priority_enc(
     input [15:0] in,
     input clear_en,
     output logic [3:0]out,
     output logic [15:0]clear);//,
     // output logic flag);
     always_comb
     begin
          out<=(in[0])?4'h0:(in[1])?4'h1:(in[2])?4'h2:(in[3])?4'h3:(in[4])?4'h4:(in[5])?4'h5:(in[6])?4'h6:(in[7])?4'h7:(in[8])?4'h8:(in[9])?4'h9:(in[10])?4'ha:(in[11])?4'hb:(in[12])?4'hc:(in[13])?4'hd:(in[14])?4'he:(in[15])?4'hf:4'h0;
          clear<=(clear_en)?((in[0])?16'hfffe:(in[1])?16'hfffd:(in[2])?16'hfffb:(in[3])?16'hfff7:(in[4])?16'hffef:(in[5])?16'hffdf:(in[6])?16'hffbf:(in[7])?16'hff7f:(in[8])?16'hfeff:(in[9])?16'hfdff:(in[10])?16'hfbff:(in[11])?16'hf7ff:(in[12])?16'hefff:(in[13])?16'hdfff:(in[14])?16'hbfff:(in[15])?16'h7fff:16'hffff):16'hffff;
          // flag<=(|in)?1'b1:1'b0;
     end
endmodule

module opp_priority_enc(
     input [15:0] in,
     // input clear_en,
     output logic [3:0]out);
     // output logic [15:0]clear);
     always_comb
     begin
          out<=(in[15])?4'hf:(in[14])?4'he:(in[13])?4'hd:(in[12])?4'hc:(in[11])?4'hb:(in[10])?4'ha:(in[9])?4'h9:(in[8])?4'h8:(in[7])?4'h7:(in[6])?4'h6:(in[5])?4'h5:(in[4])?4'h4:(in[3])?4'h3:(in[2])?4'h2:(in[1])?4'h1:(in[0])?4'h0:4'h0;
          // clear<=(clear_en)?((in[0])?16'hfffe:(in[1])?16'hfffd:(in[2])?16'hfffb:(in[3])?16'hfff7:(in[4])?16'hffef:(in[5])?16'hffdf:(in[6])?16'hffbf:(in[7])?16'hff7f:(in[8])?16'hfeff:(in[9])?16'hfdff:(in[10])?16'hfbff:(in[11])?16'hf7ff:(in[12])?16'hefff:(in[13])?16'hdfff:(in[14])?16'hbfff:(in[15])?16'h7fff:16'hffff):16'hffff;
     end
endmodule

/*
module priority_dec(
    input [3:0] in,
    input enable,
    output logic [15:0]out);

    always_comb
    begin
        out<=(enable)?(in==4'h0)?16'hfffe:(in==4'h1)?16'hfffd:(in==4'h2)?16'hfffb:(in==4'h3)?16'hfff7:(in==4'h4)?16'hffef:(in==4'h5)?16'hffdf:(in==4'h6)?16'hffbf:(in==4'h7)?16'hff7f:(in==4'h8)?16'hfeff:(in==4'h9)?16'hfdff:(in==4'ha)?16'hfbff:(in==4'hb)?16'hf7ff:(in==4'hc)?16'hefff:(in==4'hd)?16'hdfff:(in==4'he)?16'hbfff:16'h7fff:16'hffff;
    end
endmodule
*/

module dec(
    input [3:0] in,
    input enable,
    output logic [15:0]out);

    always_comb
    begin
        out<=(enable)?(in==4'h0)?16'h0001:(in==4'h1)?16'h0002:(in==4'h2)?16'h0004:(in==4'h3)?16'h0008:(in==4'h4)?16'h0010:(in==4'h5)?16'h0020:(in==4'h6)?16'h0040:(in==4'h7)?16'h0080:(in==4'h8)?16'h0100:(in==4'h9)?16'h0200:(in==4'ha)?16'h0400:(in==4'hb)?16'h0800:(in==4'hc)?16'h1000:(in==4'hd)?16'h2000:(in==4'he)?16'h4000:16'h8000:16'h0000;
    end
endmodule


// module dec(
//     input [3:0] in,
//     input enable,
//     output logic [15:0]out);
//
//     always_comb
//     begin
//         out<=(enable)?(in==4'h0)?16'h0000:(in==4'h1)?16'h0001:(in==4'h2)?16'h0002:(in==4'h3)?16'h0004:(in==4'h4)?16'h0008:(in==4'h5)?16'h0010:(in==4'h6)?16'h0020:(in==4'h7)?16'h0040:(in==4'h8)?16'h0080:(in==4'h9)?16'h0100:(in==4'ha)?16'h0200:(in==4'hb)?16'h0400:(in==4'hc)?16'h0800:(in==4'hd)?16'h1000:(in==4'he)?16'h2000:16'h4000:16'h8000;
//     end
// endmodule

module hamming_enc(
  input [9:0]d,
  output logic [14:0]code);

  always_comb
  begin
    code[0] <= d[0] ^ d[1] ^ d[3] ^ d[4] ^ d[6] ^ d[8];// ^ d[10];
    code[1] <= d[0] ^ d[2] ^ d[3] ^ d[5] ^ d[6] ^ d[9];// ^ d[10];
    code[2] <= d[0];
    code[3] <= d[1] ^ d[2] ^ d[3] ^ d[7] ^ d[8] ^ d[9];// ^ d[10];
    code[4] <= d[1];
    code[5] <= d[2];
    code[6] <= d[3];
    code[7] <= d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9];// ^ d[10];
    code[8] <= d[4];
    code[9] <= d[5];
    code[10] <= d[6];
    code[11] <= d[7];
    code[12] <= d[8];
    code[13] <= d[9];
    // code[14] <= d[10];
    code[14] <= ^code[13:0];
    // code[16] <= d[11];
    // code[17] <= d[12];
    // code[18] <= d[13];
  end
endmodule


module readout#(
    parameter sc = 16,
    parameter head_w = 6,
    parameter fc_w = 4,
    parameter r_bc_w = 3,
    parameter full_bc_w = 10,
    parameter gr_w = 6,
    parameter addr_w=10,
    parameter height=200
    )
    (
    input clk,     //Clock
    input bc_clk,
    input rstb,     // Reset signal
    input [full_bc_w-1:0]full_bcid,
    // input [r_bc_w-1:0]in_bc_in,
    input [16-1:0]in_pix_in, // Pixel in per mem syn output
    input [gr_w-1:0]in_groupid, // Groupid per mem syn output
    input [fc_w-1:0]in_finecounter, // fine counter per mem sync output
    input [sc-1:0]valid, //assertion on one sync memory
    output logic [r_bc_w-1:0]search_bc, // bc we currently want to read
    output logic [sc-1:0]read, // Read from the sync mem
    output wire [head_w+4+4+gr_w+fc_w-1:0]data_wr,
    output logic [addr_w-1:0]w_addr);
    // pixel 20 to 5 where 20 is the msb
    logic [sc-1:0]to_enc;
    logic [5:0] next_head;

    //ff with all data for synchonisation..
    // logic [r_bc_w*sc-1:0]bc_in;
    // logic [16*sc-1:0]pix_in; // Pixel in per mem syn output
    // logic [gr_w*sc-1:0]groupid; // Groupid per mem syn output
    // logic [fc_w*sc-1:0]finecounter; // fine counter per mem sync output

    // logic pre_start;
    logic start;
    logic [3:0]colid, colid_reg, opp_colid;//, temp_colid;
    // logic [gr_w-1:0]temp_groupid;
    logic [gr_w-1:0]groupid_reg;
    // logic [fc_w-1:0]temp_finecounter;
    logic [fc_w-1:0]finecounter_reg;
    logic [2:0]state,newstate;
    logic [15:0]temp_pix,reg_pix;
    // logic [full_bc_w-1:0] full_bcid_ff;//full_temp_bc, reg_bc, bc_counter_reg, bc_counter_next, bc_counter_reg_following, bc_counter_next_following;// prev_bc;
    // logic [r_bc_w-1:0]temp_bc;
    logic [full_bc_w-r_bc_w-1:0]head_bc, pre_head_bc, head_bc_ff, pre_head_bc_ff;
    logic [r_bc_w-1:0]tail_bc_ff;
    //logic or_pix;
    // logic pre_or_pix;
    logic [15:0]to_sclr_0;
    logic [sc-1:0]to_sclr_1;
    logic [3:0]addr, opp_addr;
    logic pre_data_flag;
    // integer i;
    logic addr_en;//,col_en;

    logic data_flag;// flag_pix, flag_col;
    logic [4+4+gr_w+fc_w-1:0]pre_data_out; // Data out to long term mem

    logic [head_w-1:0]reg_head;
    logic [4+4+gr_w+fc_w-1:0]data_out;
    logic data_en;

    // logic [4+4+gr_w+fc_w-1:0]bcid_code;
    logic [14:0]bcid_code;
    logic [14:0]pre_bcid_code, post_bcid_code;

    logic [r_bc_w-1:0]search_bc_next;

    logic [15:0]pre_read;

    priority_enc
      inst_prienc_0(
        .in(reg_pix),
        .clear_en(addr_en),
        .out(addr),
        .clear(to_sclr_0));//,
        // .flag(flag_pix));

    opp_priority_enc
      opp_prienc_0(
        .in(reg_pix),
        .out(opp_addr));

    priority_enc
      inst_prienc_1(
        .in(to_enc),
        .clear_en(data_en),
        .out(colid),
        .clear(to_sclr_1));//,
        // .flag(flag_col));

    opp_priority_enc
      opp_prienc_1(
        .in(to_enc),
        .out(opp_colid));

    dec
      column_read(
      .in(colid),
      .enable(data_en),
      .out(pre_read));


    genvar j;
    generate
    for(j=0;j<16;j++)
    begin : pix_loop
        ff_sclr_en inst_pix(.clk(clk),.rstb(rstb),.sclrb(to_sclr_0[j]),.data_in(in_pix_in[j]),.data_out(reg_pix[j]),.en(data_en));
    end
    endgenerate

    genvar i;
    generate
    for(i=0;i<sc;i++)
    begin : col_loop
      ff_sclr_en inst_col(.clk(clk),.rstb(rstb),.sclrb(to_sclr_1[i]),.data_in(valid[i]),.data_out(to_enc[i]),.en(!start));
      always_comb
      begin
        read[i] <= pre_read[i] & (!clk);
      end
    end
    endgenerate


    ff #(.width(3)) inst_state(.clk(clk),.rstb(rstb),.data_in(newstate),.data_out(state));
    ff #(.width(r_bc_w)) inst_search_bc(.clk(clk),.rstb(rstb),.data_in(search_bc_next),.data_out(search_bc));

    ff_en #(.width(4)) inst_colid(.clk(clk),.rstb(rstb),.data_in(colid),.en(data_en),.data_out(colid_reg));
    ff_en #(.width(gr_w)) inst_group(.clk(clk),.rstb(rstb),.data_in(in_groupid),.en(data_en),.data_out(groupid_reg));
    ff_en #(.width(fc_w)) inst_finec(.clk(clk),.rstb(rstb),.data_in(in_finecounter),.en(data_en),.data_out(finecounter_reg));
    // ff_en #(.width(r_bc_w)) inst_bcid_tailff(.clk(clk),.rstb(rstb),.data_in(in_bc_in),.en(data_en),.data_out(reg_bc[r_bc_w-1:0]));

    // ff #(.width(full_bc_w-r_bc_w)) inst_bcid_headff(.clk(clk),.rstb(rstb),.data_in(full_temp_bc[full_bc_w-1:r_bc_w]),.data_out(reg_bc[full_bc_w-1:r_bc_w]));

    // ff #(.width(full_bc_w)) inst_bcid_count_ff(.clk(clk),.rstb(rstb),.data_in(bc_counter_next),.data_out(bc_counter_reg));
    // ff #(.width(full_bc_w)) inst_bcid_count_fol_ff(.clk(clk),.rstb(rstb),.data_in(bc_counter_next_following),.data_out(bc_counter_reg_following));

    always_comb
    begin
        start<=|to_enc;
    end

////////////////////////////////////////////////////////////////////////////////////////////////
    hamming_enc post_bcid_h_enc(.d({{head_bc_ff},{search_bc}}),.code(post_bcid_code));
    hamming_enc pre_bcid_h_enc(.d({{pre_head_bc_ff},{search_bc}}),.code(pre_bcid_code));
////////////////////////////////////////////////////////////////////////////////////////////////

    ff #(.width(4+4+gr_w+fc_w)) inst_data(.clk(clk),.rstb(rstb),.data_in(pre_data_out),.data_out(data_out));
    ff #(.width(1)) inst_data_flag(.clk(clk),.rstb(rstb),.data_in(pre_data_flag),.data_out(data_flag));
    ff #(.width(6)) inst_codeff(.clk(clk),.rstb(rstb),.data_in(next_head),.data_out(reg_head));
    // ff_en #(.width(full_bc_w)) inst_bcidin_ff(.clk(clk),.rstb(rstb),.data_in(full_bcid),.en(!bc_clk),.data_out(full_bcid_ff));

    ff_en #(.width(full_bc_w-r_bc_w)) head_ff_ff(.clk(clk),.rstb(rstb),.data_in(head_bc),.en(bc_clk),.data_out(head_bc_ff));
    ff_en #(.width(full_bc_w-r_bc_w)) pre_head_ff(.clk(clk),.rstb(rstb),.data_in(pre_head_bc),.en(bc_clk),.data_out(pre_head_bc_ff));

    ff_en #(.width(r_bc_w)) tail_ff(.clk(clk),.rstb(rstb),.data_in(full_bcid[r_bc_w-1:0]),.en(bc_clk),.data_out(tail_bc_ff));


    always_comb
    begin
      //completing the bc..
      //In fast memory there are only 3 bits for the BCID, here we add the
      //other bits according to the value of the current BC.
      //If the bits stored have a value higher than that of the last bits of the current BCID
      //then it means that they belong to the previous BC and we assign pre_head_bc
      head_bc<=full_bcid[full_bc_w-1:r_bc_w];
      pre_head_bc<=full_bcid[full_bc_w-1:r_bc_w]-7'b1;
      if (search_bc > tail_bc_ff) //(full_temp_bc[r_bc_w-1:0] > tail_bc_ff) // full_bcid_ff[r_bc_w-1:0])
      begin
        bcid_code[14:0] <= pre_bcid_code;
        // full_temp_bc[full_bc_w-1:r_bc_w]<=pre_head_bc;
      end
      else
      begin
        bcid_code[14:0] <= post_bcid_code;
        // full_temp_bc[full_bc_w-1:r_bc_w]<=head_bc;
      end
      // full_temp_bc[r_bc_w-1:0]<=in_bc_in;

      //here is prepared the previous bc_id, to be stored in case there are no hits
      // prev_bc <= full_temp_bc - 11'b1;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
      case(state)
        ////////////////////////
        // setup and before writing code
        ////////////////////////
        3'h0, 3'h7: begin
          addr_en <= 1'b0;

          if(start==1'b1)
          begin
            data_en<=1'b1;
            pre_data_flag <= 1'b1;
            next_head <= 6'b011110;
            pre_data_out <= {3'h0,bcid_code};
            search_bc_next<=search_bc;
            if(colid==opp_colid)
              newstate<=3'h4;
            else
              newstate<=3'h3;
          end
          else // if(start==1'b0)
          begin
            newstate<=3'h0;
            data_en<=1'b0;
            pre_data_flag <= 1'b0;
            next_head <= '0;
            pre_data_out <= '0;
            if(search_bc==tail_bc_ff)
            begin
              search_bc_next<=search_bc;
            end
            else
            begin
              search_bc_next<=search_bc+3'h1;
            end
          end
        end
        ////////////////////////
        // just after read
        ////////////////////////
        3'h1: begin
          addr_en <= 1'b0;

          pre_data_flag <= 1'b0;
          next_head <= '0;
          pre_data_out <= '0;

          if(start==1'b1)
          begin
            data_en <= 1'b1;
            search_bc_next<=search_bc;
            if(colid==opp_colid)
              newstate<=3'h6;
            else
              newstate<=3'h5;
          end
          else //if(start==1'b0)
          begin
            data_en <= 1'b0;
            if(search_bc==tail_bc_ff)
            begin
              search_bc_next<=search_bc;
              newstate<=3'h1;
            end
            else
            begin
              search_bc_next<=search_bc+3'h1;
              newstate<=3'h2;
            end
          end
        end
        ////////////////////////
        // one BC after read
        ////////////////////////
        3'h2: begin
          addr_en <= 1'b0;

          pre_data_flag <= 1'b0;
          next_head <= '0;
          pre_data_out <= '0;

          if(start==1'b1)
          begin
            data_en <= 1'b1;
            search_bc_next<=search_bc;
            if(colid==opp_colid)
              newstate<=3'h4;
            else
              newstate<=3'h3;
          end
          else //if(start==1'b0)
          begin
            data_en <= 1'b0;
            if(search_bc==tail_bc_ff)
            begin
              search_bc_next<=search_bc;
              newstate<=3'h2;
            end
            else
            begin
              search_bc_next<=search_bc+3'h1;
              newstate<=3'h0;
            end
          end
        end
        ////////////////////////
        // write 10 code (and go to read)
        ////////////////////////
        3'h3: begin
          addr_en <= 1'b0;

          pre_data_flag<=1'b1;
          next_head<=6'b111001;
          pre_data_out<={addr,colid_reg,groupid_reg,finecounter_reg};
          search_bc_next<=search_bc;
          newstate<=3'h5;

          if(addr==opp_addr)
          begin
            data_en<=1'b1;
            addr_en<=1'b0;
          end
          else
          begin
            data_en<=1'b0;
            addr_en<=1'b1;
          end

        end
        ////////////////////////
        // write 10 code for 1 column only and go to finish read
        ////////////////////////
        3'h4: begin
          addr_en <= 1'b0;

          pre_data_flag<=1'b1;
          next_head<=6'b111001;
          pre_data_out<={addr,colid_reg,groupid_reg,finecounter_reg};
          search_bc_next<=search_bc;
          data_en<=1'b0;
          if(addr==opp_addr)
          begin
            newstate<=3'h1;
            addr_en<=1'b0;
          end
          else
          begin
            newstate<=3'h6;
            addr_en<=1'b1;
          end

        end
        ////////////////////////
        // read
        ////////////////////////
        3'h5: begin
          pre_data_flag<=1'b1;
          next_head<=6'b000000;
          pre_data_out<={addr,colid_reg,groupid_reg,finecounter_reg};
          search_bc_next<=search_bc;
          if(addr==opp_addr)
          begin
            data_en<=1'b1;
            addr_en<=1'b0;

            if(colid==opp_colid)
              newstate<=3'h6;
            else
              newstate<=3'h5;
          end
          else
          begin
            data_en<=1'b0;
            addr_en<=1'b1;

            newstate<=3'h5;
          end
        end
        ////////////////////////
        // finish read
        ////////////////////////
        3'h6: begin
          data_en<=1'b0;
          pre_data_flag<=1'b1;
          next_head<=6'b000000;
          pre_data_out<={addr,colid_reg,groupid_reg,finecounter_reg};
          search_bc_next<=search_bc;
          if(addr==opp_addr)
          begin
            addr_en<=1'b0;

            newstate<=3'h1;
          end
          else
          begin
            addr_en<=1'b1;

            newstate<=3'h6;
          end
        end

        /*default: begin
          data_en <= 1'b0;
          pre_data_flag <= 1'b0;
          next_head <= 6'b000000;
          pre_data_out <= '0;
          addr_en <= 1'b0;
          newstate <=3'h0;
          search_bc_next<=search_bc;
        end*/
      endcase
    ////////////////////////////////////////////////////////////////////////////////////////////////
//      bc_counter_next_following <= bc_counter_next+11'h001;
    end

    write_control #(
      .addr_w(addr_w),
      .height(height),
      .d_width(4+4+gr_w+fc_w),
      .h_width(head_w))
    inst_wr_control(
      .clk(clk),
      .rstb(rstb),
      .data_in(data_out),
      .head_in(reg_head),
      .d_flag(data_flag),
      .data_wr(data_wr),
      .w_addr(w_addr));

endmodule

module ff_sclr_en(
     input clk,
     input rstb,
     input en,
     input sclrb,
     input data_in,
     output logic data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
            data_out<=1'b0;
          else
          if(!sclrb)
            data_out<=1'b0;
          else
            if(en)
              data_out<=data_in;
     end

endmodule

module ff_sclr(
     input clk,
     input rstb,
     input sclrb,
     input data_in,
     output logic data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
            data_out<=1'b0;
          else
          if(!sclrb)
            data_out<=1'b0;
          else
            data_out<=data_in;
    end

endmodule

module ff #(parameter width=2)(
     input clk,
     input rstb,
     input [width -1:0]data_in,
     output logic [width -1:0]data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
               data_out<='0;
          else
             data_out<=data_in;
     end
endmodule

module ff_en #(parameter width=2)(
     input clk,
     input rstb,
     input [width -1:0]data_in,
     input en,
     output logic [width -1:0]data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
               data_out<='0;
          else
          if(en)
             data_out<=data_in;
     end
endmodule


module write_control #(
    parameter addr_w=10,
    parameter height=850,
    parameter d_width=17,
    parameter h_width=6
  )
  (
  input clk,
  input rstb,
  input [d_width-1:0]data_in,
  input [h_width-1:0]head_in,
  input d_flag,
  output logic [h_width+d_width-1:0]data_wr,
  output logic [addr_w-1:0]w_addr
  );

  logic [addr_w-1:0]w_next;

  always_ff @(posedge clk)
  begin
    if(!rstb)
    begin
      data_wr<={{6'b011110},{18'h08007}};
      w_addr<='0;
    end
    else
    if(d_flag)
    begin
      // we can't write faster than internal write speed
      data_wr<={head_in,data_in};
      w_addr<=w_next;
    end
  end

  always_comb
  begin
    // data_wr<={head_in,data_in};
    if(w_addr==height-1)
      w_next<='0;
    else
      w_next<=w_addr+8'h001;
  end

endmodule
