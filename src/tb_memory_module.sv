`timescale 1ns/10ps

module tb_memory_module ();
     localparam FINE=64'hd;
     localparam GROUP=80'h1d;
     localparam DELAY=25;
     logic clk;
     logic rstb;
     logic [15:0]data;
     logic [16*16-1:0]data_array;
     logic [15:0]valid;
     logic [11*16-1:0]bc_in;
     logic [5:0]head_out;
     logic read_reset;
     logic read_cmd;
     logic trig_flag;
     logic [10:0]trig_id;

     logic [27:0]data_out;
     logic data_flag;
     logic search_finished;
     logic read_finished;
     logic err;
     logic read_cmd_reset;



     clk #(.period(1.66)) inst_clk(.clk(clk));
     memory_module#(.sc (16),
     .fc_w(4),
     .bc_w(11),
     .gr_w(5),
     .head_w(6),
     .height(100),
     .addr_w(10)
     )inst_mem_mod(
               .clk(clk),
               .rstb(rstb),
               .bc_in(bc_in),
               .pix_in(data_array),
               .groupid(GROUP),
               .finecounter(FINE),
               .valid(valid),
               .read_reset(read_reset),
               .read_cmd(read_cmd),
               .trig_flag(trig_flag),
               .trig_id(trig_id),
               .read(),

               .data_out(data_out),
               .out_flag(data_flag),
               .search_finished(search_finished),
               .read_finished(read_finished),
               .err_out(err_out),
               .read_cmd_reset(read_cmd_reset));
     initial
     begin
		fork
		begin
	 	  // Reset
			rstb=1'b1;
			#(DELAY*5); rstb=1'b0;
			#(DELAY*1); rstb=1'b1;
		end
		begin

			data = '0;
              data_array={16{data}};
              valid = '0;
			#(DELAY*5);
              bc_in={16{11'h000}};
              data=16'h0010;
              data_array={{15{16'h0000}},{data}};
              valid=16'h0001;
			#(DELAY*2);
              read_cmd=1'b1;
              trig_flag=1'b1;
              bc_in={16{11'h001}};
              data=16'h0120;
              data_array={16{data}};
              valid=16'h4006;
      #(DELAY);
              trig_id=11'h000;
              trig_flag=1'b0;
              read_cmd=1'b1;
              bc_in={16{11'h003}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(DELAY);
              read_cmd=1'b1;
              trig_flag=1'b1;
              bc_in={16{11'h004}};
              data=16'h2000;
              data_array={{6{data}},{10{16'h0000}}};
              valid=16'h3004;
      #(DELAY);
              trig_id=11'h001;
              trig_flag=1'b0;
              read_cmd=1'b1;
              bc_in={16{11'h005}};
              data=16'h0200;
              data_array={{5{data}},{11{16'h0000}}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h001;
              trig_flag=1'b0;
              read_cmd=1'b0;
              bc_in={16{11'h006}};
              data=16'h0040;
              data_array={{12{16'h0000}},{4{data}}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h002;
              trig_flag=1'b0;
              read_cmd=1'b1;
              bc_in={16{11'h007}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h002;
              trig_flag=1'b0;
              read_cmd=1'b1;
              bc_in={16{11'h008}};
              data=16'h2040;
              data_array={{data},{12{16'h0000}},{data},{2{16'h0000}}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h003;
              trig_flag=1'b0;
              read_cmd=1'b0;
              bc_in={16{11'h009}};
              data=16'h2040;
              data_array={16{data}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h004;
              trig_flag=1'b0;
              read_cmd=1'b1;
              bc_in={16{11'h00a}};
              data=16'h2040;
              data_array={16{data}};
              valid=16'hffff;
      #(DELAY);
              trig_id=11'h005;
              trig_flag=1'b0;
              read_cmd=1'b0;
              bc_in={16{11'h00b}};
              data=16'h2040;
              data_array={16{data}};
              valid=16'hffff;

			#(DELAY*2);
          end
          join
          $finish;
     end


endmodule

//Period on NS
module clk#(parameter period=1)(
     output logic clk);

     initial
          clk=0;

     always
     begin
          #(period)clk<=!clk;
     end
endmodule
