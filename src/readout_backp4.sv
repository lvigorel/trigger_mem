`timescale 1ns/1ps

module priority_enc(
     input [15:0] in,
     input clear_en,
     output logic [3:0]out,
     output logic [15:0]clear);
     always_comb
     begin
          out<=(in[0])?4'h0:(in[1])?4'h1:(in[2])?4'h2:(in[3])?4'h3:(in[4])?4'h4:(in[5])?4'h5:(in[6])?4'h6:(in[7])?4'h7:(in[8])?4'h8:(in[9])?4'h9:(in[10])?4'ha:(in[11])?4'hb:(in[12])?4'hc:(in[13])?4'hd:(in[14])?4'he:(in[15])?4'hf:4'h0;
          clear<=(clear_en)?((in[0])?16'hfffe:(in[1])?16'hfffd:(in[2])?16'hfffb:(in[3])?16'hfff7:(in[4])?16'hffef:(in[5])?16'hffdf:(in[6])?16'hffbf:(in[7])?16'hff7f:(in[8])?16'hfeff:(in[9])?16'hfdff:(in[10])?16'hfbff:(in[11])?16'hf7ff:(in[12])?16'hefff:(in[13])?16'hdfff:(in[14])?16'hbfff:(in[15])?16'h7fff:16'hffff):16'hffff;
     end
endmodule

module opp_priority_enc(
     input [15:0] in,
     // input clear_en,
     output logic [3:0]out);
     // output logic [15:0]clear);
     always_comb
     begin
          out<=(in[15])?4'hf:(in[14])?4'he:(in[13])?4'hd:(in[12])?4'hc:(in[11])?4'hb:(in[10])?4'ha:(in[9])?4'h9:(in[8])?4'h8:(in[7])?4'h7:(in[6])?4'h6:(in[5])?4'h5:(in[4])?4'h4:(in[3])?4'h3:(in[2])?4'h2:(in[1])?4'h1:(in[0])?4'h0:4'h0;
          // clear<=(clear_en)?((in[0])?16'hfffe:(in[1])?16'hfffd:(in[2])?16'hfffb:(in[3])?16'hfff7:(in[4])?16'hffef:(in[5])?16'hffdf:(in[6])?16'hffbf:(in[7])?16'hff7f:(in[8])?16'hfeff:(in[9])?16'hfdff:(in[10])?16'hfbff:(in[11])?16'hf7ff:(in[12])?16'hefff:(in[13])?16'hdfff:(in[14])?16'hbfff:(in[15])?16'h7fff:16'hffff):16'hffff;
     end
endmodule

/*
module priority_dec(
    input [3:0] in,
    input enable,
    output logic [15:0]out);

    always_comb
    begin
        out<=(enable)?(in==4'h0)?16'hfffe:(in==4'h1)?16'hfffd:(in==4'h2)?16'hfffb:(in==4'h3)?16'hfff7:(in==4'h4)?16'hffef:(in==4'h5)?16'hffdf:(in==4'h6)?16'hffbf:(in==4'h7)?16'hff7f:(in==4'h8)?16'hfeff:(in==4'h9)?16'hfdff:(in==4'ha)?16'hfbff:(in==4'hb)?16'hf7ff:(in==4'hc)?16'hefff:(in==4'hd)?16'hdfff:(in==4'he)?16'hbfff:16'h7fff:16'hffff;
    end
endmodule
*/
module hamming_enc(
  input [10:0]d,
  output logic [15:0]code);

  always_comb
  begin
    code[0] <= d[0] ^ d[1] ^ d[3] ^ d[4] ^ d[6] ^ d[8] ^ d[10];
    code[1] <= d[0] ^ d[2] ^ d[3] ^ d[5] ^ d[6] ^ d[9] ^ d[10];
    code[2] <= d[0];
    code[3] <= d[1] ^ d[2] ^ d[3] ^ d[7] ^ d[8] ^ d[9] ^ d[10];
    code[4] <= d[1];
    code[5] <= d[2];
    code[6] <= d[3];
    code[7] <= d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10];
    code[8] <= d[4];
    code[9] <= d[5];
    code[10] <= d[6];
    code[11] <= d[7];
    code[12] <= d[8];
    code[13] <= d[9];
    code[14] <= d[10];
    code[15] <= ^code[14:0];
    // code[16] <= d[11];
    // code[17] <= d[12];
    // code[18] <= d[13];
  end
endmodule


module readout#(
    parameter sc = 16,
    parameter fc_w = 3,
    parameter r_bc_w = 3,
    parameter full_bc_w = 11,
    parameter gr_w = 5)
    (
    input clk,     //Clock
    input bc_clk,
    input rstb,     // Reset signal
    input [full_bc_w-1:0]full_bcid,
    input [r_bc_w*sc-1:0]in_bc_in,
    input [16*sc-1:0]in_pix_in, // Pixel in per mem syn output
    input [gr_w*sc-1:0]in_groupid, // Groupid per mem syn output
    input [fc_w*sc-1:0]in_finecounter, // fine counter per mem sync output
    input [sc-1:0]valid, //assertion on one sync memory
    output logic [sc-1:0]busy, // Lock the sync mem
    output logic data_flag,
    output wire [4+4+gr_w+fc_w-1:0]data_out,
    output logic [5:0]reg_head); // Data out to long term mem
    // pixel 20 to 5 where 20 is the msb
    logic [15:0]to_enc;
    logic [5:0] next_head;

    //ff with all data for synchonisation..
    logic [r_bc_w*sc-1:0]bc_in;
    logic [16*sc-1:0]pix_in; // Pixel in per mem syn output
    logic [gr_w*sc-1:0]groupid; // Groupid per mem syn output
    logic [fc_w*sc-1:0]finecounter; // fine counter per mem sync output

    logic pre_start;
    logic start;
    logic [3:0]colid;
    logic [gr_w-1:0]temp_groupid;
    logic [gr_w-1:0]groupid_reg;
    logic [fc_w-1:0]temp_finecounter;
    logic [fc_w-1:0]finecounter_reg;
    logic [1:0]state,newstate;
    logic [15:0]temp_pix,reg_pix;
    logic [full_bc_w-1:0]reg_bc, full_temp_bc, full_bcid_ff, bc_counter_reg, bc_counter_next, bc_counter_reg_following;// prev_bc;
    logic [r_bc_w-1:0]tail_bc_ff;
    // logic [r_bc_w-1:0]temp_bc;
    logic [full_bc_w-r_bc_w-1:0]head_bc, pre_head_bc, head_bc_ff, pre_head_bc_ff;
    logic or_pix;
    // logic pre_or_pix;
    logic [15:0]to_sclr_0,to_sclr_1;
    logic [3:0]addr, opp_addr;
    logic pre_data_flag;
    logic [4+4+gr_w+fc_w-1:0]pre_data_out; // Data out to long term mem
    logic [4+4+gr_w+fc_w-1:0]bcid_code;
    logic [15:0]pre_bcid_code, post_bcid_code;
    integer i;
    logic addr_en,col_en;

    priority_enc inst_prienc_0(.in(reg_pix),.clear_en(addr_en),.out(addr),.clear(to_sclr_0));
    opp_priority_enc opp_prenc_0(.in(reg_pix),.out(opp_addr));
    priority_enc inst_prienc_1(.in(to_enc),.clear_en(col_en),.out(colid),.clear(to_sclr_1));

//    priority_dec inst_pridec_0(.in(addr),.enable(addr_en),.out(to_sclr_0));
//    priority_dec inst_pridec_1(.in(colid),.enable(col_en),.out(to_sclr_1));


    //ff for synchronisation with synchronisation memory (this should already be synchronised, but, since we don't define the communication between these blocks, another synchronisation is needed

    ff_en #(.width(16*(gr_w+16+fc_w+r_bc_w))) in_data_ff(.clk(clk),.rstb(rstb),.data_in({in_groupid, in_pix_in, in_finecounter, in_bc_in}),.en(!start),.data_out({groupid, pix_in, finecounter, bc_in}));


    genvar j;
    generate
    for(j=0;j<16;j++)
    begin : loop
        ff_sclr_en inst_col(.clk(clk),.rstb(rstb),.sclrb(to_sclr_1[j]),.data_in(valid[j]),.data_out(to_enc[j]),.en(!start));
        ff_sclr inst_pix(.clk(clk),.rstb(rstb),.sclrb(to_sclr_0[j]),.data_in(temp_pix[j]),.data_out(reg_pix[j]));
    end
    endgenerate



    ff #(.width(2)) inst_state(.clk(clk),.rstb(rstb),.data_in(newstate),.data_out(state));
    ff #(.width(gr_w)) inst_group(.clk(clk),.rstb(rstb),.data_in(temp_groupid),.data_out(groupid_reg));
    ff #(.width(fc_w)) inst_finec(.clk(clk),.rstb(rstb),.data_in(temp_finecounter),.data_out(finecounter_reg));
    ff #(.width(full_bc_w)) inst_bcidff(.clk(clk),.rstb(rstb),.data_in(full_temp_bc),.data_out(reg_bc));
    ff #(.width(full_bc_w)) inst_bcid_count_ff(.clk(clk),.rstb(rstb),.data_in(bc_counter_next),.data_out(bc_counter_reg));

    //ff #(.width(1)) start_ff(.clk(clk),.rstb(rstb),.data_in(pre_start),.data_out(start));

    always_comb
    begin
        // pre_or_pix<=|temp_pix;
        or_pix<=|reg_pix;
        start<=|to_enc;
        // pre_start<=|valid;
        //bcid_code[4+4+gr_w+fc_w-1:16]<=1'b0;
        // pre_data_out={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
    end

    hamming_enc post_bcid_h_enc(.d({{head_bc_ff},{full_temp_bc[r_bc_w-1:0]}}),.code(post_bcid_code));
    hamming_enc pre_bcid_h_enc(.d({{pre_head_bc_ff},{full_temp_bc[r_bc_w-1:0]}}),.code(pre_bcid_code));

    ff #(.width(4+4+gr_w+fc_w)) inst_data(.clk(clk),.rstb(rstb),.data_in(pre_data_out),.data_out(data_out));
    ff #(.width(1)) inst_data_flag(.clk(clk),.rstb(rstb),.data_in(pre_data_flag),.data_out(data_flag));
    ff #(.width(6)) inst_codeff(.clk(clk),.rstb(rstb),.data_in(next_head),.data_out(reg_head));
//    ff_en #(.width(full_bc_w)) inst_bcidin_ff(.clk(clk),.rstb(rstb),.data_in(full_bcid),.en(!bc_clk),.data_out(full_bcid_ff));

    ff_en #(.width(full_bc_w-r_bc_w)) head_ff_ff(.clk(clk),.rstb(rstb),.data_in(head_bc),.en(bc_clk),.data_out(head_bc_ff));
    ff_en #(.width(full_bc_w-r_bc_w)) pre_head_ff(.clk(clk),.rstb(rstb),.data_in(pre_head_bc),.en(bc_clk),.data_out(pre_head_bc_ff));

    ff_en #(.width(r_bc_w)) tail_ff(.clk(clk),.rstb(rstb),.data_in(full_bcid[r_bc_w-1:0]),.en(bc_clk),.data_out(tail_bc_ff));
//    ff_en #(.width(full_bc_w-r_bc_w)) inst_bcidin_ff(.clk(clk),.rstb(rstb),.data_in(pre_head_bc),.en(!bc_clk),.data_out(pre_head_bc_ff));

/*     always_ff @ (posedge clk)
     if (!rstb)
          state=2'h0;
     else
          state<=newstate;
*/


    always_comb
    begin
      //completing the bc..
      //In fast memory there are only 3 bits for the BCID, here we add the
      //other bits according to the value of the current BC.
      //If the bits stored have a value higher than that of the last bits of the current BCID
      //then it means that they belong to the previous BC and we assign pre_head_bc
      head_bc<=full_bcid[full_bc_w-1:r_bc_w];
      pre_head_bc<=full_bcid[full_bc_w-1:r_bc_w]-8'b1;
      if (full_temp_bc[r_bc_w-1:0] > tail_bc_ff) // full_bcid_ff[r_bc_w-1:0])
      begin
        bcid_code[15:0] <= pre_bcid_code;
        full_temp_bc[full_bc_w-1:r_bc_w]<=pre_head_bc;
      end
      else
      begin
        bcid_code[15:0] <= post_bcid_code;
        full_temp_bc[full_bc_w-1:r_bc_w]<=head_bc;
      end
      // full_temp_bc[r_bc_w-1:0]<=temp_bc;
      //here is prepared the previous bc_id, to be stored in case there are no hits
      // prev_bc <= full_temp_bc - 11'b1;

          case(state)
          0:   begin
            if(start)
            begin
              case(colid)
                    0:   begin
                              temp_pix <= pix_in[15:0];
                              temp_groupid <= groupid[gr_w-1:0];
                              temp_finecounter <= finecounter[fc_w-1:0];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w-1:0];
                         end
                    1:   begin
                              temp_pix <= pix_in[31:16];
                              temp_groupid <= groupid[gr_w*2-1:gr_w];
                              temp_finecounter <= finecounter[fc_w*2-1:fc_w];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*2-1:r_bc_w];
                         end
                    2:   begin
                              temp_pix <= pix_in[47:32];
                              temp_groupid <= groupid[gr_w*3-1:gr_w*2];
                              temp_finecounter <= finecounter[fc_w*3-1:fc_w*2];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*3-1:r_bc_w*2];
                         end
                    3:   begin
                              temp_pix <= pix_in[63:48];
                              temp_groupid <= groupid[gr_w*4-1:gr_w*3];
                              temp_finecounter <= finecounter[fc_w*4-1:fc_w*3];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*4-1:r_bc_w*3];
                         end
                    4:   begin
                              temp_pix <= pix_in[79:64];
                              temp_groupid <= groupid[gr_w*5-1:gr_w*4];
                              temp_finecounter <= finecounter[fc_w*5-1:fc_w*4];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*5-1:r_bc_w*4];
                         end
                    5:   begin
                              temp_pix <= pix_in[95:80];
                              temp_groupid <= groupid[gr_w*6-1:gr_w*5];
                              temp_finecounter <= finecounter[fc_w*6-1:fc_w*5];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*6-1:r_bc_w*5];
                         end
                    6:   begin
                              temp_pix <= pix_in[111:96];
                              temp_groupid <= groupid[gr_w*7-1:gr_w*6];
                              temp_finecounter <= finecounter[fc_w*7-1:fc_w*6];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*7-1:r_bc_w*6];
                         end
                    7:   begin
                              temp_pix <= pix_in[127:112];
                              temp_groupid <= groupid[gr_w*8-1:gr_w*7];
                              temp_finecounter <= finecounter[fc_w*8-1:fc_w*7];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*8-1:r_bc_w*7];
                         end
                    8:   begin
                              temp_pix <= pix_in[143:128];
                              temp_groupid <= groupid[gr_w*9-1:gr_w*8];
                              temp_finecounter <= finecounter[fc_w*9-1:fc_w*8];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*9-1:r_bc_w*8];
                         end
                    9:   begin
                              temp_pix <= pix_in[159:144];
                              temp_groupid <= groupid[gr_w*10-1:gr_w*9];
                              temp_finecounter <= finecounter[fc_w*10-1:fc_w*9];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*10-1:r_bc_w*9];
                         end
                    10:   begin
                              temp_pix <= pix_in[175:160];
                              temp_groupid <= groupid[gr_w*11-1:gr_w*10];
                              temp_finecounter <= finecounter[fc_w*11-1:fc_w*10];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*11-1:r_bc_w*10];
                         end
                    11:   begin
                              temp_pix <= pix_in[191:176];
                              temp_groupid <= groupid[gr_w*12-1:gr_w*11];
                              temp_finecounter <= finecounter[fc_w*12-1:fc_w*11];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*12-1:r_bc_w*11];
                         end
                    12:   begin
                              temp_pix <= pix_in[207:192];
                              temp_groupid <= groupid[gr_w*13-1:gr_w*12];
                              temp_finecounter <= finecounter[fc_w*13-1:fc_w*12];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*13-1:r_bc_w*12];
                         end
                    13:   begin
                              temp_pix <= pix_in[223:208];
                              temp_groupid <= groupid[gr_w*14-1:gr_w*13];
                              temp_finecounter <= finecounter[fc_w*14-1:fc_w*13];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*14-1:r_bc_w*13];
                         end
                    14:   begin
                              temp_pix <= pix_in[239:224];
                              temp_groupid <= groupid[gr_w*15-1:gr_w*14];
                              temp_finecounter <= finecounter[fc_w*15-1:fc_w*14];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*15-1:r_bc_w*14];
                         end
                    15:   begin
                              temp_pix <= pix_in[255:240];
                              temp_groupid <= groupid[gr_w*16-1:gr_w*15];
                              temp_finecounter <= finecounter[fc_w*16-1:fc_w*15];
                              full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*16-1:r_bc_w*15];
                         end
               endcase
               // if(full_temp_bc==reg_bc)
               // begin
               //   newstate<=2'h1;
               //   next_head<=6'b000000;
               //   pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
               //   pre_data_flag<=1'b1;
               // end
               // else if(full_temp_bc==reg_bc_following)
               // begin
               //   newstate<=2'h1;
               //   next_head<=6'b111001;
               //   pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
               //   pre_data_flag<=1'b1;
               // end
               // else
               // begin
               //   newstate<=2'h3;
               //   next_head<=6'b011110;
               //   pre_data_out<=bcid_code;
               //   pre_data_flag<=1'b1;
               // end
               pre_data_out<='0;
               next_head<=6'b000000;
               newstate<=2'h2;
               pre_data_flag<=1'b0;
               addr_en<=1'b0;
               col_en<=1'b0;
             end
             else
               begin
                 newstate<=2'h0;
                 next_head<=6'b000000;
                 pre_data_out<='0;
                 addr_en<=1'b0;
                 col_en<=1'b0;
                 temp_pix<='0;
                 temp_groupid<='0;
                 temp_finecounter<='0;
                 full_temp_bc <= reg_bc;
                 pre_data_flag<=1'b0;
               end
               bc_counter_next<=bc_counter_reg;
             end

          1:   begin
              // temp_pix <= reg_pix;
              // temp_groupid <= groupid_reg;
              // temp_finecounter <= finecounter_reg;
              // full_temp_bc <= reg_bc;
              // next_head<=6'b000000;
              // pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
              bc_counter_next<=bc_counter_reg;
              // pre_data_flag<=1'b1;
              if(or_pix)
              begin
                temp_pix <= reg_pix;
                temp_groupid <= groupid_reg;
                temp_finecounter <= finecounter_reg;
                full_temp_bc <= reg_bc;
                next_head<=6'b000000;
                pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
                newstate<=2'h1;
                addr_en<=1'b1;
                pre_data_flag<=1'b1;
                addr_en<=1'b1;
                if(addr==opp_addr)
                  col_en<=1'b1;
                else
                  col_en<=1'b0;
              end
              else if(start)
              begin
                case(colid)
                      0:   begin
                                temp_pix <= pix_in[15:0];
                                temp_groupid <= groupid[gr_w-1:0];
                                temp_finecounter <= finecounter[fc_w-1:0];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w-1:0];
                           end
                      1:   begin
                                temp_pix <= pix_in[31:16];
                                temp_groupid <= groupid[gr_w*2-1:gr_w];
                                temp_finecounter <= finecounter[fc_w*2-1:fc_w];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*2-1:r_bc_w];
                           end
                      2:   begin
                                temp_pix <= pix_in[47:32];
                                temp_groupid <= groupid[gr_w*3-1:gr_w*2];
                                temp_finecounter <= finecounter[fc_w*3-1:fc_w*2];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*3-1:r_bc_w*2];
                           end
                      3:   begin
                                temp_pix <= pix_in[63:48];
                                temp_groupid <= groupid[gr_w*4-1:gr_w*3];
                                temp_finecounter <= finecounter[fc_w*4-1:fc_w*3];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*4-1:r_bc_w*3];
                           end
                      4:   begin
                                temp_pix <= pix_in[79:64];
                                temp_groupid <= groupid[gr_w*5-1:gr_w*4];
                                temp_finecounter <= finecounter[fc_w*5-1:fc_w*4];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*5-1:r_bc_w*4];
                           end
                      5:   begin
                                temp_pix <= pix_in[95:80];
                                temp_groupid <= groupid[gr_w*6-1:gr_w*5];
                                temp_finecounter <= finecounter[fc_w*6-1:fc_w*5];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*6-1:r_bc_w*5];
                           end
                      6:   begin
                                temp_pix <= pix_in[111:96];
                                temp_groupid <= groupid[gr_w*7-1:gr_w*6];
                                temp_finecounter <= finecounter[fc_w*7-1:fc_w*6];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*7-1:r_bc_w*6];
                           end
                      7:   begin
                                temp_pix <= pix_in[127:112];
                                temp_groupid <= groupid[gr_w*8-1:gr_w*7];
                                temp_finecounter <= finecounter[fc_w*8-1:fc_w*7];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*8-1:r_bc_w*7];
                           end
                      8:   begin
                                temp_pix <= pix_in[143:128];
                                temp_groupid <= groupid[gr_w*9-1:gr_w*8];
                                temp_finecounter <= finecounter[fc_w*9-1:fc_w*8];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*9-1:r_bc_w*8];
                           end
                      9:   begin
                                temp_pix <= pix_in[159:144];
                                temp_groupid <= groupid[gr_w*10-1:gr_w*9];
                                temp_finecounter <= finecounter[fc_w*10-1:fc_w*9];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*10-1:r_bc_w*9];
                           end
                      10:   begin
                                temp_pix <= pix_in[175:160];
                                temp_groupid <= groupid[gr_w*11-1:gr_w*10];
                                temp_finecounter <= finecounter[fc_w*11-1:fc_w*10];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*11-1:r_bc_w*10];
                           end
                      11:   begin
                                temp_pix <= pix_in[191:176];
                                temp_groupid <= groupid[gr_w*12-1:gr_w*11];
                                temp_finecounter <= finecounter[fc_w*12-1:fc_w*11];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*12-1:r_bc_w*11];
                           end
                      12:   begin
                                temp_pix <= pix_in[207:192];
                                temp_groupid <= groupid[gr_w*13-1:gr_w*12];
                                temp_finecounter <= finecounter[fc_w*13-1:fc_w*12];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*13-1:r_bc_w*12];
                           end
                      13:   begin
                                temp_pix <= pix_in[223:208];
                                temp_groupid <= groupid[gr_w*14-1:gr_w*13];
                                temp_finecounter <= finecounter[fc_w*14-1:fc_w*13];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*14-1:r_bc_w*13];
                           end
                      14:   begin
                                temp_pix <= pix_in[239:224];
                                temp_groupid <= groupid[gr_w*15-1:gr_w*14];
                                temp_finecounter <= finecounter[fc_w*15-1:fc_w*14];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*15-1:r_bc_w*14];
                           end
                      15:   begin
                                temp_pix <= pix_in[255:240];
                                temp_groupid <= groupid[gr_w*16-1:gr_w*15];
                                temp_finecounter <= finecounter[fc_w*16-1:fc_w*15];
                                full_temp_bc[r_bc_w-1:0] <= bc_in[r_bc_w*16-1:r_bc_w*15];
                           end
                endcase
                // if(full_temp_bc==reg_bc)
                // begin
                //   newstate<=2'h1;
                //   next_head<=6'b000000;
                //   pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
                //   pre_data_flag<=1'b1;
                // end
                // else if(full_temp_bc==reg_bc_following)
                // begin
                //   newstate<=2'h1;
                //   next_head<=6'b111001;
                //   pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
                //   pre_data_flag<=1'b1;
                // end
                // else
                // begin
                //   newstate<=2'h3;
                //   next_head<=6'b011110;
                //   pre_data_out<=bcid_code;
                //   pre_data_flag<=1'b1;
                // end
                // pre_data_out<='0;
                next_head<=6'b000000;
                pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
                newstate<=2'h1;
                pre_data_flag<=1'b1;
                addr_en<=1'b1;
                col_en<=1'b0;

                // newstate<=2'h0;
                // col_en<=1'b1;
                // addr_en<=1'b0;
                // pre_data_flag<=1'b0;
              end
              else
              begin
                temp_pix <= reg_pix;
                temp_groupid <= groupid_reg;
                temp_finecounter <= finecounter_reg;
                full_temp_bc <= reg_bc;
                next_head<=6'b000000;
                pre_data_out<='0;


                   newstate<=2'h0;
                   col_en<=1'b0;
                   addr_en<=1'b0;
                   pre_data_flag<=1'b0;
              end
           end
          2:   begin
            if(full_temp_bc==bc_counter_reg)
            begin
              newstate<=2'h1;
              next_head<=6'b000000;
              pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
              pre_data_flag<=1'b1;
              addr_en<=1'b1;
            end
            else if(full_temp_bc==bc_counter_reg_following)
            begin
              newstate<=2'h1;
              next_head<=6'b111001;
              pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
              pre_data_flag<=1'b1;
              addr_en<=1'b1;
            end
            else
            begin
              newstate<=2'h3;
              next_head<=6'b011110;
              pre_data_out<=bcid_code;
              pre_data_flag<=1'b1;
              addr_en<=1'b0;
            end
            bc_counter_next<=full_temp_bc;
            // addr_en<=1'b1;

            if(addr==opp_addr)
            begin
              col_en<=1'b1;
            end
            else
            begin
              col_en<=1'b0;
            end

            temp_pix <= reg_pix;
            temp_groupid <= groupid_reg;
            temp_finecounter <= finecounter_reg;
            full_temp_bc <= reg_bc;

            // next_head<=6'b0;
            // addr_en<=1'b0;
            // col_en<=1'b1;
            // temp_pix <= reg_pix;
            // temp_groupid <= groupid_reg;
            // temp_finecounter <= finecounter_reg;
            // full_temp_bc <= reg_bc;
            // newstate<=2'h0;
            // pre_data_out<='0;
            // pre_data_flag<=1'b0;
          end
          3:   begin
            bc_counter_next<=bc_counter_reg;
            addr_en<=1'b1;
            col_en<=1'b0;
            temp_pix<='0;
            temp_groupid <= '0;
            temp_finecounter <= '0;
            full_temp_bc <= reg_bc;
            next_head<=6'b111001;
            newstate<=2'h1;
            pre_data_out<={addr,colid,temp_groupid[gr_w-1:0],temp_finecounter[fc_w-1:0]};
            pre_data_flag<=1'b1;
          end
      //    default:begin
                    // addr_en<=1'b0;
                    // col_en<=1'b0;
                    // temp_pix <= '0;
                    // temp_groupid <= '0;
                    // pre_data_out<='0;
                    // temp_finecounter <= '0;
                    // full_temp_bc <= '0;
        //            data_flag<=1'b0;
          //     end
          endcase
          // pre_or_pix<=|temp_pix;
          // or_pix<=|reg_pix;
          // pre_data_flag<=start;
          bc_counter_reg_following <= bc_counter_reg+11'h001;
     end

endmodule

module ff_sclr_en(
     input clk,
     input rstb,
     input en,
     input sclrb,
     input data_in,
     output logic data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
            data_out<=1'b0;
          else
          if(!sclrb)
            data_out<=1'b0;
          else
            if(en)
              data_out<=data_in;
     end

endmodule

module ff_sclr(
     input clk,
     input rstb,
     input sclrb,
     input data_in,
     output logic data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
            data_out<=1'b0;
          else
          if(!sclrb)
            data_out<=1'b0;
          else
            data_out<=data_in;
    end

endmodule

module ff #(parameter width=2)(
     input clk,
     input rstb,
     input [width -1:0]data_in,
     output logic [width -1:0]data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
               data_out<='0;
          else
             data_out<=data_in;
     end
endmodule

module ff_en #(parameter width=2)(
     input clk,
     input rstb,
     input [width -1:0]data_in,
     input en,
     output logic [width -1:0]data_out);
     always_ff @(posedge clk)
     begin
          if(!rstb)
               data_out<='0;
          else
          if(en)
             data_out<=data_in;
     end
endmodule
