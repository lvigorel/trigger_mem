`timescale 1ns/1ps

module bc_extender#(
  parameter full_bc_w = 11,
  parameter r_bc_w = 3,
  parameter fc_w = 3,
  parameter gr_w = 5)
  (
  input clk, //clock
  input bc_clk, //bunch crossing (trigger) clock
  input rstb, // Reset signal
  input [full_bc_w-1:0]full_bcid,
  input [r_bc_w-1:0]r_bc_in,
  input [16-1:0]pix_in, // Pixel in per mem syn output
  input [gr_w-1:0]groupid_in, // Groupid per mem syn output
  input [fc_w-1:0]finecounter_in, // fine counter per mem sync output
  input valid_in, //signal from sync mem to inform that it has data
  input busy_in, //signal from next module to warn that it's busy
  output logic valid_out, //signal to next module to inform that we have data
  output logic busy_out, //signal to sync mem to warn that we are busy
  output logic [full_bc_w-1:0]full_r_bc_out,
  output logic [16-1:0]pix_out, // Pixel in per mem syn output
  output logic [gr_w-1:0]groupid_out, // Groupid per mem syn output
  output logic [fc_w-1:0]finecounter_out // fine counter per mem sync output
  );

  logic [full_bc_w-r_bc_w-1:0]head_bc, pre_head_bc, head_bc_ff, pre_head_bc_ff;
  logic [r_bc_w-1:0]tail_bc_ff;

  always_comb
  begin
    head_bc<=full_bcid[full_bc_w-1:r_bc_w];
    pre_head_bc<=full_bcid[full_bc_w-1:r_bc_w]-8'b1;
  end

  ff_en #(.width(full_bc_w-r_bc_w)) head_ff_ff(.clk(clk),.rstb(rstb),.data_in(head_bc),.en(bc_clk),.data_out(head_bc_ff));
  ff_en #(.width(full_bc_w-r_bc_w)) pre_head_ff(.clk(clk),.rstb(rstb),.data_in(pre_head_bc),.en(bc_clk),.data_out(pre_head_bc_ff));
  ff_en #(.width(r_bc_w)) tail_ff(.clk(clk),.rstb(rstb),.data_in(full_bcid[r_bc_w-1:0]),.en(bc_clk),.data_out(tail_bc_ff));

  always_comb
  begin
    // if((!busy_in) & valid_in)
    // begin
      if (r_bc_in[r_bc_w-1:0] > tail_bc_ff)
      begin
        full_r_bc_out[full_bc_w-1:r_bc_w]<=pre_head_bc;
      end
      else
      begin
        full_r_bc_out[full_bc_w-1:r_bc_w]<=head_bc;
      end
      full_r_bc_out[r_bc_w-1:0]<=r_bc_in;
      {pix_out,groupid_out,finecounter_out}<={pix_in,finecounter_in,groupid_in};
      busy_out<=busy_in;
      valid_out<=valid_in;
    // end
  end

  always_comb
  begin
    // if(!busy_in)
    //   busy_out<=1'b0;
    // else
    //   busy_out<=1'b1;
    // busy_out<=busy_in;
    // valid_out<=valid_in;
  end

  // always_comb
  // begin
  //   if(valid_in)
  //     valid_out<=1'b1;
  //   else
  //     valid_out<=1'b0;
  // end

endmodule

module pix_encoder#(
  parameter full_bc_w = 11,
  parameter fc_w = 3,
  parameter gr_w = 5)
  (
  input clk, //clock
  input bc_clk, //bunch crossing (trigger) clock
  input rstb, // Reset signal
  input [full_bc_w-1:0]r_bc_in,
  input [16-1:0]pix_in, // Pixel in per mem syn output
  input [gr_w-1:0]groupid_in, // Groupid per mem syn output
  input [fc_w-1:0]finecounter_in, // fine counter per mem sync output
  input valid_in, //singnal from sync mem to inform that it has data
  input busy_in, //signal from next module to warn that it's busy
  output logic valid_out, //signal to next module to inform that we have data
  output logic busy_out, //signal to sync mem to warn that we are busy
  output logic [full_bc_w-1:0]r_bc_out,
  output logic [4-1:0]pix_addr_out, // Pixel in per mem syn output
  output logic [gr_w-1:0]groupid_out, // Groupid per mem syn output
  output logic [fc_w-1:0]finecounter_out // fine counter per mem sync output
  );

  logic [3:0]addr, opp_addr;
  logic [15:0]sclr, pix_ff;

  logic [full_bc_w-1:0]r_bc_ff;
  logic [4-1:0]pix_addr_ff; // Pixel in per mem syn output
  logic [gr_w-1:0]groupid_ff; // Groupid per mem syn output
  logic [fc_w-1:0]finecounter_ff; // fine counter per mem sync output

  //input ff for all data except pix
  ff_en #(.width(full_bc_w+fc_w+gr_w))
    data_in_ff(
      .clk(clk),
      .rstb(rstb),
      .data_in({r_bc_in,groupid_in,finecounter_in}),
      .en((!busy_out) & valid_in),
      .data_out({r_bc_ff,groupid_ff,finecounter_ff})
      );

  //input ff for pix
  genvar j;
  generate
  for(j=0;j<16;j++)
  begin : loop
    ff_sclr_en
      inst_pix(
        .clk(clk),
        .rstb(rstb),
        .sclrb(sclr[j]),
        .data_in(pix_in[j]),
        .data_out(pix_ff[j]),
        .en((!busy_out) & valid_in)
        );
  end
  endgenerate

  priority_enc
    addr_enc(
      .in(pix_ff),
      .clear_en(!busy_in),
      .out(addr),
      .clear(sclr),
      .flag(flag_pix)
      );

  opp_priority_enc
    opp_addr_enc(
      .in(pix_ff),
      .out(opp_addr)
      );

  always_comb
  begin
    valid_out<=flag_pix;
    if(addr==opp_addr)
      busy_out<=busy_in;
    else
      busy_out<=1'b1;
  end

  ff_en #(.width(4+full_bc_w+fc_w+gr_w))
    data_out_ff(
      .clk(clk),
      .rstb(rstb),
      .data_in({r_bc_ff,groupid_ff,addr,finecounter_ff}),
      .en(!busy_in),
      .data_out({r_bc_out,groupid_out,pix_addr_out,finecounter_out})
      );

endmodule


module tree_readout(
  input [:0]data_in,
  input [ :0] bc_in
  );

endmodule





module priority_enc(
     input [15:0] in,
     input clear_en,
     output logic [3:0]out,
     output logic [15:0]clear,
     output logic flag);
     always_comb
     begin
          out<=(in[0])?4'h0:(in[1])?4'h1:(in[2])?4'h2:(in[3])?4'h3:(in[4])?4'h4:(in[5])?4'h5:(in[6])?4'h6:(in[7])?4'h7:(in[8])?4'h8:(in[9])?4'h9:(in[10])?4'ha:(in[11])?4'hb:(in[12])?4'hc:(in[13])?4'hd:(in[14])?4'he:(in[15])?4'hf:4'h0;
          flag<=(|in)?1'b1:1'b0;
          clear<=(clear_en)?((in[0])?16'hfffe:(in[1])?16'hfffd:(in[2])?16'hfffb:(in[3])?16'hfff7:(in[4])?16'hffef:(in[5])?16'hffdf:(in[6])?16'hffbf:(in[7])?16'hff7f:(in[8])?16'hfeff:(in[9])?16'hfdff:(in[10])?16'hfbff:(in[11])?16'hf7ff:(in[12])?16'hefff:(in[13])?16'hdfff:(in[14])?16'hbfff:(in[15])?16'h7fff:16'hffff):16'hffff;
     end
endmodule

module opp_priority_enc(
     input [15:0] in,
     // input clear_en,
     output logic [3:0]out);
     // output logic [15:0]clear);
     always_comb
     begin
          out<=(in[15])?4'hf:(in[14])?4'he:(in[13])?4'hd:(in[12])?4'hc:(in[11])?4'hb:(in[10])?4'ha:(in[9])?4'h9:(in[8])?4'h8:(in[7])?4'h7:(in[6])?4'h6:(in[5])?4'h5:(in[4])?4'h4:(in[3])?4'h3:(in[2])?4'h2:(in[1])?4'h1:(in[0])?4'h0:4'h0;
     end
endmodule

module hamming_enc(
  input [10:0]d,
  output logic [15:0]code);

  always_comb
  begin
    code[0] <= d[0] ^ d[1] ^ d[3] ^ d[4] ^ d[6] ^ d[8] ^ d[10];
    code[1] <= d[0] ^ d[2] ^ d[3] ^ d[5] ^ d[6] ^ d[9] ^ d[10];
    code[2] <= d[0];
    code[3] <= d[1] ^ d[2] ^ d[3] ^ d[7] ^ d[8] ^ d[9] ^ d[10];
    code[4] <= d[1];
    code[5] <= d[2];
    code[6] <= d[3];
    code[7] <= d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10];
    code[8] <= d[4];
    code[9] <= d[5];
    code[10] <= d[6];
    code[11] <= d[7];
    code[12] <= d[8];
    code[13] <= d[9];
    code[14] <= d[10];
    code[15] <= ^code[14:0];
    // code[16] <= d[11];
    // code[17] <= d[12];
    // code[18] <= d[13];
  end
endmodule



module ff_sclr_en(
  input clk,
  input rstb,
  input en,
  input sclrb,
  input data_in,
  output logic data_out);
  always_ff @(posedge clk)
  begin
    if(!rstb)
      data_out<=1'b0;
    else
    if(!sclrb)
      data_out<=1'b0;
    else
    if(en)
      data_out<=data_in;
  end

endmodule

module ff #(parameter width=2)(
  input clk,
  input rstb,
  input [width -1:0]data_in,
  output logic [width -1:0]data_out);
  always_ff @(posedge clk)
  begin
    if(!rstb)
      data_out<='0;
    else
      data_out<=data_in;
  end
endmodule

module ff_en #(parameter width=2)(
  input clk,
  input rstb,
  input [width -1:0]data_in,
  input en,
  output logic [width -1:0]data_out);
  always_ff @(posedge clk)
  begin
    if(!rstb)
      data_out<='0;
    else
    if(en)
      data_out<=data_in;
  end
endmodule
