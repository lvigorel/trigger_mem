library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity head_h_dec_tb is

end head_h_dec_tb;

architecture comb of head_h_dec_tb is

  -- Hamming decoder (for BCID) signals

  signal code_in_sim : std_logic_vector(5 downto 0);
  signal code_out_sim : std_logic_vector(5 downto 0);
  signal err_sim : std_logic;

  component head_h_dec
  port(
  code_in : in std_logic_vector(5 downto 0);
  code_out : out std_logic_vector(5 downto 0);
  err : out std_logic
  );
  end component;


  begin

  uut : head_h_dec
  port map(
    code_in => code_in_sim,
    code_out => code_out_sim,
    err => err_sim
  );

  stim_proc : process
  begin
    code_in_sim <= "011110";
  wait for 10 ns;
    code_in_sim <= "000000";
  wait for 10 ns;
    code_in_sim <= "111001";
  wait for 10 ns;
    code_in_sim <= "011111";
  wait for 10 ns;
    code_in_sim <= "010110";
  wait for 10 ns;
    code_in_sim <= "111101";
  wait for 10 ns;
    code_in_sim <= "011011";
  wait for 10 ns;
    code_in_sim <= "011101";
  wait for 10 ns;
    code_in_sim <= "101110";
  wait for 10 ns;
    code_in_sim <= "111001";
  wait for 10 ns;
    code_in_sim <= "011010";
  wait for 10 ns;
    code_in_sim <= "111110";
  wait for 10 ns;
  end process;

end comb;
