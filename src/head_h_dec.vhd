library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity head_h_dec is
  port(

    code_in : in std_logic_vector(6-1 downto 0);
    code_out : out std_logic_vector(6-1 downto 0);
    err : out std_logic

  );
end head_h_dec;

architecture comb of head_h_dec is

  -- Hamming decoder (for header) signals

  signal p : std_logic_vector(3 downto 0);
  signal s : std_logic_vector(2 downto 0);
  signal corr : std_logic_vector(6-1 downto 0);

  --signal corrected : std_logic_vector(6-1 downto 0);

  begin

    ------------------------------------
    -- Hamming decoder for row header --
    ------------------------------------
    p_0 : p(0) <= code_in(2) xor code_in(4);
    p_1 : p(1) <= code_in(2);
    p_2 : p(2) <= code_in(4);
    p_tot : p(3) <= code_in(0) xor code_in(1)
         xor code_in(2) xor code_in(3)
         xor code_in(4);

    s_0 : s(0) <= p(0) xor code_in(0);
    s_1 : s(1) <= p(1) xor code_in(1);
    s_2 : s(2) <= p(2) xor code_in(3);

    decoder_gen_h:
    for j in 0 to 5 generate
      corr(j) <= '1' when to_unsigned(j,3) = unsigned(s(2 downto 0)) else
                   '0';
    end generate;
    pre_corr : code_out(4 downto 0) <= code_in(6-2 downto 0)
                                            xor corr(5 downto 1);
    err_l : err <= corr(0) or (p(3)xor code_in(5));
    corr_l : code_out(5) <= p(3) xor (not corr(0));
    -----------------------------------------

  end comb;
