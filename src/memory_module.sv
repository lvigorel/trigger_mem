`timescale 1ns/1ps

module memory_module #(
  parameter sc=16,
  parameter fc_w=4,
  parameter full_bc_w=10,
  parameter r_bc_w=3,
  parameter gr_w=6,
  parameter head_w=6,
  parameter height=250,
  parameter addr_w=8)
  (
  input clk,
  input rstb,
  input bc_clk,

  //input [r_bc_w-1:0]r_bc_in,
  input [16-1:0]pix_in, // Pixel in per mem syn output
  input [gr_w-1:0]groupid, // Groupid per mem syn output
  input [fc_w-1:0]finecounter, // fine counter per mem sync output

  input [sc-1:0]valid, //assertion on one sync memory
  // input read_reset,
  // input read_cmd_in,
  input trig,
  // input [bc_w-1:0]trig_id,

  output logic [r_bc_w-1:0]search_bc_sync,
  output logic [sc-1:0]read, // Lock the sync mem

  output logic [full_bc_w+4+4+gr_w+fc_w-1:0]data_out,
  output logic out_flag,
  // output logic search_finished,
  // output logic read_finished,
  output logic err_out);

  logic [full_bc_w-1:0]bc_id;
  logic [full_bc_w-1:0]search_id;
  logic search_flag;
  logic read_reset;
  logic search_finished;
  logic read_finished;
  logic read_cmd;
  logic [head_w+4+4+gr_w+fc_w-1:0]data_rd;
  logic [addr_w-1:0]r_addr;
  logic [addr_w-1:0]w_addr;
  logic en_data;
  logic [head_w-1:0]pre_head_wr;
  // logic [4+4+gr_w+fc_w-1:0]pre_data_wr;
  logic data_w_flag;
  logic [head_w+4+4+gr_w+fc_w-1:0]data_wr;



  // readout from synchronisation memory
  readout #(
    .sc(sc),
    .head_w(head_w),
    .fc_w(fc_w),
    .r_bc_w(r_bc_w),
    .full_bc_w(full_bc_w),
    .gr_w(gr_w),
    .addr_w(addr_w),
    .height(height))
  inst_writing(
    .clk(clk),
    .bc_clk(bc_clk),
    .rstb(rstb),
    .full_bcid(bc_id),
    // .in_bc_in(r_bc_in),
    .in_pix_in(pix_in),
    .in_groupid(groupid),
    .in_finecounter(finecounter),
    .valid(valid),
    .search_bc(search_bc_sync),
    .read(read),
    .data_wr(data_wr),
    .w_addr(w_addr));

  // controller for writing in the memory (count the address also accounting for the total mem height)
  // write_control #(.addr_w(addr_w),.height(height),.d_width(4+4+gr_w+fc_w),.h_width(head_w))
  // inst_wr_control(.clk(clk),.rstb(rstb),.data_in(pre_data_wr),.head_in(pre_head_wr),
  //   .d_flag(data_w_flag),.data_wr(data_wr),.w_addr(w_addr));

  // memory inst
  memory #(
    .addr_w(addr_w),
    .height(height),
    .d_width(4+4+gr_w+fc_w),
    .h_width(head_w))
  inst_mem(
    .clk(clk),
    .rstb(rstb),
    .data_wr(data_wr),
    .w_addr(w_addr),
    .r_addr(r_addr),
    .data_en(en_data),
    .data_rd(data_rd));
  //

  // controller for readout. Controls address and also all the stuff related to trigger
  // made to work with the trig_controller for the token ring
  readout_corr #(
    .addr_size(addr_w),
    .height(height),
    .width(head_w+4+4+gr_w+fc_w),
    .bc_width(full_bc_w),
    .head_width(head_w))
  inst_readout(
    .clock(clk),
    .setup(rstb),
    .r_addr(r_addr),
    .en_data(en_data),
    .r_d(data_rd),
    .out_ff(data_out),
    .out_flag_ff(out_flag),
    .search_finished_out(search_finished),
    .read_finished_out(read_finished),
    .read_reset(read_reset),
    .err_out(err_out),
    .read_cmd(read_cmd),
    .read_cmd_reset(read_cmd_reset),
    .in_trig_flag(search_flag),
    .in_trig_id(search_id));

  trigger_control #(
    .bc_width(full_bc_w))
  inst_trig_contr(
    .clock(clk),
    .bc_clock(bc_clk),
    .reset(rstb),
    .trig(trig),
    .total_read_finished(read_finished),
    .read_finished_reset(read_reset),
    .read_cmd(read_cmd),
    .read_cmd_reset(read_cmd_reset),
    .search_flag(search_flag),
    .search_id(search_id),
    .bc_id(bc_id));

  // always_ff @(posedge clk)
  // begin
  //   if(!rstb)
  //     read_cmd<=1'b1;
  //   else
  //   if(!read_cmd_in)
  //     read_cmd<=1'b0;
  //   else
  //   if(!read_cmd_reset)
  //     read_cmd<=1'b1;
  // end

endmodule

// module write_control #(
//     parameter addr_w=10,
//     parameter height=850,
//     parameter d_width=17,
//     parameter h_width=6
//   )
//   (
//   input clk,
//   input rstb,
//   input [d_width-1:0]data_in,
//   input [h_width-1:0]head_in,
//   input d_flag,
//   output logic [h_width+d_width-1:0]data_wr,
//   output logic [addr_w-1:0]w_addr
//   );
//
//   logic [addr_w-1:0]w_next;
//
//   always_ff @(posedge clk)
//   begin
//     if(!rstb)
//     begin
//       data_wr<={{6'b011110},{16'h08007}};
//       w_addr<='0;
//     end
//     else
//     if(d_flag)
//     begin
//       // we can't write faster than internal write speed
//       data_wr<={head_in,data_in};
//       w_addr<=w_next;
//     end
//   end
//
//   always_comb
//   begin
//     // data_wr<={head_in,data_in};
//     if(w_addr==height-1)
//       w_next<='0;
//     else
//       w_next<=w_addr+10'h001;
//   end
//
// endmodule

// module memory #(
//   parameter addr_w=10,
//   parameter height=850,
//   parameter d_width=17,
//   parameter h_width=6
//   )
//   (
//   input clk,
//   input rstb,
//   input [h_width+d_width-1:0]data_wr,
//   input [addr_w-1:0]w_addr,
//   input [addr_w-1:0]r_addr,
//   input data_en,
//   output logic [h_width+d_width-1:0]data_rd
//   );
// endmodule
