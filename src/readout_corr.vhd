library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity readout_corr is
  generic(
    addr_size : natural := 10;
    height : natural := 200;
    width : natural := 24;
    bc_width : natural := 10;
    head_width : natural := 6
  );
  port(
    clock : in std_logic;
    setup : in std_logic;

    r_addr : out std_logic_vector(addr_size-1 downto 0);
    en_data : out std_logic;

    r_d : in std_logic_vector(width-1 downto 0);

    out_ff : out std_logic_vector(width+bc_width-head_width-1 downto 0);
    out_flag_ff : out std_logic;

    search_finished_out : out std_logic;
    read_finished_out : out std_logic;
    read_reset : in std_logic;

    err_out : out std_logic;

    read_cmd : in std_logic; -- cmd bc read
    read_cmd_reset : out std_logic;
    in_trig_flag : in std_logic; -- cmd bc search
    in_trig_id : in std_logic_vector(bc_width-1 downto 0) -- bc to search
  );
end readout_corr;

architecture comb of readout_corr is

  type STATE_TYPE is (setting, to_next, read_full, wait40, wait_count,
                      pre_read, to_bc, resync);

  constant NEW_BC : std_logic_vector(5 downto 0) := "111001";
  constant SAME_BC : std_logic_vector(5 downto 0) := "000000";
  constant COUNTER : std_logic_vector(5 downto 0) := "011110";

  constant HALF_BC :
  unsigned(bc_width-1 downto 0) := to_unsigned(2**(bc_width-1), bc_width);
  constant ZERO_BC : unsigned(bc_width-1 downto 0) := (others => '0');

  signal state, next_state : STATE_TYPE;

  ---------------------------------------------------------------
  signal head : std_logic_vector(6-1 downto 0);
  signal trig : std_logic;

  signal bc_counter_reg : unsigned(bc_width-1 downto 0);
  signal bc_counter_next : unsigned(bc_width-1 downto 0);
  signal r_d_data : std_logic_vector(width-head_width-1 downto 0);
  signal r_d_bc : std_logic_vector(bc_width-1 downto 0);


  signal out_d : std_logic_vector(width+bc_width-head_width-1 downto 0);
  signal out_flag : std_logic;

  signal read_finished : std_logic;
  signal search_finished : std_logic;
  ---------------------------------------------------------------

  signal trig_flag : std_logic;
  signal trig_id : std_logic_vector(bc_width-1 downto 0);

  signal trig_reset : std_logic;


  signal r_reg : unsigned(addr_size-1 downto 0);
  signal r_next : unsigned(addr_size-1 downto 0);
  signal en_data_next : std_logic;


  -- Hamming decoder (for header) signals
  signal err_head : std_logic;
  -- signal corrected_h : std_logic_vector(6-1 downto 0);


  -- Hamming decoder (for BCID) signals
  signal pre_err_bc : std_logic;
  signal err_bc : std_logic;
--  signal p : std_logic_vector(4 downto 0);
--  signal s : std_logic_vector(3 downto 0);
--  signal corr : std_logic_vector(16-1 downto 0);
  signal corrected_bc : std_logic_vector(15-1 downto 0);
  --signal r_d_corr : std_logic_vector(width-head_width-1 downto 0);
--  signal r_d_tail : std_logic_vector(width-head_width-1 downto 0);

  -- Error signal for both hamming decoders
  signal err_hamm : std_logic;
  -- Total error signal (out of state machine)
  signal err : std_logic;

  component head_h_dec
  port(
  code_in : in std_logic_vector(5 downto 0);
  code_out : out std_logic_vector(5 downto 0);
  err : out std_logic
  );
  end component;

  component bc_h_dec
  port(
  code_in : in std_logic_vector(15 downto 0);
  code_out : out std_logic_vector(15 downto 0);
  err : out std_logic
  );
  end component;


  begin
    --------------------
    --Trigger latching--
    --------------------

    -- FF with enable
    trig_flag_ff : process(clock, setup)
    begin
      if(setup = '0')then
        trig_flag <= '1';
      elsif(clock'event and clock ='1') then
        if(in_trig_flag = '0')then
          trig_flag <= '0';
        elsif(trig_reset = '0') then
          trig_flag <= '1';
        end if;
      end if;
    end process;

    -- FF with enable
    trig_id_ff : process(clock, setup)
    begin
      if (setup = '0') then
        trig_id <= (others => '0');
      elsif (clock'event and clock = '1') then
        if(in_trig_flag = '0') then
          trig_id <= in_trig_id;
        end if;
      end if;
    end process;

    ------------------------------------
    -- Hamming decoder for row header --
    ------------------------------------

    hamm_head : head_h_dec
    port map(
      code_in => r_d(width-1 downto width-head_width),
      code_out => head,
      err => err_head
    );
    -----------------------------------------
    -- Hamming (15,11) with additional parity bit (16,11).
    -- 11 is enought for the BCID
    -----------------------------------------
    -- Selector for decoder - decode only if header is 11
    r_d_data <= r_d(width-head_width-1 downto 0);

    mux : process(head, pre_err_bc)
    begin
      if(head="011110")then
        -- r_d_corr <= '0' & corrected_bc;
        err_bc <= pre_err_bc;
      else
        -- r_d_corr <= r_d_tail;
        err_bc <= '1';
      end if;
    end process;

    hamm_bc : bc_h_dec
    port map(
      code_in => r_d(14 downto 0),
      code_out => corrected_bc,
      err => pre_err_bc
    );

    hamm_err_assign : err_hamm <= err_head and err_bc;
    -------------------
    --Trigger control--
    -------------------
    -- r_d_data <= r_d_corr(width - head_width -1 downto 0);

    r_d_bc(0) <= corrected_bc(2);
    r_d_bc(3 downto 1) <= corrected_bc(6 downto 4);
    r_d_bc(9 downto 4) <= corrected_bc(13 downto 8);


    state_machine : process(
      state, head, r_d_data, r_d_bc, trig, trig_id,
      r_reg, bc_counter_reg, read_cmd, err_hamm)
    begin
      --default values..
      out_d <= (others => '0');
      out_flag <= '1';
      bc_counter_next <= bc_counter_reg;
      r_next <= r_reg;
      next_state <= state;
      trig_reset <= '1';
      en_data_next <= '1';
      err <= '1';
      search_finished <= '1';
      read_finished <= '1';
      read_cmd_reset <= '1';

      case state is
        -- setting
        when setting =>
          next_state <= wait_count;
          r_next <= (others => '0');
          en_data_next <= '0';

        -- move to next
        when to_next =>
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        else
          if(head = SAME_BC) then
            if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            --next_state <= to_next;
          elsif(head = NEW_BC) then
            next_state <= wait40;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
            -- r_next <= r_reg;
          elsif(head = COUNTER) then
            next_state <= wait_count;
            en_data_next <= '0';
            -- r_next <= r_reg;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          else
            err <= '0';
            next_state <= to_bc;
          end if;
        end if;

        -- pre_read
        when pre_read =>
        -- out_flag <= '0';
        en_data_next <= '0';
        -- read_cmd_reset <= '0';
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        elsif(read_cmd = '0') then
          read_cmd_reset <= '0';
          next_state <= read_full;
          out_flag <= '0';
          out_d <= std_logic_vector(bc_counter_reg) & r_d_data;
          en_data_next <= '0';

          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        elsif(HALF_BC > unsigned(trig_id) - bc_counter_reg
          and ZERO_BC < unsigned(trig_id) - bc_counter_reg)
          then
          next_state <= to_next;
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
        end if;

        -- read full
        when read_full =>
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        else
          if(head = SAME_BC) then
            if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            out_d <= std_logic_vector(bc_counter_reg) & r_d_data;
            out_flag <= '0';
            en_data_next <= '0';
          elsif(head = NEW_BC) then
            read_finished <= '0';
            -- read_cmd_reset <= '0';
            next_state <= wait40;
            trig_reset <= '0';
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          elsif(head = COUNTER) then
            read_finished <= '0';
            -- read_cmd_reset <= '0';
            next_state <= wait_count;
            en_data_next <= '0';
            trig_reset <= '0';
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          else
            trig_reset <= '0';
            read_finished <= '0';
            read_cmd_reset <= '0';
            err <= '0';
            next_state <= to_bc;
          end if;
        end if;

        -- wait for 40
        when wait40 =>
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        else
          if(trig = '0') then
            next_state <= pre_read;
            -- out_flag <= '0';
            en_data_next <= '0';
            search_finished <= '0';
            trig_reset <= '0';
            -- if (unsigned(r_reg)=to_unsigned(height,addr_size)-1) then
            --   r_next <= (others => '0');
            -- else
            --   r_next <= r_reg + to_unsigned(1,addr_size);
            -- end if;
            -- out_flag <= '0';
            -- out_d <= std_logic_vector(bc_counter_reg) & r_d_data;
          elsif(HALF_BC > unsigned(trig_id) - bc_counter_reg
            and ZERO_BC < unsigned(trig_id) - bc_counter_reg)
            then
            next_state <= to_next;

            if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            -- bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          end if;
        end if;

        -- wait for count
        when wait_count =>
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        else
          en_data_next <= '0';
          if(trig = '0') then
            --next_state <= wait_count;
            -- out_d <= trig_id & DATA_EMPTY;
            search_finished <= '0';
            if(read_cmd = '0')then
              read_finished <= '0';
              read_cmd_reset <= '0';
              out_flag <= '1';
              -- r_next <= r_reg;
              trig_reset <= '0';
            end if;
          -- Here could be added an addditional check to see if
          -- the trig_id is higher than r_d_bc. In this case some errors may have
          -- occurred and we move the pointer. Otherwise the syncronisation will
          -- be recovered only after (it's not a great advantage,
          -- also considering statistics)
          elsif(HALF_BC > unsigned(trig_id)-unsigned(r_d_bc)
            and unsigned(trig_id)-unsigned(r_d_bc) >= ZERO_BC)
            then

            next_state <= wait40;
            -- r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
              bc_counter_next <= unsigned(r_d_bc(bc_width -1 downto 0));
                              -- - to_unsigned(1,bc_width);
          elsif(HALF_BC > unsigned(trig_id) - bc_counter_reg
            and ZERO_BC < unsigned(trig_id) - bc_counter_reg)
            then
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          end if;
        end if;

        -- to_bc
        when to_bc =>
        -- if(err_hamm = '0') then
        --   next_state <= to_bc;
        --   err <= '0';
        --   if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
        --     r_next <= (others => '0');
        --   else
        --     r_next <= r_reg + to_unsigned(1,addr_size);
        --   end if;
        -- else
          if(head = COUNTER) then
            next_state <= resync;
            en_data_next <= '0';
          else
            if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            err <= '0';
          end if;
        -- end if;

        -- resync
        when resync =>
        if(err_hamm = '0') then
          next_state <= to_bc;
          err <= '0';
          if (unsigned(r_reg)=to_unsigned(height-1,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
        else
          next_state <= wait_count;
          en_data_next <= '0';
          bc_counter_next <= unsigned(r_d_bc(bc_width -1 downto 0));
        end if;

      end case;
    end process;


    --------------------------------------
    ---------- NEXT STATE LOGIC ----------
    --------------------------------------

    next_state_ff : process(clock, setup)
    begin
      if (setup = '0') then
        state <= setting;
      elsif (clock'event and clock = '1') then
        state <= next_state;
      end if;
    end process;

    next_num_ff : process(clock, setup)
    begin
      if (setup = '0') then
        r_reg <= (others => '0');
        en_data <= '0';
        bc_counter_reg <= (others => '0');
      elsif (clock'event and clock = '1') then
        r_reg <= r_next;
        en_data <= en_data_next;
        bc_counter_reg <= bc_counter_next;
      end if;
    end process;

    bc_comparator : process(clock, setup)
    begin
      if (setup = '0') then
        trig <= '1';
      elsif (clock'event and clock = '1') then
        if (bc_counter_reg = unsigned(trig_id) and trig_flag = '0') then
          trig <= '0';
        else
          trig <= '1';
        end if;
      end if;
    end process;

    out_d_ff : process(clock, setup)
    begin
      if(setup = '0')then
        search_finished_out <= '1';
        -- read_finished_out <= '1';
        out_ff <= (others => '0');
        out_flag_ff <= '1';
        err_out <= '0';
      elsif(clock'event and clock ='1') then
        search_finished_out <= search_finished;
        -- read_finished_out <= read_finished;
        out_ff <= out_d;
        out_flag_ff <= out_flag;
        err_out <= err;
      end if;
    end process;


    out_read_ff : process(clock, setup)
    begin
      if(setup = '0')then
        read_finished_out <= '1';
      elsif(clock'event and clock ='1') then
        if(read_finished = '0')then
          read_finished_out <= '0';
        elsif(read_reset = '0') then
          read_finished_out <= '1';
        end if;
      end if;
    end process;

    -- search_finished : search_finished <= trig;
    addr_conv : r_addr <= std_logic_vector(r_reg);

  end comb;
