`timescale 1ns/1ps

module valid_gen(
  input [16*16-1:0]pix,
  output logic [16-1:0]valid);

   genvar j;
   generate
     for(j=0;j<16;j++)
     begin : loop
       always_comb
       begin
         valid[j]<=|(pix[(16*(j+1)-1):(16*j)]);
       end
     end
   endgenerate
endmodule

/*
module sync_mem#(
  parameter sc=16,
  parameter r_bc_w=3,
  parameter gr_w = 5,
  parameter fc_w=4
  )
  (
  input [sc*r_bc_w-1:0]in_bc,
  input [sc*16-1:0]in_pix, // Pixel in per mem syn output
  input [sc*gr_w-1:0]in_groupid, // Groupid per mem syn output
  input [sc*fc_w-1:0]in_finecounter, // fine counter per mem sync output
  input [sc-1:0]read, // Lock the sync mem

  output logic [r_bc_w-1:0]out_bc,
  output logic [16-1:0]out_pix, // Pixel in per mem syn output
  output logic [gr_w-1:0]out_groupid, // Groupid per mem syn output
  output logic [fc_w-1:0]out_finecounter, // fine counter per mem sync output
  output logic [sc-1:0]valid); //assertion on one sync memory

  // logic [r_bc_w-1:0]pre_out_bc;
  // logic [16-1:0]pre_out_pix; // Pixel in per mem syn output
  // logic [gr_w-1:0]pre_out_groupid; // Groupid per mem syn output
  // logic [fc_w-1:0]pre_out_finecounter; // fine counter per mem sync output

    genvar i;
    generate
    for(i=0;i<16;i++)
    begin : merge_loop
      always
      begin
        if(read[i]==1'b1)
        begin
          // #(0.7)pre_out_bc[(i+1)*r_bc_w-1:i*r_bc_w]<=in_bc[(i+1)*r_bc_w-1:i*r_bc_w];
          // #(0.7)pre_out_pix[(i+1)*16-1:i*16]<=in_pix[(i+1)*16-1:i*16];
          // #(0.7)pre_out_groupid[(i+1)*gr_w-1:i*gr_w]<=in_groupid[(i+1)*gr_w-1:i*gr_w];
          // #(0.7)pre_out_finecounter[(i+1)*fc_w-1:i*fc_w]<=in_finecounter[(i+1)*fc_w-1:i*fc_w];
          #(0.7)out_bc<=in_bc[(i+1)*r_bc_w-1:i*r_bc_w];
          #(0.7)out_pix<=in_pix[(i+1)*16-1:i*16];
          #(0.7)out_groupid<=in_groupid[(i+1)*gr_w-1:i*gr_w];
          #(0.7)out_finecounter<=in_finecounter[(i+1)*fc_w-1:i*fc_w];
        end
      end
      always_comb
      begin
        valid[i]<=|in_pix[(i+1)*16-1:i*16];
      end
    end
    endgenerate
endmodule
*/


module sync_mem#(
//  parameter sc=16,
  parameter r_bc_w=3,
  parameter gr_w = 5,
  parameter fc_w=4
  )
  (
  input rstb,
  input [r_bc_w-1:0]in_bc,
  input [16-1:0]in_pix, // Pixel in per mem syn output
  input [gr_w-1:0]in_groupid, // Groupid per mem syn output
  input [fc_w-1:0]in_finecounter, // fine counter per mem sync output
  input read,

  output logic [r_bc_w+16+gr_w+fc_w-1:0]out,
//  output logic [16-1:0]out_pix, // Pixel in per mem syn output
//  output logic [gr_w-1:0]out_groupid, // Groupid per mem syn output
//  output logic [fc_w-1:0]out_finecounter, // fine counter per mem sync output
  output logic valid); //assertion on one sync memory

  // logic [r_bc_w-1:0]pre_out_bc;
  // logic [16-1:0]pre_out_pix; // Pixel in per mem syn output
  // logic [gr_w-1:0]pre_out_groupid; // Groupid per mem syn output
  // logic [fc_w-1:0]pre_out_finecounter; // fine counter per mem sync output
  logic [r_bc_w+16+gr_w+fc_w-1:0]pre_out;
  logic pre_valid;

      always @(posedge read, negedge rstb)
      begin
        if(read==1'b1)
        begin
          #(0.7)
          out<=pre_out;
          #(1.56)
          out<='0;
        end
        else if(rstb==1'b0)
        begin
          out<='0;
        end
      end

      always @(posedge read, negedge rstb, posedge pre_valid)
      begin
        if(read==1'b1)
        begin
          #(1)
          valid<=pre_valid;
          #(1.56)
          valid<=1'b0;
        end
        else if(pre_valid==1'b1)
        begin
          valid<=pre_valid;
        end
        else if(rstb==1'b0)
        begin
          valid<=1'b0;
        end
      end

      always_latch
      begin
        if(!rstb)
        begin
          pre_valid<=1'b0;
          pre_out<='0;
        end
        else if(read)
        begin
          pre_valid<=1'b0;
        end
        else if(|in_pix==1'b1)
        begin
          pre_valid<=|in_pix;
          pre_out<={in_bc,in_pix,in_groupid,in_finecounter};
        end
      end
endmodule

  //Period on NS
/*  module clk#(parameter period=1)(
       output logic clk);

       initial
            clk=0;

       always
       begin
            #(period)clk<=!clk;
       end
  endmodule
*/

module tb_memory_module_forsyn ();
     localparam FINE={{4'h0},{4'h1},{4'h2},{4'h3},{4'h4},{4'h5},{4'h6},{4'h7},{4'h0},{4'h1},{4'h2},{4'h3},{4'h4},{4'h5},{4'h6},{4'h7}};//48'hd;
     localparam GROUP={{6'h00},{6'h01},{6'h02},{6'h03},{6'h04},{6'h05},{6'h06},{6'h07},{6'h08},{6'h09},{6'h0a},{6'h0b},{6'h0c},{6'h0d},{6'h0e},{6'h0f}};//80'h1d;
     localparam DELAY=12.48;
     logic clk;
     logic bc_clk;
     logic rstb;
     logic [15:0]data;
     logic [15:0]valid;
     logic [15:0]valid_auto;
     logic [16*16-1:0]data_array;
     logic [3*16-1:0]r_bc_in;
     logic [5:0]head_out;
     logic read_reset;
     logic read_cmd;
     logic trig;
     logic trig_flag;
     logic [10:0]trig_id;

     logic [16-1:0]single_pix;
     logic [3-1:0]single_r_bc;
     logic [6-1:0]single_groupid;
     logic [4-1:0]single_fine;


     logic [27:0]data_out;
     logic data_flag;
     logic search_finished;
     logic read_finished;
     logic err;
     logic read_cmd_reset;
     logic [15:0]read_16;

     logic [3-1:0]search_bc_sync;


 logic [16+4+5+3-1:0] pre_or [16-1:0];
// logic [16+4+5+3-1:0] post_or;
 logic [16*4-1:0] fine_vec;

  //   valid_gen valid_generator(.pix(data_array),.valid(valid_auto));
/*
    sync_mem
    #(
      .sc(16),
      .r_bc_w(3),
      .gr_w(5),
      .fc_w(4)
      )
    sync_mem_sym(
      .in_bc(r_bc_in),
      .in_pix(data_array),
      .in_groupid(GROUP),
      .in_finecounter(FINE),
      .read(read_16),
      .out_bc(single_r_bc),
      .out_pix(single_pix),
      .out_groupid(single_groupid),
      .out_finecounter(single_fine),
      .valid(valid_auto));
*/
genvar j;
generate
for(j=0; j<16; j++)
begin: mem_loop
    sync_mem
    #(
      //.sc(16),
      .r_bc_w(3),
      .gr_w(5),
      .fc_w(4)
      )
    sync_mem_sym(
      .rstb(rstb),
      .in_bc(r_bc_in[(j+1)*3-1:3*j]),
      .in_pix(data_array[(j+1)*16-1:16*j]),
      .in_groupid(GROUP[(j+1)*5-1:5*j]),
      .in_finecounter(FINE[(j+1)*4-1:4*j]),
      .read(read_16[j]),
      .out(pre_or[j]),
      //.out_pix(single_pix),
      //.out_groupid(single_groupid),
      //.out_finecounter(single_fine),
      .valid(valid_auto[j]));
end
endgenerate

always_comb
begin
  {single_r_bc,single_pix,single_groupid,single_fine}<=pre_or[0]|pre_or[1]|pre_or[2]|pre_or[3]|pre_or[4]|pre_or[5]|pre_or[6]|pre_or[7]|pre_or[8]|pre_or[9]|pre_or[10]|pre_or[11]|pre_or[12]|pre_or[13]|pre_or[14]|pre_or[15];
end

// original clk is 1.56
     clk #(.period(1.56)) inst_clk(.clk(clk));
     clk #(.period(12.48)) inst_bc_clk(.clk(bc_clk));
memory_module
  //#(.sc (16),
    // .fc_w(4),
  //   .bc_w(11),
  //   .gr_w(5),
  //   .head_w(6),
  //   .height(100),
  //   .addr_w(10)
  //   )
inst_mem_mod(
               .clk(clk),
               .rstb(rstb),
               .bc_clk(bc_clk),
               //.r_bc_in(single_r_bc),
               .pix_in(single_pix),
               .groupid(single_groupid),
               .finecounter(single_fine),
               .valid(valid_auto),
               // .read_reset(read_reset),
               // .read_cmd_in(read_cmd),
               .trig(read_cmd),
               // .trig_id(trig_id),
               .search_bc_sync(search_bc_sync),
               .read(read_16),

               .data_out(data_out),
               .out_flag(data_flag),
               // .search_finished(search_finished),
               // .read_finished(read_finished),
               .err_out(err_out));
    initial
    begin
    fork
      begin
// Reset
        rstb=1'b1;
        #(25.5);
        #(DELAY*5); rstb=1'b0;
        #(DELAY*3); rstb=1'b1;
      end
      begin

              data = '0;
              read_reset='0;
              data_array={16{data}};
              valid = '0;
              read_cmd=1'b1;
              trig_id=11'h000;
              trig_flag=1'b0;
      #(25.5);
      #(DELAY*5);
              trig_flag=1'b0;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{14{16'h0000}},{data},{16'h0000}};
              valid=16'h0001;
      #(DELAY*3);
      #(DELAY*2);
              read_cmd=1'b1;
              trig_id=11'h000;
              trig_flag=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{3{data}},{13{16'h0000}}};
              valid=16'h0000;


      #(DELAY);
              trig_id=11'h000;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h001}};
              data=16'h2040;
              data_array={{16'h0000},{data},{14{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              read_cmd=1'b1;
              trig_flag=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{4{data}},{12{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h001;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h002}};
              data=16'h0200;
              data_array={{3{data}},{13{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h001;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{12{16'h0000}},{4{data}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h002;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h003}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h002;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{12{16'h0000}},{data},{2{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h003;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h004}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h004;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{15{16'h0000}},{data}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h005;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h005}};
              data=16'h2040;
              data_array={{4{data}},{12{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h006;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              read_cmd=1'b1;
              trig_flag=1'b1;
              trig_id=11'h007;
              r_bc_in={16{3'h006}};
              data=16'h2000;
              data_array={{6{data}},{10{16'h0000}}};
              valid=16'h3004;
      #(1);
      // #(DELAY*2);
              trig_id=11'h008;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{5{data}},{11{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h009;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h007}};
              data=16'h0040;
              data_array={{12{16'h0000}},{4{data}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h00a;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h00b;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h008}};
              data=16'h2040;
              data_array={{data},{12{16'h0000}},{data},{2{16'h0000}}};
              valid=16'hffff;
      #(DELAY*2-3.5);
      read_cmd=1'b0;
      #(3.5);
              trig_id=11'h00d;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h009}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(1);
      // #(DELAY*2);
              trig_id=11'h008;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{5{data}},{11{16'h0000}}};
              valid=16'h0000;
      #(DELAY*2-1);
      // #(0);
              trig_id=11'h00f;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h00a}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(3.5);
              trig_id=11'h00f;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h00a}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(DELAY*2-3.5);
              read_cmd=1'b1;
              trig_id=11'h010;
              trig_flag=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{6{data}},{10{16'h0000}}};
              valid=16'h0000;
      #(0);
              trig_id=11'h011;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h00b}};
              data=16'h0200;
              data_array={{5{data}},{11{16'h0000}}};
              valid=16'hffff;
      #(DELAY*2);
              trig_id=11'h013;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h00c}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(3.5);
              trig_id=11'h013;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h00c}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(DELAY-3.5)
      #(DELAY);
              trig_id=11'h014;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{12{16'h0000}},{data},{2{16'h0000}}};
              valid=16'h0000;
      #(0);
              trig_id=11'h015;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h00d}};
              data=16'h2040;
              data_array={{15{16'h0000}},{data}};
              valid=16'hffff;
      #(1);
              trig_id=11'h016;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'h0000;
      #(0);
      #(DELAY*2-1);
              trig_id=11'h017;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h00e}};
              data=16'h2040;
              data_array={{15{16'h0000}},{data}};
              valid=16'hffff;
      #(3.5);
              trig_id=11'h017;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h00e}};
              data=16'h2040;
              data_array={{2{data}},{13{16'h0000}},{data}};
              valid=16'hffff;
      #(DELAY-3.5)
      #(DELAY);
              read_cmd=1'b1;
              trig_id=11'h019;
              trig_flag=1'b1;
              r_bc_in={16{3'h00f}};
              data=16'h2000;
              data_array={{6{data}},{10{16'h0000}}};
              valid=16'h3004;
      #(1);
              trig_id=11'h01a;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{5{data}},{11{16'h0000}}};
              valid=16'h0000;
      #(0);
      #(DELAY*2-1);
              trig_id=11'h01b;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h010}};
              data=16'h0040;
              data_array={{12{16'h0000}},{4{data}}};
              valid=16'hffff;
      #(3.5);
              trig_id=11'h01b;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h010}};
              data=16'h0000;
              data_array={{12{16'h0000}},{4{data}}};
              valid=16'hffff;
      #(2*DELAY-3.5)
              trig_id=11'h01c;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{15{16'h0000}}};
              valid=16'h0000;
      #(0);
              trig_id=11'h01d;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h011}};
              data=16'h2040;
              data_array={{data},{12{16'h0000}},{data},{2{16'h0000}}};
              valid=16'hffff;
      #(3.5);
              read_cmd=1'b1;
      #(2*DELAY-3.5);
              trig_id=11'h01e;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{data},{14{16'h000}},{data}};
              valid=16'h0000;
      #(0);
              trig_id=11'h01f;
              trig_flag=1'b0;
              read_cmd=1'b0;
              r_bc_in={16{3'h012}};
              data=16'h2040;
              data_array={{data},{15{16'h0000}}};
              valid=16'hffff;
      #(3.5);
              read_cmd=1'b1;
      #(2*DELAY-3.5);
              trig_id=11'h020;
              trig_flag=1'b0;
              read_cmd=1'b1;
              r_bc_in={16{3'h000}};
              data=16'h0000;
              data_array={{13{16'h0000}},{3{data}}};
              valid=16'h0000;

      #(0);
          end
          join
          $finish;
     end


endmodule

//Period on NS
module clk#(parameter period=1)(
     output logic clk);

     initial
          clk=0;

     always
     begin
          #(period)clk<=!clk;
     end
endmodule
