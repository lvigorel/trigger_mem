`timescale 1ns/10ps

module tb_readout ();
     localparam FINE=48'hd;
     localparam GROUP=80'h1d;
     localparam DELAY=23;
     logic clk;
     logic rstb;
     logic [15:0]data;
     logic [16*16-1:0]data_array;
     logic [15:0]valid;
     logic [16:0]data_out;
     logic [3*16-1:0]r_bc_in;
     logic data_flag;
     logic [5:0]head_out;

     clk #(.period(1.66)) inst_clk(.clk(clk));
     readout#(.sc (16),
     .fc_w(3),
     .r_bc_w(3),
     .full_bc_w(11),
     .gr_w(5)
     )inst_readout(
               .clk(clk),
               .rstb(rstb),
               .bc_in(bc_in),
               .pix_in(data_array),
               .groupid(GROUP),
               .finecounter(FINE),
               .valid(valid),
               .busy(),
               .data_flag(data_flag),
               .data_out(data_out),
               .reg_head(head_out));
     initial
     begin
		fork
		begin
	 	  // Reset
			rstb=1'b1;
			#(DELAY*5); rstb=1'b0;
			#(DELAY*1); rstb=1'b1;
		end
		begin

			data = '0;
               data_array={16{data}};
               valid = '0;
			#(DELAY*10);
               bc_in={16{11'h000}};
               data=16'hffff;
               data_array={{15{16'h0000}},{data}};
               valid=16'h0001;
			#(DELAY*10);
               bc_in={16{11'h001}};
               data=16'hffff;
               data_array={16{data}};
               valid=16'h4006;
      #(DELAY*10);
               bc_in={16{11'h003}};
               data=16'hffff;
               data_array={16{data}};
               valid=16'hffff;
			#(DELAY*30);
          end
          join
          $finish;
     end


endmodule

//Period on NS
module clk#(parameter period=1)(
     output logic clk);

     initial
          clk=0;

     always
     begin
          #(period)clk<=!clk;
     end
endmodule
