library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity bc_h_dec is
  port(
    code_in : in std_logic_vector(15-1 downto 0);
    code_out : out std_logic_vector(15-1 downto 0);
    err :out std_logic
  );
end bc_h_dec;

architecture comb of bc_h_dec is

  -- Hamming decoder (for BCID) signals

  signal p : std_logic_vector(4 downto 0);
  signal s : std_logic_vector(3 downto 0);
  signal corr : std_logic_vector(15-1 downto 0);


  begin

    -----------------------------------------
    -- Hamming (15,11) with additional parity bit (16,11).
    -- 11 is enought for the BCID
    -----------------------------------------

    p_0 : p(0) <= code_in(2) xor code_in(4) xor code_in(6) xor code_in(8) xor code_in(10)
        xor code_in(12);-- xor code_in(14);
    p_1 : p(1) <= code_in(2) xor code_in(5) xor code_in(6) xor code_in(9) xor code_in(10)
        xor code_in(13);-- xor code_in(14);
    p_2 : p(2) <= code_in(4) xor code_in(5) xor code_in(6) xor code_in(11) xor code_in(12)
        xor code_in(13);-- xor code_in(14);
    p_3 : p(3) <= code_in(8) xor code_in(9) xor code_in(10) xor code_in(11) xor code_in(12)
        xor code_in(13);-- xor code_in(14);
    p_5 : p(4) <= code_in(0) xor code_in(1) xor code_in(2) xor code_in(3) xor code_in(4)
        xor code_in(5) xor code_in(6) xor code_in(7) xor code_in(8) xor code_in(9) xor code_in(10)
        xor code_in(11) xor code_in(12) xor code_in(13);-- xor code_in(14);

    s_0 : s(0) <= p(0) xor code_in(0);
    s_1 : s(1) <= p(1) xor code_in(1);
    s_2 : s(2) <= p(2) xor code_in(3);
    s_3 : s(3) <= p(3) xor code_in(7);

    decoder_gen:
    for i in 0 to 14 generate
      corr(i) <=  '1' when to_unsigned(i,4) = unsigned(s(3 downto 0)) else
                  '0';
    end generate;

    pre_corr: code_out(13 downto 0)<=code_in(13 downto 0)xor corr(14 downto 1);

    --check if there is both a single correction inside the 15,11
    --(by checking (not corr(0)) and no error in overall parity (p(4) is overall
    --parity that should be 0)
    --In this case two errors have occurred, changing something in the 15,11 but
    -- compensating each other in the overall parity, leaving it correct
    --err_l : err <= (not corr(0)) and (not(p(4)));
    -- reimplementation in negative logic
    err_l : err <= (corr(0) or (p(4) xor code_in(14)));

    -- toggle corrected(15) if there has been only one error
    corr_l : code_out(14) <= p(4) xor (not corr(0));

end comb;
