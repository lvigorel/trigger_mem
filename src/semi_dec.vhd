library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


entity semi_dec is
generic (
    addr_size : natural:=10;
    out_size : natural:=850
    );
port (
		enable : in std_logic;
		addr : in std_logic_vector(addr_size - 1 downto 0);
		one_hot : out std_logic_vector(out_size - 1 downto 0)
	);
end semi_dec;

architecture behav of semi_dec is

begin

--for!! "think of for as a replicated structure"

	process(addr, enable)
	begin
		for i in 0 to out_size - 1 loop
			if (unsigned(addr) = i and enable = '1') then
				one_hot(i) <= '1';
			else
				one_hot(i) <= '0';
			end if;
		end loop;
	end process;

end behav;
