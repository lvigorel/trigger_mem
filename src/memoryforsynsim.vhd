library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

  entity memory_addr_w8_height250_d_width18_h_width6 is
    generic(
      addr_w : natural:=8;
      height : natural:=250;
      d_width : natural:=18;
      h_width : natural:=6
    );
    port(
      clk : in std_logic;
      rstb : in std_logic;

      data_wr : in std_logic_vector(h_width+d_width-1 downto 0);

      w_addr : in std_logic_vector(addr_w-1 downto 0);
      r_addr : in std_logic_vector(addr_w-1 downto 0);
      data_en : in std_logic;

      data_rd : out std_logic_vector(h_width+d_width-1 downto 0)
    );
  end memory_addr_w8_height250_d_width18_h_width6; 

architecture arch of memory_addr_w8_height250_d_width18_h_width6 is

	component semi_dec
  generic(
    addr_size : natural;
    out_size : natural
  );
	port (
    enable : in std_logic;
		addr : in std_logic_vector(addr_size-1 downto 0);
		one_hot : out std_logic_vector(out_size-1 downto 0)
	);
  end component;

  component memory_cell_array
  generic(
    height : natural;
    width : natural
  );
	port (
		w : in std_logic_vector(height-1 downto 0);
		r : in std_logic_vector(height-1 downto 0);

		w_d : in std_logic_vector(width-1 downto 0);
		r_d : out std_logic_vector(width-1 downto 0)
	);
  end component;

  --decoder signals

	signal w : std_logic_vector(height-1 downto 0);
	signal r : std_logic_vector(height-1 downto 0);

  signal read_enable : std_logic;
  signal write_enable : std_logic;

begin

--------------------
----READ CONTROL----
--------------------
  read_enable <= '1';

  read_dec : semi_dec
  generic map(
    addr_size => addr_w,
    out_size => height
  )
	port map (
    enable => read_enable,
		addr => r_addr,
		one_hot => r
	);


---------------------
----WRITE CONTROL----
---------------------
  -- during clock = 1 data is prepared, then is enabled
  -- we could do this even
  write_enable <= not clk;

  write_dec : semi_dec
  generic map(
    addr_size => addr_w,
    out_size => height
  )
	port map (
    enable => write_enable,
		addr => w_addr,
		one_hot => w
	);

----------------------
--------MEMORY--------
----------------------
  data_memory : memory_cell_array
  generic map(
    height => height,
    width => d_width
  )
  port map(
    w => w,
    r => r,

    w_d => data_wr(d_width-1 downto 0),
    r_d => data_rd(d_width-1 downto 0)
  );

  head_memory : memory_cell_array
  generic map(
    height => height,
    width => h_width
  )
  port map(
    w => w,
    r => r,

    w_d => data_wr(h_width+d_width-1 downto d_width),
    r_d => data_rd(h_width+d_width-1 downto d_width)
  );

end arch;
