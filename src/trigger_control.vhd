library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity trigger_control is
  generic(
    bc_width : natural := 10
  );
  port(
    clock : in std_logic;
    bc_clock : in std_logic;
    reset : in std_logic;

    trig : in std_logic;
    total_read_finished : in std_logic;
    read_finished_reset : out std_logic;

    read_cmd : out std_logic; -- cmd bc read
    read_cmd_reset : in std_logic;
    search_flag : out std_logic; -- cmd bc search
    search_id : out std_logic_vector(bc_width-1 downto 0); -- bc to search
    -- bc_flag : out std_logic;
    bc_id : out std_logic_vector(bc_width-1 downto 0) -- bc for writing
  );
end trigger_control;

architecture fast of trigger_control is
  type STATE_TYPE is (search, read);
  CONSTANT LATENCY : natural := 500;

  constant HALF_BC :
  unsigned(bc_width-1 downto 0) := to_unsigned(2**(bc_width-1), bc_width);
  constant ZERO_BC : unsigned(bc_width-1 downto 0) := (others => '0');

  signal state, next_state : STATE_TYPE;

  signal bc_counter_reg : unsigned(bc_width-1 downto 0);
  signal bc_counter_next : unsigned(bc_width-1 downto 0);

  -- signal bc_flag_reg : std_logic;
  -- signal bc_flag_next : std_logic;

  signal search_id_reg : std_logic_vector(bc_width-1 downto 0);
  signal search_id_next : std_logic_vector(bc_width-1 downto 0);
  signal search_id_triggered : std_logic_vector(bc_width-1 downto 0);
  signal search_counter_reg : std_logic_vector(bc_width-1 downto 0);
  signal search_counter_next : std_logic_vector(bc_width-1 downto 0);

  signal read_cmd_next : std_logic;
  signal trig_flag : std_logic;
  signal trig_reset : std_logic;
  signal search_flag_next : std_logic;
  signal read_reset_next : std_logic;

  --------------------------------------------------------------

  begin

    -- FF with enable. Search_ID number locker
    trig_id_ff : process(clock, reset)
    begin
      if(reset = '0')then
        search_id_triggered <= (others=>'0');
      elsif(clock'event and clock = '1') then
        if(trig = '0')then
          search_id_triggered <= search_counter_reg;
        end if;
      end if;
    end process;

    trig_flag_ff : process(clock, reset)
    begin
      if(reset = '0')then
        trig_flag <= '1';
      elsif(rising_edge(clock)) then
        if(trig = '0')then
          trig_flag <= '0';
        elsif(trig_reset = '0') then
          trig_flag <= '1';
        end if;
      end if;
    end process;

    read_finished_reset_ff : process(clock, reset)
    begin
      if(reset = '0')then
        read_finished_reset <= '1';
      elsif(rising_edge(clock)) then
        read_finished_reset <= read_reset_next;
      end if;
    end process;

    ----------------------------------------------------------------

    search_flag_ff : process(clock, reset)
    begin
      if(reset = '0')then
        search_flag <= '1';
        -- read_cmd <= '1';
      elsif(rising_edge(clock)) then
        search_flag <= search_flag_next;
        -- read_cmd <= read_cmd_next;
      end if;
    end process;

    read_cmd_ff : process(clock, reset)
    begin
      if(reset = '0')then
        read_cmd <= '1';
      elsif(rising_edge(clock)) then
        if(read_cmd_next = '0')then
          read_cmd <= '0';
        elsif(read_cmd_reset = '0') then
          read_cmd <= '1';
        end if;
      end if;
    end process;

    search_id_ff : process(clock, reset)
    begin
      if(reset = '0')then
        search_id_reg <= (others => '0');
      elsif(falling_edge(clock)) then
        search_id_reg <= search_id_next;
      end if;
    end process;

    search_id <= search_id_reg;

    state_machine : process(total_read_finished, trig_flag,
      search_id_reg, search_counter_reg, state, search_id_triggered)
    begin
      trig_reset <= '1';
      read_reset_next <= '1';
      case state is
        -- reading
        when read =>
          if(total_read_finished = '1')then
            search_id_next <= search_id_reg;
            search_flag_next <= '1';
            next_state <= read;
            read_cmd_next <= '1';
          elsif(total_read_finished = '0')then
-- memory for a single trigger counter (and trig_flag) inside the ff
            if (trig_flag = '1') then
              search_id_next <= search_counter_reg;
              next_state <= search;
              read_cmd_next <= '1';
            elsif (trig_flag = '0') then
              trig_reset <= '0';
              search_id_next <= search_id_triggered;
              next_state <= read;
              read_cmd_next <= '0';
            end if;
            search_flag_next <= '0';
            read_reset_next <= '0';
          end if;

        -- searching
        when search =>
          if(trig_flag = '1')then
            search_id_next <= search_counter_reg;
            search_flag_next <= '0';
            next_state <= search;
            read_cmd_next <= '1';
          elsif(trig_flag = '0')then
            trig_reset <= '0';
            search_id_next <= search_id_triggered;
            search_flag_next <= '0';
            next_state <= read;
            read_cmd_next <= '0';
          end if;
      end case;
    end process;

    -- bc_flag_proc : process(clock, reset)
    -- begin
    --   if(reset = '0') then
    --     bc_flag_reg <= '0';
    --     state <= search;
    --   elsif(rising_edge(clock)) then
    --     bc_flag_reg <= bc_flag_next;
    --     state <= next_state;
    --   end if;
    -- end process;
    --
    next_state_proc : process(clock, reset)
    begin
      if(reset = '0') then
        -- bc_flag_reg <= '0';
        state <= search;
      elsif(rising_edge(clock)) then
        -- bc_flag_reg <= bc_flag_next;
        state <= next_state;
      end if;
    end process;

    -- bc_flag_next <= not bc_flag_reg;
    -- bc_flag <= bc_flag_reg;

    bc_counter_proc : process(bc_clock, reset)
    begin
      if(reset = '0') then
        bc_counter_reg <= (others => '0');
      elsif(falling_edge(bc_clock)) then
        bc_counter_reg <= bc_counter_next;
      end if;
    end process;

    search_counter_proc : process(bc_clock, reset)
    begin
      if(reset = '0') then
        search_counter_reg <= (others => '0');
      elsif(rising_edge(bc_clock)) then
        search_counter_reg <= search_counter_next;
      end if;
    end process;


    -- need a Gray encoder!!
    --  and also check if and how much is demanding to decode in each memory
    -- bc_counter_next <= bc_counter_reg + to_unsigned(1, bc_width+3);
    bc_counter_next <= bc_counter_reg + to_unsigned(1, bc_width);
    -- search_finished : search_finished <= trig;
    -- late_diff : search_counter_next <=
    --             std_logic_vector(bc_counter_reg(bc_width-1+3 downto 3)-to_unsigned(latency, bc_width));
    late_diff : search_counter_next <=
                std_logic_vector(bc_counter_reg-to_unsigned(latency, bc_width));
    -- bc_id_val : bc_id <= std_logic_vector(bc_counter_reg(bc_width-1+3 downto 3));
    bc_id_val : bc_id <= std_logic_vector(bc_counter_reg);

  end fast;
