library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- use IEEE.STD_LOGIC_ARITH.ALL;
-- use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity hd_dec is
  generic(
    width : natural := 21;
    code_width : natural := 2
    );
  port (
    clock : in std_logic;
    setup : in std_logic;

    code_in : in std_logic_vector(width - 1 downto 0);
    r_move : in std_logic;
    corrected_out : out std_logic_vector(width - 1 downto 0);
    err_out : out std_logic
    );
  end hd_dec;


architecture arch of hd_dec is

  signal p : std_logic_vector(4 downto 0);
  signal s : std_logic_vector(3 downto 0);
  signal corr : std_logic_vector(16-1 downto 0);

  signal code : std_logic_vector(16-1 downto 0);
  signal corrected : std_logic_vector(16-1 downto 0);

  signal corrected_out_reg : std_logic_vector(width-1 downto 0);
  signal corrected_out_next : std_logic_vector(width-1 downto 0);
  signal pre_corrected_out : std_logic_vector(width-1 downto 0);

  signal err_out_reg : std_logic;
  signal err_out_next : std_logic;
  signal err : std_logic;
  -----------------------------------------
  -- Hamming (15,11) with additional parity bit (16,11).
  -- 11 is enought for the BCID
  -- This choice is because even if we could use more bits for parity
  -- they are not necessary and we don't know the final number
  -- and also because adding more bits also adds more unnecessary complexity

begin
  code <= code_in(15 downto 0);
  intersection : process(code_in, corrected)
  begin
    if(code_in(width-1 downto width-2) = "11")then
      pre_corrected_out <= "11000" & corrected;
    else
      pre_corrected_out <= code_in;
    end if;
  end process;
  -- FF with enable --
  out_d_ff : process(clock, setup)
  begin
    if(setup = '0')then
      corrected_out_reg <= (others => '0');
      err_out_reg <= '0';
    elsif(clock'event and clock ='1') then
      corrected_out_reg <= corrected_out_next;
      err_out_reg <= err_out_next;
    end if;
  end process;
  -- state machine for FF output control --
  corrected_out_next <= pre_corrected_out when r_move = '0' else
                        corrected_out_reg;
  err_out_next <= err when r_move = '0' else
                  err_out_reg;
  corrected_out <= corrected_out_reg;
  err_out <= err_out_reg;
  -----------------------------------------
  -- Hamming (15,11) with additional parity bit (16,11).
  -- 11 is enought for the BCID

  p_0 : p(0) <= code(2) xor code(4) xor code(6) xor code(8) xor code(10)
      xor code(12) xor code(14);
  p_1 : p(1) <= code(2) xor code(5) xor code(6) xor code(9) xor code(10)
      xor code(13) xor code(14);
  p_2 : p(2) <= code(4) xor code(5) xor code(6) xor code(11) xor code(12)
      xor code(13) xor code(14);
  p_3 : p(3) <= code(8) xor code(9) xor code(10) xor code(11) xor code(12)
      xor code(13) xor code(14);
  p_5 : p(4) <= code(0) xor code(1) xor code(2) xor code(3) xor code(4)
      xor code(5) xor code(6) xor code(7) xor code(8) xor code(9) xor code(10)
      xor code(11) xor code(12) xor code(13) xor code(14) xor code(15);

  s_0 : s(0) <= p(0) xor code(0);
  s_1 : s(1) <= p(1) xor code(1);
  s_2 : s(2) <= p(2) xor code(3);
  s_3 : s(3) <= p(3) xor code(7);

  decoder_gen:
  for i in 0 to 15 generate
    corr(i) <=  '1' when to_unsigned(i,4) = unsigned(s(3 downto 0)) else
                '0';
  end generate;

  pre_corr : corrected(14 downto 0) <= code(14 downto 0) xor corr(15 downto 1);

  --check if there is both a single correction inside the 15,11
  --(by checking (not corr(0)) and no error in overall parity (p(4) is overall
  --parity that shiuld be 0)
  --In this case two errors have occurred, changing something in the 15,11 but
  -- compensating each other in the overall parity, leaving it correct
  err_l : err <= (not corr(0)) and (not(p(4)));
  -- toggle corrected(15) if there has been only one error
  corr_l : corrected(15) <= p(4) xor (not corr(0));
end arch;
