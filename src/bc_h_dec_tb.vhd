library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity bc_h_dec_tb is

end bc_h_dec_tb;

architecture comb of bc_h_dec_tb is

  -- Hamming decoder (for BCID) signals

  signal code_in_sim : std_logic_vector(15 downto 0);
  signal code_out_sim : std_logic_vector(15 downto 0);
  signal err_sim : std_logic;

  component bc_h_dec
  port(
  code_in : in std_logic_vector(15 downto 0);
  code_out : out std_logic_vector(15 downto 0);
  err : out std_logic
  );
  end component;


  begin

  uut : bc_h_dec
  port map(
    code_in => code_in_sim,
    code_out => code_out_sim,
    err => err_sim
  );

  stim_proc : process
  begin
    code_in_sim <= "0111100000000000";
  wait for 10 ns;
    code_in_sim <= "0000000000000000";
  wait for 10 ns;
    code_in_sim <= "1110011100101010";
  wait for 10 ns;
    code_in_sim <= "0111111000011111";
  wait for 10 ns;
    code_in_sim <= "0101100000011110";
  wait for 10 ns;
    code_in_sim <= "1111110000100001";
  wait for 10 ns;
    code_in_sim <= "0110111100100111";
  wait for 10 ns;
    code_in_sim <= "0111111111011101";
  wait for 10 ns;
    code_in_sim <= "1011100111101100";
  wait for 10 ns;
    code_in_sim <= "1110111111100101";
  wait for 10 ns;
    code_in_sim <= "0110111110101000";
  wait for 10 ns;
    code_in_sim <= "1110000000111110";
  wait for 10 ns;
  end process;

end comb;
