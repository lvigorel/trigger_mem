library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity pos_enc is
port (
		clock : in std_logic;
		next_pix : in std_logic;
		pix : in std_logic_vector(16-1 downto 0);
		free : out std_logic;
		code : out std_logic_vector(4 - 1 downto 0)
	);
end pos_enc;

architecture arch of pos_enc is

	signal pix_in : std_logic_vector(16-1 downto 0);
	signal erase : std_logic_vector(16-1 downto 0);
	signal pre_erase : std_logic_vector(16-1 downto 0);
	signal pre_code : std_logic_vector(4-1 downto 0);
	signal busy : std_logic;

  component priority_enc_16
	port(
		flag : in std_logic_vector(16-1 downto 0);
		block_flag : out std_logic;
		b : out std_logic_vector(4-1 downto 0)
	);
	end component;

	component decoder
  generic(
    size : natural
  );
	port(
		enable : in std_logic;
		addr : in std_logic_vector(size-1 downto 0);
		one_hot : out std_logic_vector(2**size-1 downto 0)
	);
	end component;

begin

	code <= pre_code;
	free <= not busy;

	encoder : priority_enc_16
	port map(
		flag => pix_in,
		block_flag => busy,
		b => pre_code
	);

--we could use busy for enable..
	eraser : decoder
	generic map(
		size => 4
	)
	port map(
		enable => busy,
		addr => pre_code,
		one_hot => pre_erase
	);

	delete_control : process(clock)
	begin
		if falling_edge(clock) then
			erase <= pre_erase;
		end if;
	end process;

	input_control:
  for i in 0 to 15 generate
		input_bit : process(clock)
		begin
			if rising_edge(clock) then
				if next_pix='1' then
    			pix_in(i) <= pix(i);
				elsif erase(i)='1' then
    			pix_in(i) <= '0';
				end if;
			end if;
		end process;
	end generate;

end arch;
