All sources are in 'src' folder.

The sources are arranged in this way:

tb_memory_module_forsyn.sv : testbench file for the synthesised circuit
tb_memory_module.sv : testbench file for the pre-synthesis code
More in general 'tb_...' is the testbench of '...'

memory_module.sv : top level
  readout.sv : merging, compressing and writing. The core is the 'readout' module, that handles the merging of data from different columns and the relative serialisation and compression. It also include priority encoders, a decoder, a hamming encoder, an address controller for writing in the memory (write control) and flip flops of different kind. All these modules are used in the 'readout' module.

  memory.vhd : structure of the memory. NON SYNTHESISABLE. Composed of decoders (for the address) and two memory cell arrays, one for the header, the other for the data. The data memory can be enabled by an external signal.
    -memory_cell_array.vhd
    -decoders


  readout_corr.vhd : reads data from the memory. Sends data outside according to the signals from the trigger controller. Capable of working in a token ring scheme.

  trigger_control.vhd : acquires the trigger signal and associate each trigger to the corresponding BCID. It controls the bunch crossing identifier in the full circuit, both for writing and reading (the one for readout is evaluated with a difference). The signals are both shifted relatively to the bunch crossing clock, in order to allow for signal propagation.
