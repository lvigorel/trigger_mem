library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;


entity memory_cell_array_tb is
--  Port ( );
end memory_cell_array_tb;

architecture behavioral of memory_cell_array_tb is

	COMPONENT memory_cell_array
	generic(
		height: natural;
		width: natural
		);
	port (
		r : in std_logic_vector(height-1 downto 0);
		w : in std_logic_vector(height-1 downto 0);

		w_d : in std_logic_vector(width-1 downto 0);
		r_d : out std_logic_vector(width-1 downto 0)
		);
	END COMPONENT;


	--Inputs
	signal r_sym : std_logic_vector(4-1 downto 0);
	signal w_sym : std_logic_vector(4-1 downto 0);

	signal w_d_sym : std_logic_vector(4-1 downto 0);

	--Outputs
	signal r_d_sym : std_logic_vector(4-1 downto 0);

	-- Clock period definitions
-- 	constant clk_100_period : time := 10 ns;

begin
	-- Instantiate the Unit Under Test (UUT)

	uut : memory_cell_array
	generic map(
		height => 4,
		width => 4
	)
	port map (
		r => r_sym,
		w => w_sym,

		w_d => w_d_sym,
		r_d => r_d_sym
	);

   -- Stimulus process
	stim_proc: process
	begin
			w_sym <= "0100";
			w_d_sym <= "0110";
		wait for 10 ns;
			w_sym <= "1000";
			w_d_sym <= "0010";
		wait for 10 ns;
			r_sym <= "0100";
		wait for 10 ns;
			r_sym <= "1000";
		wait for 10 ns;
			w_sym <= "0100";
			w_d_sym <= "0111";
		wait for 10 ns;
			w_sym <= "0100";
			w_d_sym <= "1100";
		wait for 10 ns;
			r_sym <= "0100";
		wait for 10 ns;
			r_sym <= "0100";

		wait for 10 ns;
	end process;

end behavioral;
