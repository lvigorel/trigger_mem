module write_mem#(
    parameter data_w = 4,
    parameter bc_w = 10,
    parameter gr_w = 5)
    (
     input bc_tg, //BCID trigger signal
     input clk,
     input wren, // Wren signal to store into the Long Tern Memory
     input [bc_w-1:0]bcid, //BCID
     input [18:0]data_in, // DAta from the Sync MEm readout
     output logic [12:0]addr,
     output logic [1:0]code,
     output logic [18:0]data_out);

     logic [2:0]state,newstate;

     logic [19:0]pre_data_out;
     logic [19:0]data_out_direct;
     logic [19:0]counter;
     logic [1:0]pre_code;
     logic bypass;

     ff #(.width(2)) inst_state(.clk('clk),.rstb(rstb),.data_in(newstate),.data_out(state));

     always_comb
     begin
          case(state)
          //begin
          0:  begin
                  if(bc_tg & wren)
                      newstate <= 3'h6;
                      pre_code <= 2'b11;
                  else
                  if(wren)
                      newstate <= 3'h1;
                  else
                  if(bc_tg)
                      newstate <= 3'h5;
                      counter <= '0;
                  else
                      newstate <= 3'h0;
              end

          //data
          1:  begin
                  pre_code <= 2'b00;
                  if(wren)
                      newstate <= 3'h2;
                  else
                      newstate <= 3'h3;
              end

          //data write
          2:  begin
                  data_out <= data_in;
                  if(wren)
                      newstate <= 3'h1;
                      pre_code <= 2'b00;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
                  else
                      newstate <= 3'h2;
              end

          //last data write
          3:  begin
                  data_out <= data_in;
                  if(wren)
                      newstate <= 3'h1;
                  else
                      newstate <= 3'h4;
              end

          //suspend
          4:  begin
                  if(wren & bc_tg)
                      newstate <= 3'h1;
                      pre_code <= 2'b10;
                  if(wren)
                      newstate <= 3'h1;
                      pre_code <= 2'b00;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
              end

          //empty
          5:  begin
                  if(wren)
                      newstate <= 3'h6;
                      pre_code <= 2'b11;
                  else
                  if(bc_tg)
                      counter <= counter + 1;
                  else
                      newstate <= 3'h5;
              end

          // emptywrite
          6:  begin
                  if(wren)
                      newstate <= 3'h2;
                      data_out <= counter;
                      pre_code <= 2'b10;
                  else
                      newstate <= 3'h3;
                      data_out <= counter;
                      pre_code <= 2'b10;
              end


          default:newstate <= 2'h0;
          endcase
     end

endmodule
