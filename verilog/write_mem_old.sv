module write_mem(
     input bc_tg, //BCID trigger signal
     input clk,
     input wren, // Wren signal to store into the Long Tern Memory
     input [10:0]bcid, //BCID address pointer
     input [18:0]data_in, // DAta from the Sync MEm readout
     output logic [12:0]addr,
     output logic [1:0]code,
     output logic [18:0]data_out);

     logic [19:0]pre_data_out;
     logic [19:0]data_out_direct;
     logic [19:0]counter;
     logic [1:0]pre_code;
     logic bypass;

     ff #(.width(2)) inst_state(.clk('clk),.rstb(0),.data_in(newstate),.data_out(state));
     ff_bypass #(.width(2)) inst_state(.clk('clk),.bypass(bypass),.data_in(pre_code),.data_in_direct(code_direct),.data_out(code));
     ff_bypass #(.width(19)) inst_state(.clk('clk),.bypass(bypass),.data_in(pre_data_out),.data_in_direct(data_out_direct),.data_out(data_out));


     always_comb
     begin
          case(state)
          //begin
          0:  begin
                  if(bc_tg & wren)
                      bypass <= 1'b1;
                      code_direct <= 2'b11;
                      data_out_direct <= '0;
                      pre_code <= 2'b10;
                      pre_data_out <= data_in;
                      newstate <= 2'h1;
                  else
                  if(wren)
                      newstate <= 2'h1;
                      pre_code <= 2'b10;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 2'h3;
                      //pre_code <= 2'b11;
                      counter <= '0;
                  else
                      newstate <= 2'h0;
              end
          //write
          1:  begin
                  //already in ff!! data_out <= pre_data_out;
                  bypass <= 1'b0;
                  if(wren)
                      newstate <= 2'h1;
                      pre_code <= 2'b00;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 2'h0;
                  else
                      newstate <= 2'h2;
              end
          //suspend
          2:  begin
                  if(wren)
                      newstate <= 2'h1;
                      code <= 2'b00;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 2'h0;

              end
          //empty
          3:  begin
                  if(bc_tg & wren)
                      code_direct <= 2'b11;
                      data_direct <= counter;
                      pre_code <= 2'b10;
                  else
                  if(bc_tg)
                      counter <= counter + 1;
                  else
                  if(wren)
                      code_direct <= 2'b11;
                      data_out_direct <= counter;
                      pre_code <= 2'b10;
                      pre_data_out <= data_in;
                      newstate <= 2'h1;
                  else
                      newstate <= 2'h3; //current state
              end

          default:newstate <= 2'h0;
          endcase
     end

endmodule

module ff_bypass #(parameter width=2)(
     input clk,
     input bypass,
     input [width -1:0]data_in,
     input [width -1:0]data_in_direct,
     output logic [width -1:0]data_out);
     always_ff @ (clk, bypass)
     begin
          if(clk)
              data_out<=data_in;
          else
          if (bypass)
              data_out<=data_in_direct;
     end

endmodule
