module write_mem(
     input bc_tg, //BCID trigger signal
     input clk,
     input wren, // Wren signal to store into the Long Tern Memory
     input [10:0]bcid, //BCID address pointer
     input [18:0]data_in, // DAta from the Sync MEm readout
     output logic [12:0]addr,
     output logic [1:0]code,
     output logic [18:0]data_out);

     logic [2:0]state,newstate;

     logic [19:0]pre_data_out;
     logic [19:0]data_out_direct;
     logic [19:0]counter;
     logic [1:0]pre_code;
     logic bypass;

     ff #(.width(2)) inst_state(.clk('clk),.rstb(rstb),.data_in(newstate),.data_out(state));

     always_comb
     begin
          case(state)
          //begin
          0:  begin
                  if(bc_tg & wren)
                      newstate <= 3'h11;
                      counter <= '0;
                  else
                  if(wren)
                      newstate <= 3'h1;
                  else
                  if(bc_tg)
                      newstate <= 3'h10;
                      counter <= '0;
                  else
                      newstate <= 3'h0;
              end

          //first data
          1:  begin
                  if(wren & bc_tg)

                  if(wren)
                      newstate <= 3'h4;
                  else
                      newstate <= 3'h5;
              end

          //data
          2:  begin
                  if(wren)
                      newstate <= 3'h4;
                  else
                      newstate <= 3'h6;
              end

          //first data write
          3:  begin
                  data_out <= data_in;
                  code <= 2'b10;
                  if(wren & bc_tg)
                      newstate <= 3'h1;
                  else
                  if(wren)
                      newstate <= 3'h2;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
                  else
                      newstate <= 3'h7;
              end

          //data write
          4:  begin
                  data_out <= data_in;
                  code <= 2'b00;
                  if(wren)
                      newstate <= 3'h2;
                      pre_code <= 2'b00;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
                  else
                      newstate <= 3'h5;
              end

          //last data write
          5:  begin
                  data_out <= data_in;
                  code <= 2'b00;
                  if(wren)
                      newstate <= 3'h2;
                  else
                      newstate <= 3'h5;
              end

          //last data write before new
          6:  begin
                  data_out <= data_in;
                  code <= 2'b00;
                  newstate <= 4'h3;
              end

          //first single data write
          7:  begin
                  data_out <= data_in;
                  code <= 2'b10;
                  if(wren)
                      newstate <= 3'h2;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
                  else
                      newstate <= 3'h3;
              end

          //single data write
          8:  begin
                  data_out <= data_in;
                  code <= 2'b00;
                  if(wren)
                      newstate <= 3'h2;
                      pre_data_out <= data_in;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
                  else
                      newstate <= 3'h3;
              end

          //suspend
          9:  begin
                  if(wren & bc_tg)
                      newstate <= 3'h9;
                  else
                  if(wren)
                      newstate <= 3'h4;
                  else
                  if(bc_tg)
                      newstate <= 3'h0;
              end

          //empty
          10:  begin
                  if(wren)
                      newstate <= 3'h7;
                  else
                  if(bc_tg)
                      counter <= counter + 1;
                  else
                      newstate <= 3'h6;
              end

          // emptywrite
          11:  begin
                  data_out <= counter;
                  code <= 2'b11;
                  if(wren)
                      newstate <= 3'h3;
                  else
                      newstate <= 3'h6;
              end


          default:newstate <= 3'h0;
          endcase
     end

endmodule
