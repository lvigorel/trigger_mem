library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;


entity priority_enc_16 is
port (
		flag : in std_logic_vector(16-1 downto 0);
		block_flag : out std_logic;
		b : out std_logic_vector(4 - 1 downto 0)
	);
end priority_enc_16;

-- architecture tree of priority_enc_16 is
--
--   signal flag_1 : std_logic_vector(4-1 downto 0);
-- 	signal flag_2 : std_logic;
-- 	signal b_1 : std_logic_vector(8-1 downto 0);
-- 	signal b_2 : std_logic_vector(2-1 downto 0);
--
--   component priority_enc_4
-- 	port(
-- 		flag : in std_logic_vector(4-1 downto 0);
-- 		block_flag : out std_logic;
-- 		b : out std_logic_vector(2-1 downto 0)
-- 	);
-- 	end component;
--
--
-- begin
--
--   comp_gen:
--   for i in 0 to 3 generate
--
--     enc : priority_enc_4
--     port map (
--       flag => flag(4*i+3 downto 4*i),
--       block_flag => flag_1(i),
--       b => b_1(i*2+1 downto i*2)
--     );
--
--   end generate;
--
--   enc_f : priority_enc_4
--   port map (
--     flag => flag_1,
--     block_flag => flag_2,
--     b => b_2
--   );
--
-- 	block_flag <= flag_2;
--
--   b(3 downto 2) <= b_2;
-- 	b(1 downto 0) <= b_1(7 downto 6) when (b_2 = "11") else
-- 							 b_1(5 downto 4) when (b_2 = "10") else
-- 							 b_1(3 downto 2) when (b_2 = "01") else
-- 							 b_1(1 downto 0);
--
-- end tree;



architecture arch of priority_enc_16 is

begin

	dec : process(flag)
	begin
		if flag(15)='1' then
			b <= "1111";
		elsif flag(14)='1' then
			b <= "1110";
		elsif flag(13)='1' then
			b <= "1101";
		elsif flag(12)='1' then
			b <= "1100";
		elsif flag(11)='1' then
			b <= "1011";
		elsif flag(10)='1' then
			b <= "1010";
		elsif flag(9)='1' then
			b <= "1001";
		elsif flag(8)='1' then
			b <= "1000";
		elsif flag(7)='1' then
			b <= "0111";
		elsif flag(6)='1' then
			b <= "0110";
		elsif flag(5)='1' then
			b <= "0101";
		elsif flag(4)='1' then
			b <= "0100";
		elsif flag(3)='1' then
			b <= "0011";
		elsif flag(2)='1' then
			b <= "0010";
		elsif flag(1)='1' then
			b <= "0001";
		elsif flag(0)='1' then
			b <= "0000";
		end if;
	end process;


	--
  -- b <=	"1111" when (flag(15) = '1') else
	-- 			"1110" when (flag(14) = '1') else
  --       "1101" when (flag(13) = '1') else
  --       "1100" when (flag(12) = '1') else
  --       "1011" when (flag(11) = '1') else
  --       "1010" when (flag(10) = '1') else
  --       "1001" when (flag(9) = '1') else
  --       "1000" when (flag(8) = '1') else
	-- 			"0111" when (flag(7) = '1') else
  --       "0110" when (flag(6) = '1') else
  --       "0101" when (flag(5) = '1') else
  --       "0100" when (flag(4) = '1') else
  --       "0011" when (flag(3) = '1') else
  --       "0010" when (flag(2) = '1') else
  --       "0001" when (flag(1) = '1') else
	--   		"0000" when (flag(0) = '1');

	block_flag <= '0' when flag="0000000000000000" else
								'1';

end arch;
