library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- use IEEE.STD_LOGIC_ARITH.ALL;
-- use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity hd_dec is
  generic(
    width : natural := 21;
    code_width : natural := 2
    );
  port (
    clock : in std_logic;
    setup : in std_logic;

    code_in : in std_logic_vector(width - 1 downto 0);
    r_move : in std_logic;
    corrected_out : out std_logic_vector(width - 1 downto 0);
    err_out : out std_logic
    );
  end hd_dec;


architecture arch of hd_dec is

  signal p : std_logic_vector(5 downto 0);
  signal s : std_logic_vector(4 downto 0);
  signal corr : std_logic_vector(width-1 downto 0);

  signal code : std_logic_vector(width-1 downto 0);
  signal corrected : std_logic_vector(width-1 downto 0);

  signal corrected_out_reg : std_logic_vector(width-1 downto 0);
  signal corrected_out_next : std_logic_vector(width-1 downto 0);
  signal pre_corrected_out : std_logic_vector(width-1 downto 0);

  signal err_out_reg : std_logic;
  signal err_out_next : std_logic;
  signal err : std_logic;

--architecture for 19 (18 downto 0) bits encoding. Can work with longer words,
--just ignoring the added bits
begin
  code <= code_in;
  intersection : process(code_in, corrected)
  begin
    if(code_in(width-1 downto width-2) = "11")then
      pre_corrected_out <= corrected;
    else
      pre_corrected_out <= code_in;
    end if;
  end process;
  -- FF with enable --
  out_d_ff : process(clock, setup)
  begin
    if(setup = '0')then
      corrected_out_reg <= (others => '0');
      err_out_reg <= '0';
    elsif(clock'event and clock ='1') then
      corrected_out_reg <= corrected_out_next;
      err_out_reg <= err_out_next;
    end if;
  end process;
  -- state machine for FF output control --
  corrected_out_next <= pre_corrected_out when r_move = '1' else
                        corrected_out_reg;
  err_out_next <= err when r_move = '1' else
                  err_out_reg;
  corrected_out <= corrected_out_reg;
  err_out <= err_out_reg;
  -----------------------------------------

  p_0 : p(0) <= code(2) xor code(4) xor code(6) xor code(8) xor code(10)
      xor code(12) xor code(14) xor code(16) xor code(18);
  p_1 : p(1) <= code(2) xor code(5) xor code(6) xor code(9) xor code(10)
      xor code(13) xor code(14) xor code(17) xor code(18);
  p_2 : p(2) <= code(4) xor code(5) xor code(6) xor code(11) xor code(12)
      xor code(13) xor code(14);
  p_3 : p(3) <= code(8) xor code(9) xor code(10) xor code(11) xor code(12)
      xor code(13) xor code(14);
  p_4 : p(4) <= code(16) xor code(17) xor code(18);
  p_5 : p(5) <= code(0) xor code(1) xor code(2) xor code(3) xor code(4)
      xor code(5) xor code(6) xor code(7) xor code(8) xor code(9) xor code(10)
      xor code(11) xor code(12) xor code(13) xor code(14) xor code(15)
      xor code(16) xor code(17) xor code(18);

  s_0 : s(0) <= p(0) xor code(0);
  s_1 : s(1) <= p(1) xor code(1);
  s_2 : s(2) <= p(2) xor code(3);
  s_3 : s(3) <= p(3) xor code(7);
  s_4 : s(4) <= p(4) xor code(15);


  decoder_gen:
  for i in 0 to width-code_width-1 generate
    corr(i) <=  '1' when to_unsigned(i,5) = unsigned(s) else
                '0';
  end generate;

  pre_corr : corrected(18 downto 0) <= code(18 downto 0) xor corr(19 downto 1);

  err_l : err <= (not corr(0)) and (not(code(19) xor p(5)));
  corr_l : corrected(19) <= p(5) xor (not corr(0));
end arch;
