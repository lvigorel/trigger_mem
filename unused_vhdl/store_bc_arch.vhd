
architecture store_bc of trig_memory is

  CONSTANT const_addr_size : natural := natural(ceil(log2(real(height))));
  
  component memory
  generic(
    addr_size : natural;
    height : natural;
    width : natural
  );
	port (
    clock : in std_logic;
    setup : in std_logic;

		w_addr : in std_logic_vector(const_addr_size-1 downto 0);
		r_addr : in std_logic_vector(const_addr_size-1 downto 0);

		w_d : in std_logic_vector(width-1 downto 0);
		r_d : out std_logic_vector(width-1 downto 0)
	);
  end component;

  type state_type is (to_next, read_full, wait40, wait_count, setting);

  signal state, next_state : state_type;

  signal data_empty : std_logic_vector(width-3 downto 0);

  ---------------------------------------------------------------
  signal code : std_logic_vector(1 downto 0);
  signal trig : std_logic;

  signal bcid : std_logic_vector(bc_width-1 downto 0);

  --signal count_comp_reg : unsigned(width-3 downto 0);
  --signal count_comp_next : unsigned(width-3 downto 0);
  signal bc_counter_reg : unsigned(bc_width-1 downto 0);
  signal bc_counter_next : unsigned(bc_width-1 downto 0);
  signal r_d_data : std_logic_vector(width-3 downto 0);

  signal comp_reset : std_logic;

  ---------------------------------------------------------------

  signal w_addr : std_logic_vector(const_addr_size-1 downto 0);
  signal r_addr : std_logic_vector(const_addr_size-1 downto 0);

  signal r_d : std_logic_vector(width-1 downto 0);

  signal trig_reset : std_logic;

  signal trig_flag : std_logic;
  signal trig_id : std_logic_vector(bc_width-1 downto 0);

  signal late_pos : std_logic_vector(bc_width-1 downto 0);
  signal late_pos_reg : unsigned(bc_width-1 downto 0);
  signal late_pos_next : unsigned(bc_width-1 downto 0);

  signal trig_data_flag : std_logic;

  signal r_reg : unsigned(const_addr_size-1 downto 0);
  signal r_next : unsigned(const_addr_size-1 downto 0);

  signal w_reg : unsigned(const_addr_size-1 downto 0);
  signal w_next : unsigned(const_addr_size-1 downto 0);

  signal w_d : std_logic_vector(width-1 downto 0);

begin
----------------------
--------MEMORY--------
----------------------
  memory_inst : memory
  generic map(
    addr_size => const_addr_size,
    height => height,
    width => width
  )
  port map(
    clock => clock,
    setup => setup,

    w_addr => w_addr,
    r_addr => r_addr,

    w_d => w_d,
    r_d => r_d
  );

  --------------------
  --Trigger latching--
  --------------------

  trig_flag_latch : process(trig_reset, in_trig_flag)
  begin
    if (trig_reset = '1') then
      trig_flag <= '0';
    elsif (in_trig_flag='1') then
      trig_flag <= '1';
    end if;
  end process;


  trig_id_latch : process (clock, setup, in_trig_flag, in_trig_id)
  begin
    if (setup = '1') then
      trig_id <= (others => '0');
    elsif (clock'event and clock = '1' and in_trig_flag = '1') then
      trig_id <= in_trig_id;
    end if;
  end process;

  bc_ff : process (clock, setup)
  begin
    if (setup = '1') then
      bcid <= (others => '0');
    elsif (clock'event and clock = '1') then
      bcid <= in_bcid;
    end if;
  end process;

  -------------------
  --Trigger control--
  -------------------
  code <= r_d(width - 1 downto width - 2);
  r_d_data <= r_d(width - 3 downto 0);

  process(state, code, r_d_data, trig, r_reg, bc_counter_reg, late_pos_reg, trig_id, data_empty)
  begin
    --default values..

    case state is
      -- setting
      when setting =>
        if(setup = '1') then
          bc_counter_next <= bc_counter_reg;
          r_next <= r_reg;
          next_state <= setting;
        elsif(setup = '0') then
          next_state <= wait_count;
          r_next <= r_reg + 1;
        end if;

      -- move to next
      when to_next =>
        if(code = "00") then
          r_next <= r_reg + 1;
          next_state <= to_next;
        elsif(code = "10") then
          next_state <= wait40;
          r_next <= r_reg;
        elsif(code = "11") then
          next_state <= wait_count;
          r_next <= r_reg;
          bc_counter_next <= unsigned(r_d_data(bc_width -1 downto 0));
        end if;

      -- read full
      when read_full =>
        if(code = "00") then
          next_state <= read_full;
          r_next <= r_reg + 1;
          out_d <= trig_id & r_d_data;
        elsif(code = "10") then
          next_state <= wait40;
          r_next <= r_reg;
          bc_counter_next <= bc_counter_reg + 1;
        elsif(code = "11") then
          next_state <= wait_count;
          r_next <= r_reg;
          bc_counter_next <= unsigned(r_d_data(bc_width -1 downto 0));
        end if;

      -- wait for 40
      when wait40 =>
        if(trig = '1') then
          comp_reset <= '1';
          next_state <= read_full;
          r_next <= r_reg + 1;
          out_d <= trig_id & r_d_data;
        elsif(2**(bc_width - 1) > late_pos_reg - bc_counter_reg and 0 < late_pos_reg - bc_counter_reg ) then
          next_state <= to_next;
          r_next <= r_reg + 1;
          bc_counter_next <= bc_counter_reg + 1;
        end if;

      -- wait for count
      when wait_count =>
        if(trig = '1') then
--          if(trig_id = bc_counter_reg) then
            comp_reset <= '1';
            next_state <= wait_count;
            out_d <= trig_id & data_empty;
  --        elsif() then

    --      end if;
        elsif(2**(bc_width - 1) > late_pos_reg - bc_counter_reg and 0 < late_pos_reg - bc_counter_reg) then
          next_state <= wait40;
          r_next <= r_reg + 1;
          bc_counter_next <= bc_counter_reg + 1;
        else
          next_state <= wait_count;
        end if;

    end case;
  end process;

  data_empty <= (others => '0');
  --bc_counter_next <= bc_counter_reg + 1;

  --------------------------------------
  ---------- NEXT STATE LOGIC ----------
  --------------------------------------

  next_state_ff : process(clock, setup)
  begin
    if (setup = '1') then
      r_reg <= (others => '0');
      state <= setting;
      --count_comp_reg <= (others => '0');
      bc_counter_reg <= (others => '0');
    elsif (clock'event and clock = '1') then
      r_reg <= r_next;
      state <= next_state;
      --count_comp_reg <= count_comp_next;
      bc_counter_reg <= bc_counter_next;
    end if;
  end process;

  r_addr <= std_logic_vector(r_reg);

-- do them with external bc counter from outside (as it should already exist)
  bc_comparator : process(clock, comp_reset)
  begin
    if (comp_reset = '1') then
      trig <= '0';
    elsif (clock'event and clock = '1') then
      if (bc_counter_reg = unsigned(trig_id)) then
        trig <= '1';
      else
        trig <= '0';
      end if;
    end if;
  end process;

  -- late_pos_control : process(setup, bc_clock)
  -- begin
  --   if (setup = '1') then
  --     late_pos_reg <= (others => '0');
  --   elsif (bc_clock'event and bc_clock = '1') then
  --     late_pos_reg <= late_pos_next;
  --   end if;
  -- end process;

--  late_pos_next <= (late_pos_reg + 1) when (trig_flag = '0') else unsigned(trig_id);

  late_pos_reg <= unsigned(bcid) - latency;
  late_pos <= std_logic_vector(late_pos_reg);

  --------------------------------------
  --------------------------------------

  -------------------
  -- write control --
  -------------------
  write_control : process(clock, setup, w_next, in_d_flag)
  begin
    if (setup = '1') then
      w_reg <= (others => '0');
    elsif (clock'event and clock = '1' and in_d_flag = '1') then
      -- we can't write faster than internal write speed
      w_reg <= w_next;
    end if;
  end process;

  w_counter_control : process(w_reg)
  begin
    if (w_reg = height - 1) then
      w_next <= (others => '0');
    else
      w_next <= w_reg + 1;
    end if;
  end process;

  w_addr <= std_logic_vector(w_reg);

  w_d <= in_d;

end store_bc;
