library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

  entity readout_fsm is
    generic(
      addr_size : natural;
      height : natural;
      width : natural;
      bc_width : natural;
      code_width : natural := 2
    );
    port(
      clock : in std_logic;
      setup : in std_logic;

      code : in std_logic_vector(code_width-1 downto 0);
      r_d_data : in std_logic_vector(width-code_width-1 downto 0);
      trig : in std_logic;
      r_reg : out unsigned(addr_size-1 downto 0);
      bc_counter_reg_out : out unsigned(bc_width-1 downto 0);
      late_pos_reg : in unsigned(bc_width-1 downto 0)
    );
  end readout_fsm;

architecture arch of readout_fsm is

  type STATE_TYPE is (setting, to_next, read_full, wait40, wait_count);

  constant NEW_BC : std_logic_vector(1 downto 0) := "10";
  constant SAME_BC : std_logic_vector(1 downto 0) := "00";
  -- constant COUNTER : std_logic_vector(1 downto 0) := "11";

  constant DATA_EMPTY :
        std_logic_vector(width-code_width-1 downto 0) := (others => '0');

  constant HALF_BC :
  unsigned(bc_width-1 downto 0) := to_unsigned(2**(bc_width-1), bc_width);
  constant ZERO_BC : unsigned(bc_width-1 downto 0) := (others => '0');

  signal state, next_state : STATE_TYPE;

  ---------------------------------------------------------------
--  signal code : std_logic_vector(code_width-1 downto 0);
  -- signal trig : std_logic;

  signal bcid : std_logic_vector(bc_width-1 downto 0);

  --signal count_comp_reg : unsigned(width-3 downto 0);
  --signal count_comp_next : unsigned(width-3 downto 0);
  -- signal bc_counter_reg : unsigned(bc_width-1 downto 0);
  signal bc_counter_next : unsigned(bc_width-1 downto 0);
  -- signal r_d_data : std_logic_vector(width-code_width-1 downto 0);

  -- signal comp_reset : std_logic;

  signal out_d : std_logic_vector(width+bc_width-code_width-1 downto 0);
  signal out_flag : std_logic;

  ---------------------------------------------------------------

  -- signal trig_flag : std_logic;
  signal trig_id : std_logic_vector(bc_width-1 downto 0);

  -- signal trig_reset : std_logic;

  -- signal late_pos_reg : unsigned(bc_width-1 downto 0);

  -- signal trig_data_flag : std_logic;

  -- signal r_reg : unsigned(addr_size-1 downto 0);
  signal r_next : unsigned(addr_size-1 downto 0);



begin

  state_machine : process(
    state, code, r_d_data, trig,
    r_reg, bc_counter_reg, late_pos_reg, trig_id)
  begin
    --default values..
    out_d <= (others => '0');
    out_flag <= '0';
    bc_counter_next <= bc_counter_reg;
    r_next <= r_reg;
    next_state <= state;

    case state is
      -- setting
      when setting =>
        -- if(setup = '0') then
          -- bc_counter_next <= bc_counter_reg;
          --next_state <= setting;
        -- elsif(setup = '1') then
        next_state <= wait_count;
        if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
          r_next <= (others => '0');
        else
          r_next <= r_reg + to_unsigned(1,addr_size);
        end if;

      -- move to next
      when to_next =>
        if(code = SAME_BC) then
          if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          --next_state <= to_next;
        elsif(code = NEW_BC) then
          next_state <= wait40;
          -- r_next <= r_reg;
        else --if(code = COUNTER) then
          next_state <= wait_count;
          -- r_next <= r_reg;
          bc_counter_next <= unsigned(r_d_data(bc_width -1 downto 0));
        end if;

      -- read full
      when read_full =>
        if(code = SAME_BC) then
          --next_state <= read_full;
          if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          out_d <= trig_id & r_d_data;
          out_flag <= '1';
        elsif(code = NEW_BC) then
          next_state <= wait40;
          -- r_next <= r_reg;
          bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
        else --if(code = COUNTER) then
          next_state <= wait_count;
          -- r_next <= r_reg;
          bc_counter_next <= unsigned(r_d_data(bc_width -1 downto 0));
        end if;

      -- wait for 40
      when wait40 =>
        if(trig = '1') then
          -- comp_reset <= '1';
          next_state <= read_full;
          if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          out_d <= trig_id & r_d_data;
        elsif(HALF_BC > late_pos_reg - bc_counter_reg
          and ZERO_BC < late_pos_reg - bc_counter_reg)
          then
          next_state <= to_next;
          if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
        -- else
          --next_state <= wait40;
          -- r_next <= r_reg;
        end if;

      -- wait for count
      when wait_count =>
        if(trig = '1') then
          --next_state <= wait_count;
          out_d <= trig_id & DATA_EMPTY;
          out_flag <= '1';
          -- r_next <= r_reg;

        elsif(HALF_BC>late_pos_reg-bc_counter_reg
            and ZERO_BC<late_pos_reg-bc_counter_reg) then

          next_state <= wait40;
          if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          else
            r_next <= r_reg + to_unsigned(1,addr_size);
          end if;
          bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
        --else
          --next_state <= wait_count;
          -- r_next <= r_reg;
        end if;

    end case;
  end process;


  --------------------------------------
  ---------- NEXT STATE LOGIC ----------
  --------------------------------------

  next_state_ff : process(clock, setup)
  begin
    if (setup = '0') then
      -- r_reg <= (others => '0');
      state <= setting;
      -- bc_counter_reg <= (others => '0');
    elsif (clock'event and clock = '1') then
      -- r_reg <= r_next;
      state <= next_state;
      -- bc_counter_reg <= bc_counter_next;
    end if;
  end process;

  next_num_ff : process(clock, setup)
  begin
    if (setup = '0') then
      r_reg <= (others => '0');
      bc_counter_reg <= (others => '0');
    elsif (clock'event and clock = '1') then
      r_reg <= r_next;
      bc_counter_reg <= bc_counter_next;
    end if;
  end process;

end arch;
