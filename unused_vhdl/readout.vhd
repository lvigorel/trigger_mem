library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity readout is
  generic(
    addr_size : natural := 10;
    height : natural := 850;
    width : natural := 21;
    bc_width : natural := 11;
    code_width : natural := 2
  );
  port(
    clock : in std_logic;
    setup : in std_logic;

    in_bcid : in std_logic_vector(bc_width-1 downto 0);

    r_addr : out std_logic_vector(addr_size-1 downto 0);
    r_d : in std_logic_vector(width-1 downto 0);

    r_move : out std_logic;

    out_ff : out std_logic_vector(width+bc_width-code_width-1 downto 0);
    out_flag_ff : out std_logic;

    in_trig_flag : in std_logic;
    in_trig_id : in std_logic_vector(bc_width-1 downto 0)
  );
end readout;

architecture arch of readout is

  type STATE_TYPE is (setting, to_next, read_full, wait40, wait_count);

  CONSTANT LATENCY : natural := 4;

  constant NEW_BC : std_logic_vector(1 downto 0) := "10";
  constant SAME_BC : std_logic_vector(1 downto 0) := "00";
  -- constant COUNTER : std_logic_vector(1 downto 0) := "11";

  constant DATA_EMPTY :
        std_logic_vector(width-code_width-1 downto 0) := (others => '0');

  constant HALF_BC :
  unsigned(bc_width-1 downto 0) := to_unsigned(2**(bc_width-1), bc_width);
  constant ZERO_BC : unsigned(bc_width-1 downto 0) := (others => '0');

  signal state, next_state : STATE_TYPE;

  ---------------------------------------------------------------
  signal code : std_logic_vector(code_width-1 downto 0);
  signal trig : std_logic;

  signal bcid : std_logic_vector(bc_width-1 downto 0);

  --signal count_comp_reg : unsigned(width-3 downto 0);
  --signal count_comp_next : unsigned(width-3 downto 0);
  signal bc_counter_reg : unsigned(bc_width-1 downto 0);
  signal bc_counter_next : unsigned(bc_width-1 downto 0);
  signal r_d_data : std_logic_vector(width-code_width-1 downto 0);

  -- signal comp_reset : std_logic;

  signal out_d : std_logic_vector(width+bc_width-code_width-1 downto 0);
  signal out_flag : std_logic;

  ---------------------------------------------------------------

  signal trig_flag : std_logic;
  signal trig_id : std_logic_vector(bc_width-1 downto 0);

  signal trig_reset : std_logic;

  signal late_pos_reg : unsigned(bc_width-1 downto 0);

  -- signal trig_data_flag : std_logic;

  signal r_reg : unsigned(addr_size-1 downto 0);
  signal r_next : unsigned(addr_size-1 downto 0);

  signal r_move_next : std_logic;
  signal r_move_reg : std_logic;

  -- signal bc_corr : std_logic_vector(20-1 downto 0);
  -- signal err : std_logic;

  begin
    --------------------
    --Trigger latching--
    --------------------

    trig_flag_latch : process(trig_reset, in_trig_flag)
    begin
      if (trig_reset = '0') then
        trig_flag <= '1';
      elsif (in_trig_flag = '0') then
        trig_flag <= '0';
      end if;
    end process;

    -- trig_id_latch : process (clock, setup, in_trig_flag, in_trig_id)
    -- begin
    --   if (setup = '0') then
    --     trig_id <= (others => '0');
    --   elsif (clock'event and clock = '1' and in_trig_flag = '1') then
    --     trig_id <= in_trig_id;
    --   end if;
    -- end process;

    trig_id_ff : process(clock, setup, in_trig_flag, in_trig_id)
    begin
      if (setup = '0') then
        trig_id <= (others => '0');
      elsif (clock'event and clock = '1' and in_trig_flag = '0') then
        trig_id <= in_trig_id;
      end if;
    end process;

    bc_ff : process (clock, setup)
    begin
      if (setup = '0') then
        bcid <= (others => '0');
      elsif (clock'event and clock = '1') then
        bcid <= in_bcid;
      end if;
    end process;

    -------------------
    --Trigger control--
    -------------------
    code <= r_d(width - 1 downto width - code_width);
    r_d_data <= r_d(width - code_width -1 downto 0);

    state_machine : process(
      state, code, r_d_data, trig,
      r_reg, bc_counter_reg, late_pos_reg, trig_id)
    begin
      --default values..
      out_d <= (others => '0');
      out_flag <= '1';
      bc_counter_next <= bc_counter_reg;

      r_next <= r_reg;
      next_state <= state;
      r_move_next <= r_move_reg;

      trig_reset <= '1';
      case state is
        -- setting
        when setting =>
          -- if(setup = '0') then
            -- bc_counter_next <= bc_counter_reg;
            --next_state <= setting;
          -- elsif(setup = '1') then
          next_state <= wait_count;
          r_move_next <= '1';
          -- if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
            r_next <= (others => '0');
          -- else
          --   r_next <= r_reg + to_unsigned(1,addr_size);
          -- end if;

        -- move to next
        when to_next =>
          if(code = SAME_BC) then
            -- r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            --next_state <= to_next;
          elsif(code = NEW_BC) then
            next_state <= wait40;
            r_move_next <= '1';
            -- r_next <= r_reg;
          else --if(code = COUNTER) then
            next_state <= wait_count;
            r_move_next <= '1';
            -- r_next <= r_reg;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          end if;

        -- read full
        when read_full =>
          if(code = SAME_BC) then
            --next_state <= read_full;
            -- r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            out_d <= std_logic_vector(bc_counter_reg) & r_d_data;
            out_flag <= '0';
          elsif(code = NEW_BC) then
            next_state <= wait40;
            r_move_next <= '1';
            trig_reset <= '0';
            -- r_next <= r_reg;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          else --if(code = COUNTER) then
            next_state <= wait_count;
            r_move_next <= '1';
            -- r_next <= r_reg;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          end if;

        -- wait for 40
        when wait40 =>
          if(trig = '0') then
            -- comp_reset <= '1';
            next_state <= read_full;
            r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            out_flag <= '0';
            out_d <= std_logic_vector(bc_counter_reg) & r_d_data;
          elsif(HALF_BC > late_pos_reg - bc_counter_reg
            and ZERO_BC < late_pos_reg - bc_counter_reg)
            then
            next_state <= to_next;
            r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            bc_counter_next <= bc_counter_reg + to_unsigned(1,bc_width);
          -- else
            --next_state <= wait40;
            -- r_next <= r_reg;
          end if;

        -- wait for count
        when wait_count =>
          if(trig = '0') then
            --next_state <= wait_count;
            out_d <= trig_id & DATA_EMPTY;
            out_flag <= '0';
            -- r_next <= r_reg;
            trig_reset <= '0';

          elsif(HALF_BC>late_pos_reg-unsigned(r_d_data(bc_width -1 downto 0))
              and ZERO_BC<late_pos_reg-unsigned(r_d_data(bc_width -1 downto 0)))
              then

            next_state <= to_next;
            r_move_next <= '0';
            if (unsigned(r_reg)=to_unsigned(height,addr_size)) then
              r_next <= (others => '0');
            else
              r_next <= r_reg + to_unsigned(1,addr_size);
            end if;
            bc_counter_next <= unsigned(r_d_data(bc_width -1 downto 0))
                               + to_unsigned(1,bc_width);
          else
            bc_counter_next <= late_pos_reg;
          end if;

      end case;
    end process;


    --------------------------------------
    ---------- NEXT STATE LOGIC ----------
    --------------------------------------

    next_state_ff : process(clock, setup)
    begin
      if (setup = '0') then
        -- r_reg <= (others => '0');
        state <= setting;
        -- bc_counter_reg <= (others => '0');
      elsif (clock'event and clock = '1') then
        -- r_reg <= r_next;
        state <= next_state;
        -- bc_counter_reg <= bc_counter_next;
      end if;
    end process;

    next_num_ff : process(clock, setup)
    begin
      if (setup = '0') then
        r_reg <= (others => '0');
        bc_counter_reg <= (others => '0');
        r_move_reg <= '1';
      elsif (clock'event and clock = '1') then
        r_reg <= r_next;
        bc_counter_reg <= bc_counter_next;
        r_move_reg <= r_move_next;
      end if;
    end process;

    r_move <= r_move_reg;

    bc_comparator : process(clock, setup)
    begin
      if (setup = '0') then
        trig <= '1';
      elsif (clock'event and clock = '1') then
        if (bc_counter_reg = unsigned(trig_id) and trig_flag = '0') then
          trig <= '0';
        else
          trig <= '1';
        end if;
      end if;
    end process;

    out_d_ff : process(clock, setup)
    begin
      if(setup = '0')then
        out_ff <= (others => '0');
        out_flag_ff <= '1';
      elsif(clock'event and clock ='1') then
        out_ff <= out_d;
        out_flag_ff <= out_flag;
      end if;
    end process;


    late_diff : late_pos_reg <= unsigned(bcid) - to_unsigned(latency, bc_width);
    addr_conv : r_addr <= std_logic_vector(r_reg);

  end arch;
