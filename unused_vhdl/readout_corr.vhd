library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity readout_corr is
  generic(
    addr_size : natural := 10;
    height : natural := 850;
    width : natural := 21;
    bc_width : natural := 11;
    code_width : natural := 2
  );
  port(
    clock : in std_logic;
    setup : in std_logic;

    in_bcid : in std_logic_vector(bc_width-1 downto 0);

    r_addr : out std_logic_vector(addr_size-1 downto 0);
    r_d : in std_logic_vector(width-1 downto 0);
    out_ff : out std_logic_vector(width+bc_width-code_width-1 downto 0);
    out_flag_ff : out std_logic;

    err_out : out std_logic;

    in_trig_flag : in std_logic;
    in_trig_id : in std_logic_vector(bc_width-1 downto 0)
  );
end readout_corr;

architecture arch of readout_corr is

  component hd_dec
  generic(
    width : natural := 21;
    code_width : natural := 2
    );
  port(
    clock : in std_logic;
    setup : in std_logic;

    code_in : in std_logic_vector(width - 1 downto 0);
    r_move : in std_logic;
    corrected_out : out std_logic_vector(width - 1 downto 0);
    err_out : out std_logic
  );
  end component;

  component readout
  generic(
    addr_size : natural := 10;
    height : natural:= 850;
    width : natural := 22;
    bc_width : natural := 10;
    code_width : natural := 2
  );
  port(
    clock : in std_logic;
    setup : in std_logic;

    in_bcid : in std_logic_vector(bc_width-1 downto 0);

    r_addr : out std_logic_vector(addr_size-1 downto 0);
    r_d : in std_logic_vector(width-1 downto 0);

    r_move : out std_logic;

    out_ff : out std_logic_vector(width+bc_width-code_width-1 downto 0);
    out_flag_ff : out std_logic;

    in_trig_flag : in std_logic;
    in_trig_id : in std_logic_vector(bc_width-1 downto 0)
  );
  end component;


  signal corrected_d : std_logic_vector(width-1 downto 0);
  signal r_move : std_logic;

  begin

    hamm_dec : hd_dec
    generic map(
      width => width,
      code_width => code_width
    )
    port map(
      clock => clock,
      setup => setup,

      code_in => r_d,
      r_move => r_move,
      corrected_out => corrected_d,

      err_out => err_out
    );

    readout_inst : readout
    generic map(
      addr_size => addr_size,
      height => height,
      width => width,
      bc_width => bc_width,
      code_width => code_width
    )
    port map(
      clock => clock,
      setup => setup,

      in_bcid => in_bcid,

      r_addr => r_addr,
      r_d => corrected_d,

      r_move => r_move,

      out_ff => out_ff,
      out_flag_ff => out_flag_ff,

      in_trig_flag => in_trig_flag,
      in_trig_id => in_trig_id
    );

  end arch;
