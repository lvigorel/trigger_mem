library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

USE IEEE.MATH_REAL.all;
--USE IEEE.MATH_REAL.CEIL;

  entity trig_memory is
    generic(
      height : natural := 20;
      width : natural := 25;
      bc_width : natural := 11
    );
    port(
      clock : in std_logic;
      bc_clock : in std_logic;
      setup : in std_logic;

      in_bcid : in std_logic_vector(bc_width-1 downto 0);

      in_d_flag : in std_logic;
      in_d : in std_logic_vector(width-6+2-1 downto 0);
      out_d : out std_logic_vector(width+bc_width-6-1 downto 0);
      out_flag : out std_logic;

      err_out : out std_logic;

      search_finished_out : out std_logic;
      read_finished_out : out std_logic;
      read_reset : in std_logic;

      read_cmd : in std_logic;
      read_cmd_reset : out std_logic;

      in_trig_flag : in std_logic;
      in_trig_id : in std_logic_vector(bc_width-1 downto 0)
    );
  end trig_memory;


architecture composite_arch of trig_memory is

  CONSTANT const_addr_size : natural := natural(ceil(log2(real(height))));
  CONSTANT latency : natural := 4;

  component memory
  generic(
    addr_size : natural;
    height : natural;
    width : natural
  );
  port (
    clock : in std_logic;
    setup : in std_logic;

    w_addr : in std_logic_vector(const_addr_size-1 downto 0);
    r_addr : in std_logic_vector(const_addr_size-1 downto 0);

    w_d : in std_logic_vector(width-1 downto 0);
    r_d : out std_logic_vector(width-1 downto 0)
  );
  end component;

  component readout_corr
  generic(
    addr_size : natural;
    height : natural;
    width : natural;
    bc_width : natural;
    head_width : natural
  );
  port(
    clock : in std_logic;
    setup : in std_logic;

    r_addr : out std_logic_vector(const_addr_size-1 downto 0);
    en_data : out std_logic;

    r_d : in std_logic_vector(width-1 downto 0);
    out_ff : out std_logic_vector(width+bc_width-head_width -1 downto 0);
    out_flag_ff : out std_logic;

    search_finished_out : out std_logic;
    read_finished_out : out std_logic;
    read_reset : in std_logic;

    err_out : out std_logic;

    read_cmd : in std_logic; -- cmd bc read
    read_cmd_reset : out std_logic;
    in_trig_flag : in std_logic;
    in_trig_id : in std_logic_vector(bc_width-1 downto 0)
  );
  end component;

  -- component trigger_control
  --   generic(
  --     bc_width : natural
  --   );
  --   port(
  --     bc_clock : in std_logic;
  --     clock : in std_logic;
  --     reset : in std_logic;
  --
  --     trig : in std_logic;
  --     total_read_finished : in std_logic;
  --     read_finished_reset : out std_logic;
  --
  --     read_cmd : out std_logic; -- cmd bc read
  --     read_cmd_reset : in std_logic;
  --     search_flag : out std_logic; -- cmd bc search
  --     search_id : out std_logic_vector(bc_width-1 downto 0); -- bc to search
  --     bc_flag : out std_logic;
  --     bc_id : out std_logic_vector(bc_width-1 downto 0) -- bc for writing
  --   );
  -- end component;


  component hd_enc_comb
  port(
    d_in : in std_logic_vector(21-1 downto 0);
    code_out : out std_logic_vector(21-1 downto 0)
  );
  end component;

  ---------------------------------------------------------------

  signal w_addr : std_logic_vector(const_addr_size-1 downto 0);
  signal r_addr : std_logic_vector(const_addr_size-1 downto 0);
  signal r_addr_data : std_logic_vector(const_addr_size-1 downto 0);

  signal r_d : std_logic_vector(width-1 downto 0);
  signal en_data : std_logic;

  signal w_reg : unsigned(const_addr_size-1 downto 0);
  signal w_next : unsigned(const_addr_size-1 downto 0);

  signal pre_w_d : std_logic_vector(21-1 downto 0);

  signal w_d : std_logic_vector(width-1 downto 0);

  -- signal read_cmd : std_logic; -- cmd bc read

  signal search_flag : std_logic;
  -- signal search_finished_out : std_logic;
  -- signal read_finished_out : std_logic;
  -- signal read_reset : std_logic;
  -- signal read_cmd_reset : std_logic;
  signal trig_id : std_logic_vector(bc_width-1 downto 0);

  signal bc_flag : std_logic;

  signal bcid : std_logic_vector(bc_width-1 downto 0);
  signal d_in : std_logic_vector(width-6+2-1 downto 0);


begin
  ----------------------
  ------- MEMORY -------
  ----------------------
  head_memory : memory
  generic map(
    addr_size => const_addr_size,
    height => height,
    width => 6
  )
  port map(
    clock => clock,
    setup => setup,

    w_addr => w_addr,
    r_addr => r_addr,

    w_d => w_d(width-1 downto width-6),
    r_d => r_d(width-1 downto width-6)
  );

  data_memory : memory
  generic map(
    addr_size => const_addr_size,
    height => height,
    width => width-6
  )
  port map(
    clock => clock,
    setup => setup,

    w_addr => w_addr,
    r_addr => r_addr_data,

    w_d => w_d(width-7 downto 0),
    r_d => r_d(width-7 downto 0)
  );

  r_data_control : process(en_data, r_addr)
  begin
    if(en_data = '0')then
      r_addr_data <= r_addr;
    else
      r_addr_data <= (others => '0');
    end if;
  end process;

  ---------------------
  ------ READOUT ------
  ---------------------
  readout_inst : readout_corr
  generic map(
    addr_size => const_addr_size,
    height => height,
    width => width,
    bc_width => bc_width,
    head_width => 6
  )
  port map(
    clock => clock,
    setup => setup,

    r_addr => r_addr,
    en_data => en_data,

    r_d => r_d,

    out_ff => out_d,
    out_flag_ff => out_flag,

    search_finished_out => search_finished_out,
    read_finished_out => read_finished_out,
    read_reset => read_reset,

    err_out => err_out,

    read_cmd => read_cmd,
    read_cmd_reset => read_cmd_reset,
    in_trig_flag => in_trig_flag,
    in_trig_id => in_trig_id
  );

  -- trig_contr : trigger_control
  --   generic map(
  --     bc_width => bc_width
  --   )
  --   port map(
  --     bc_clock => bc_clock,
  --     clock => clock,
  --     reset => setup,
  --
  --     trig => in_trig_flag,
  --     total_read_finished => read_finished_out,
  --     read_finished_reset => read_reset,
  --
  --     read_cmd => read_cmd, -- cmd bc read
  --     read_cmd_reset => read_cmd_reset,
  --     search_flag => search_flag, -- cmd bc search
  --     search_id => trig_id, -- bc to search
  --     bc_flag => bc_flag,
  --     bc_id => bcid -- bc for writing
  --   );

  -------------------
  -- write control --
  -------------------

  input_process : process (in_bcid, in_d)
  begin
    if(in_d(width-6+2-1 downto width-6)= "11") then
      d_in <= "11" & "00000000" & in_bcid;
    else
      d_in <= in_d;
    end if;
  end process;


  secdec_enc : hd_enc_comb
  port map(
    d_in => d_in,
    code_out => pre_w_d
  );

  w_d(width-6+0) <= pre_w_d(width-6+0) xor pre_w_d(width-6+1);
  w_d(width-6+1) <= pre_w_d(width-6+0);
  w_d(width-6+2) <= pre_w_d(width-6+0);
  w_d(width-6+3) <= pre_w_d(width-6+1);
  w_d(width-6+4) <= pre_w_d(width-6+1);
  w_d(width-6+5) <= w_d(width-6+0) xor w_d(width-6+1)
    xor w_d(width-6+2) xor w_d(width-6+3)
    xor w_d(width-6+4);

  w_d(width-6-1 downto 0) <= pre_w_d(width-6-1 downto 0);

  write_control : process(clock, setup, w_next, in_d_flag)
  begin
    if (setup = '0') then
      w_reg <= (others => '0');
    elsif (clock'event and clock = '1' and in_d_flag = '1') then
      -- we can't write faster than internal write speed
      w_reg <= w_next;
    end if;
  end process;

  w_counter_control : process(w_reg)
  begin
    if (w_reg = height - 1) then
      w_next <= (others => '0');
    else
      w_next <= w_reg + 1;
    end if;
  end process;

  w_addr <= std_logic_vector(w_reg);

--  w_d <= in_d;

end composite_arch;
