library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;


  entity memory is
    generic(
      addr_size : natural;
      height : natural;
      width : natural
    );
    port(
      clock : in std_logic;
      setup : in std_logic;

      w_addr : in std_logic_vector(addr_size-1 downto 0);
      r_addr : in std_logic_vector(addr_size-1 downto 0);

      w_d : in std_logic_vector(width-1 downto 0);
      r_d : out std_logic_vector(width-1 downto 0)
    );
  end memory;

architecture arch of memory is

	component semi_dec
  generic(
    addr_size : natural;
    out_size : natural
  );
	port (
    enable : in std_logic;
		addr : in std_logic_vector(addr_size-1 downto 0);
		one_hot : out std_logic_vector(out_size-1 downto 0)
	);
  end component;

  component memory_cell_array
  generic(
    height : natural;
    width : natural
  );
	port (
		w : in std_logic_vector(height-1 downto 0);
		r : in std_logic_vector(height-1 downto 0);

		w_d : in std_logic_vector(width-1 downto 0);
		r_d : out std_logic_vector(width-1 downto 0)
	);
  end component;

  --decoder signals

	signal w : std_logic_vector(height-1 downto 0);
	signal r : std_logic_vector(height-1 downto 0);
  signal pre_r : std_logic_vector(height-1 downto 0);

  signal read_enable : std_logic;
  signal write_enable : std_logic;


  -- data control and amplification signals
  signal pre_r_d : std_logic_vector(width-1 downto 0);
  signal not_r_d : std_logic_vector(width-1 downto 0);

  signal w_d_latch : std_logic_vector(width-1 downto 0);
  signal post_w_d : std_logic_vector(width-1 downto 0);
  signal not_w_d : std_logic_vector(width-1 downto 0);

begin

--------------------
----READ CONTROL----
--------------------
  read_enable <= '1';

  read_dec : semi_dec
  generic map(
    addr_size => addr_size,
    out_size => height
  )
	port map (
    enable => read_enable,
		addr => r_addr,
		one_hot => r
	);

  -- latch the output of decoder.
  -- In this way it's possible to reduce toggling
  -- and also to reduce dead time (the full clock
  -- period is used to charge the output lines)
  -- read_ff : process(clock, setup)
  -- begin
  --   if (setup = '0') then
  --     r <= (others => '0');
  --   elsif rising_edge(clock) then
  --     r <= pre_r;
  --   end if;
  -- end process;

  -- read_ff : process (clock)
  -- begin
  --   if (clock'event and clock='1') then
  --     r <= pre_r;
  --   end if;
  -- end process;


  -- read amplification - single not (other not on input)
  -- removed not for ease in simulation. To add back again!
  r_d <= pre_r_d;
  -- not_r_d <= not pre_r_d;
  --we could try somthing better (sense amplifier, check how..)

---------------------
----WRITE CONTROL----
---------------------
  -- during clock = 1 data is prepared, then is enabled
  -- we could do this even
  write_enable <= '1';

  write_dec : semi_dec
  generic map(
    addr_size => addr_size,
    out_size => height
  )
	port map (
    enable => write_enable,
		addr => w_addr,
		one_hot => w
	);

  -- write_ff : process(clock, setup)
  -- begin
  --   if (setup = '0') then
  --     w_d_latch <= (others => '0');
  --   elsif rising_edge(clock) then
  --     w_d_latch <= w_d;
  --   end if;
  -- end process;

  -- write amplification - single not (other not on output)
  -- post_w_d <= not not_w_d;
  post_w_d <= w_d_latch;

----------------------
--------MEMORY--------
----------------------
  memory_inst : memory_cell_array
  generic map(
    height => height,
    width => width
  )
  port map(
    w => w,
    r => r,

    w_d => w_d,
    r_d => pre_r_d
  );

end arch;
